package henry.leetcode;

import java.util.*;

/*
Given a binary tree, return the vertical order traversal of its nodes values.
For each node at position (X, Y), its left and right children respectively will be at positions (X-1, Y-1) and (X+1, Y-1).
Running a vertical line from X = -infinity to X = +infinity, whenever the vertical line touches some nodes, we report the values of the nodes in order from top to bottom (decreasing Y coordinates).
If two nodes have the same position, then the value of the node that is reported first is the value that is smaller.
Return an list of non-empty reports in order of X coordinate.  Every report will have a list of values of nodes.

https://leetcode.com/contest/weekly-contest-122/problems/vertical-order-traversal-of-a-binary-tree/
 */

public class Leetcode987 {
    class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }
    TreeMap<Integer, List<int[]>> map = new TreeMap<>();
    public List<List<Integer>> verticalTraversal(TreeNode root) {
        helper(root, 0, 0);
        List<List<Integer>> res = new LinkedList<>();
        for(int x: map.keySet()) {
            List<int[]> inner = map.get(x);
            List<Integer> temp = new LinkedList<>();
            Collections.sort(inner, (a, b) -> a[0] != b[0] ? b[0] - a[0] : a[1] - b[1]);
            for(int[] arr: inner) {
                temp.add(arr[1]);
            }
            res.add(temp);
        }
        return res;
    }

    private void helper(TreeNode node, int x, int y) {
        if(node == null)
            return;
        if(!map.containsKey(x)) {
            map.put(x, new LinkedList<>());
        }
        List<int[]> list = map.get(x);
        list.add(new int[] {y, node.val});
        helper(node.left, x-1, y-1);
        helper(node.right, x+1, y-1);
    }
}
