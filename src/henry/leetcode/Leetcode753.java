package henry.leetcode;

/*
There is a box protected by a password. The password is n digits, where each letter can be one of the first k digits 0, 1, ..., k-1.
You can keep inputting the password, the password will automatically be matched against the last n digits entered.
For example, assuming the password is "345", I can open it when I type "012345", but I enter a total of 6 digits.
Please return any string of minimum length that is guaranteed to open the box after the entire string is inputted.

Example 1:
Input: n = 1, k = 2
Output: "01"
Note: "10" will be accepted too.
 */

import java.util.*;

public class Leetcode753 {
    public String crackSafe(int n, int k) {
        StringBuffer sb = new StringBuffer();
        Set<String> visited = new HashSet<>();
        for(int i = 0; i < n-1; i++) {
            sb.append(0);
        }
        int total = 1;
        for(int i = 0; i < n; i++)  total = total * k;
        for(int i = 0; i < total; i++) {
            for(int j = k-1; j >= 0; j--) {
                String sub = sb.substring(sb.length() - n + 1, sb.length());
                sub = sub + j;
                if(!visited.contains(sub)) {
                    visited.add(sub);
                    sb.append(j);
                    break;
                }
            }
        }
        return sb.toString();
    }
}

/*
beat 31.82%
这个题目是欧拉回路（一笔画问题）
记住这个题目的代码！
 */