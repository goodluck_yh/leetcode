package henry.leetcode;

import java.util.LinkedList;
import java.util.List;

public class Leetcode958 {
    public class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }

    public boolean isCompleteTree(TreeNode root) {
        if(root.left == null && root.right == null)  return true;
        boolean isFull = true;
        List<TreeNode> list = new LinkedList<>();
        list.add(root);
        while(list.size() != 0) {
            List<TreeNode> temp = new LinkedList<>();
            boolean isFullNew = true;
            for(int i = 0; i < list.size(); i++) {
                if(list.get(i).left != null) {
                    if(!isFull || !isFullNew) return false;
                    temp.add(list.get(i).left);
                } else {
                    isFullNew = false;
                }
                if(list.get(i).right != null) {
                    if(!isFull || !isFullNew) return false;
                    temp.add(list.get(i).right);
                } else {
                    isFullNew = false;
                }
            }
            isFull = isFullNew;
            list = temp;
        }
        return true;
    }
}
