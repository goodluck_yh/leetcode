package henry.leetcode;

import java.util.*;

public class Leetcode809 {
    public int expressiveWords(String S, String[] words) {
        if(S.length() == 0)  return 0;
        int res = 0;
        List<Character> chs = new LinkedList<>();
        List<Integer> count = new LinkedList<>();
        char[] ss = S.toCharArray();
        char cur = ss[0];
        int cur_num = 0;
        for(char c: ss) {
            if(c == cur) {
                cur_num++;
            } else {
                chs.add(cur);
                count.add(cur_num);
                cur = c;
                cur_num = 1;
            }
        }
        chs.add(cur);
        count.add(cur_num);

        for(String word: words) {
            int index = 0;
            int indexS = 0;
            while(index < word.length()) {
                int i = 0;
                while(index+i < word.length() && word.charAt(index+i) == chs.get(indexS))    i++;
                if(count.get(indexS) < 3) {
                    if(i != count.get(indexS))  break;
                } else {
                    if(i == 0 || i > count.get(indexS)) break;
                }
                indexS++;
                index = index + i;
            }
            if(index == word.length() && indexS == chs.size()) //bug
                res++;
        }
        return res;
    }
}

/*
自己的思路和官方答案一致。但是有一个bug
有一点心得体会是，往往bug出现在if里面

复杂度O(n*k) n是words总长度，k是字符串缩减后长度
 */