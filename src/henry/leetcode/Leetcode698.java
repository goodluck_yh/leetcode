package henry.leetcode;

import java.util.*;

public class Leetcode698 {
    public boolean canPartitionKSubsets(int[] nums, int k) {
        int res = 0, max = 0;
        for(int i = 0; i < nums.length; i++) {
            res += nums[i];
            max = Math.max(max, nums[i]);
        }

        if(res % k != 0 || max > res / k)
            return false;

        Arrays.sort(nums);
        int i = nums.length-1, average = res / k;
        while(i >= 0 && nums[i] == average)
            i--;
        int count = nums.length - i - 1;
        int[] nums1 = new int[i+1];
        for(i = 0; i < nums1.length; i++) {
            nums1[i] = nums[i];
        }
        return helper(nums1, k - count, 0, 0, 0, average);
    }

    private boolean helper(int[] nums, int k, int sum, int idx, int count, int average) {
        if(k-1 == count || k == count)
            return true;

        for(int i = idx; i < nums.length; i++) {
            int sum1 = sum + nums[i];
            int temp = nums[idx];
            nums[idx] = nums[i];
            nums[i] = temp;
            if(sum1 == average) {
                boolean flag = helper(nums, k, 0, idx+1, count+1, average);
                if(flag)
                    return true;
            }
            else if (sum1 < average){
                boolean flag = helper(nums, k, sum1, idx+1, count, average);
                if(flag)
                    return true;
            }else
                break;
            temp = nums[idx];
            nums[idx] = nums[i];
            nums[i] = temp;
        }
        return false;
    }
}

/*
此题目是利用了穷举法，穷举法是我一直会遗忘的方法，有时候想了很久贪心，dp，其实他们都不行，只能穷举
题目中数值的范围，提示了这是个穷举法
上面是我自己写的，下面是discussion里面的答案， 很简洁
在穷举时候，我们不需要移位，可以通过设置一个visited数组来处理，毕竟本质上穷举也是一种dfs
 */

class Leetcode698_2 {
    public boolean canPartitionKSubsets(int[] A, int k) {
        if (k > A.length) return false;
        int sum = 0;
        for (int num : A) sum += num;
        if (sum % k != 0) return false;
        boolean[] visited = new boolean[A.length];
        Arrays.sort(A);
        return dfs(A, 0, A.length - 1, visited, sum / k, k);
    }

    public boolean dfs(int[] A, int sum, int st, boolean[] visited, int target, int round) {
        if (round == 0) return true;
        if (sum == target && dfs(A, 0, A.length - 1, visited, target, round - 1))
            return true;
        for (int i = st; i >= 0; --i) {
            if (!visited[i] && sum + A[i] <= target) {
                visited[i] = true;
                if (dfs(A, sum + A[i], i - 1, visited, target, round))
                    return true;
                visited[i] = false;
            }
        }
        return false;
    }
}