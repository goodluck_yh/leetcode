package henry.leetcode;

/*
Input: A = [2,1,2,4,2,2],
       B = [5,2,6,2,3,2]
Output: 2
Explanation:
The first figure represents the dominoes as given by A and B: before we do any rotations.
If we rotate the second and fourth dominoes, we can make every value in the top row equal to 2, as indicated by the second figure.
 */
public class Leetcode1007 {
    public int minDominoRotations(int[] A, int[] B) {
        int num1 = A[0];
        int num2 = B[0];
        int cnt11 = 0, cnt12 = 0, total1 = 0;
        int cnt21 = 0, cnt22 = 0, total2 = 0;
        for (int i = 0; i < A.length; i++) {
            // handle A
            if (A[i] == num1 || B[i] == num1) {
                total1++;
                if(A[i] == num1) {
                    cnt11++;
                }
                if(B[i] == num1) {
                    cnt12++;
                }
            }

            //handle B
            if (A[i] == num2 || B[i] == num2) {
                total2++;
                if(A[i] == num2) {
                    cnt21++;
                }
                if(B[i] == num2) {
                    cnt22++;
                }
            }
        }
        int res = Integer.MAX_VALUE;
        if(total1 == A.length) {
            res = Math.min(res, Math.min(A.length - cnt11, A.length- cnt12));
        }
        if(total2 == A.length) {
            res = Math.min(res, Math.min(A.length - cnt21, A.length- cnt22));
        }
        return res == Integer.MAX_VALUE? -1 : res;
//        if (total1 != A.length && total2 != B.length) {
//            return -1;
//        } else if (total1 != A.length) {
//            return Math.min(cnt2, A.length - cnt2);
//        } else if (total2 != A.length) {
//            return Math.min(cnt1, A.length - cnt1);
//        } else {
//            return Math.min(Math.min(cnt1, A.length - cnt1), Math.min(cnt2, A.length - cnt2));
//        }
    }
}
