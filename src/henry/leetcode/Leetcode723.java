package henry.leetcode;

public class Leetcode723 {
    public int[][] candyCrush(int[][] board) {
        boolean isChange;
        do {
            isChange = false;
            for(int i = 0; i < board.length; i++) {
                for(int j = 0; j < board[0].length; j++) {
                    if(board[i][j] != 0) {
                        int val = Math.abs(board[i][j]);
                        boolean res = count(board, i, j, val);
                        if(res)
                            isChange = true;
                    }
                }
            }
            if(isChange) {
                drop(board);
            }
        } while(isChange);
        return board;
    }

    private boolean count(int[][] board, int i, int j, int val) {
        boolean res = false;
        if(i + 2 < board.length && Math.abs(board[i+1][j]) == val && Math.abs(board[i+2][j]) == val) {
            res = true;
            board[i][j] = -val;
            board[i+1][j] = -val;
            board[i+2][j] = -val;
        }

        if(j + 2 < board[0].length && Math.abs(board[i][j+1]) == val && Math.abs(board[i][j+2]) == val) {
            res = true;
            board[i][j] = -val;
            board[i][j+1] = -val;
            board[i][j+2] = -val;
        }
        return res;
    }

    private void drop(int[][] board) {
        for(int i = 0; i < board[0].length; i++) {
            int fast = board.length-1, slow = board.length-1;
            while(fast >= 0 && slow >= 0) {
                if(board[fast][i] > 0) {
                    int temp = board[fast][i];
                    board[fast][i] = board[slow][i];
                    board[slow][i] = temp;
                    slow--;
                    fast--;
                } else {
                    board[fast][i] = 0;
                    fast--;
                }
            }
        }
    }
}

/*
一开始理解错了题目，必须是横向三个，纵向三个才能消
我的这个做法是每次向右检测3个，再向下检测3个，并不快
比较快的做法是，每次仅仅检测横向，然后再检测纵向。
 */
