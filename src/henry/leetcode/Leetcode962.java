package henry.leetcode;

import java.util.*;

public class Leetcode962 {
    public int maxWidthRamp(int[] A) {
        int res = 0;
        LinkedList<Integer> s = new LinkedList<>();
        for(int i = 0; i < A.length; i++) {
            if(s.size() > 0) {
                int j = s.size() - 1;
                while(j >= 0 && A[s.get(j)] <= A[i])
                    j--;
                if(j != s.size() - 1)
                    res = Math.max(res, i - s.get(j+1) );
            }
            if(s.isEmpty() || A[i] < A[s.getLast()])
                s.add(i);
        }
        return res;
    }


    public static void main(String[] args) {
        Leetcode962 leetcode962 = new Leetcode962();
        int res = leetcode962.maxWidthRamp(new int[] {3,4,1,2});
        System.out.println(res);
    }
}
