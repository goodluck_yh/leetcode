package henry.leetcode;

/*
Input: ["bella","label","roller"]
Output: ["e","l","l"]
 */
import java.util.*;

public class Leetcode1002 {
    public List<String> commonChars(String[] A) {
        List<String> res = new LinkedList<>();
        int[] count = new int[26];
        for(int i = 0; i < A.length; i++) {
            String s = A[i];
            int[] temp = new int[26];
            for(int j = 0; j < s.length(); j++) {
                char ch = s.charAt(j);
                int idx = ch - 'a';
                temp[idx]++;
            }
            if(i == 0) {
                count = temp;
            } else {
                for(int j = 0; j < 26; j++) {
                    count[j] = Math.min(count[j], temp[j]);
                }
            }
        }
        for(int i = 0; i < 26; i++) {
            for(int j = 0; j < count[i]; j++) {
                res.add("" + (char)(i + 'a'));
            }
        }
        return res;
    }
}
