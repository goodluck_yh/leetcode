package henry.leetcode;

import java.util.*;

public class Leetcode792 {
    public int numMatchingSubseq(String S, String[] words) {
        char[] chs = S.toCharArray();
        int res = 0;
        for(String word: words) {
            char[] p = word.toCharArray();
            if(match(chs, p))
                res++;
        }
        return res;
    }

    private boolean match(char[] s, char[] t) {
        int p1 = 0;
        for(int i = 0; i < t.length; i++) {
            while(p1 < s.length && s[p1] != t[i]) p1++;
            if(p1 == s.length)  return false;
            p1++;
        }
        return true;
    }
}

/*
上面这个解法非常慢！复杂度是O(m*n) m是S长度，n是words长度

下面这个解法仅仅loop S一次。 这个算法不是那么好想，其实说到底是个多指针问题！
 */

class Leetcode792_2 {
    public int numMatchingSubseq(String S, String[] words) {
        int[] ps = new int[words.length];
        Map<Character, Set<Integer>> map = new HashMap<>();
        int res = 0;
        for(char ch = 'a'; ch <= 'z'; ch = (char)(ch + 1)) {
            map.put(ch, new HashSet<>());
        }
        for(int i = 0; i < words.length; i++) {
            map.get(words[i].charAt(0)).add(i);
        }
        char[] chs = S.toCharArray();
        for(char c: chs) {
            Set<Integer> set = map.get(c);
            if(!set.isEmpty()) {
                Set<Integer> temp = new HashSet<>();
                map.put(c, temp);
                for(int i: set) {
                    ps[i]++;
                    if(ps[i] == words[i].length()) {
                        res++;
                    } else {
                        char nextC = words[i].charAt(ps[i]);
                        map.get(nextC).add(i);
                    }
                }
            }
        }
        return res;
    }
}