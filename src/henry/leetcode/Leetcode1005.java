package henry.leetcode;

/*
Input: A = [2,-3,-1,5,-4], K = 2
Output: 13
Explanation: Choose indices (1, 4) and A becomes [2,3,-1,5,4].
 */

import java.util.*;

public class Leetcode1005 {
    public int largestSumAfterKNegations(int[] A, int K) {
        List<Integer> neg = new LinkedList<>();
        PriorityQueue<Integer> pqn = new PriorityQueue<>();
        PriorityQueue<Integer> pqp = new PriorityQueue<>();
        int res = 0;
        for(int i = 0; i < A.length; i++) {
            if(A[i] < 0) {
                neg.add(i);
                pqn.add(A[i]);
            } else{
                res += A[i];
                pqp.add(A[i]);
            }
        }
        while(K > 0 && pqn.size() > 0) {
            int peek = pqn.peek();
            res = res - peek;
            pqn.poll();
            pqp.add(-peek);
            K--;
        }
        if(K % 2 == 1) {
            res = res - pqp.peek();
        }

        return res;
    }
}
