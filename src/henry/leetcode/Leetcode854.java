package henry.leetcode;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

/*
Input: A = "aabc", B = "abca"
Output: 2
 */
public class Leetcode854 {
    public int kSimilarity(String A, String B) {
        Set<String> visited = new HashSet<>();
        int step = 0;
        LinkedList<String> queue = new LinkedList<>();
        queue.add(A);
        while(!queue.isEmpty()) {
            int size = queue.size();
            while(size-- > 0) {
                String s = queue.poll();
                if(s.equals(B))
                    return step;
                int i;
                for(i = 0; i < s.length(); i++) {
                    if(s.charAt(i) != B.charAt(i))
                        break;
                }
                for(int j = i + 1; j < s.length(); j++) {
                   if(s.charAt(j) == B.charAt(i)) {
                       StringBuffer sb = new StringBuffer(s);
                       sb.setCharAt(j, s.charAt(i));
                       sb.setCharAt(i, B.charAt(i));
                       String temp = sb.toString();
                       if(!visited.contains(temp)) {
                           visited.add(temp);
                           queue.add(temp);
                       }
                   }
                }
            }
            step++;
        }
        return -1;
    }
}
