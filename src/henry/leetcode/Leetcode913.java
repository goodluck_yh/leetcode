package henry.leetcode;

import java.util.*;

public class Leetcode913 {
    private final int DRAW = 0, MOUSE_WIN = 1, CAT_WIN = 2, MOUSE_TURN = 1, CAT_TURN = 2;
    public int catMouseGame(int[][] graph) {
        int N = graph.length;

        int[][][] degree = new int[50][50][3];
        int[][][] color = new int[50][50][3];
        // init degree
        for(int i = 0; i < N; i++) {
            for(int j = 0; j < N; j++) {
                degree[i][j][MOUSE_TURN] = graph[i].length;
                degree[i][j][CAT_TURN] = graph[j].length;
                for(int k: graph[j]) {
                    if(k == 0) {
                        degree[i][j][CAT_TURN]--;
                        break;
                    }
                }
            }
        }

        Queue<int[]> queue = new LinkedList<>();
        // init queue and color
        for(int i = 0; i < N; i++) {
            for(int j = 1; j <= 2; j++) {
                color[0][i][j] = MOUSE_WIN;
                queue.add(new int[] {0, i, j, MOUSE_WIN});
                System.out.println(0 + " " + i + " " + j + " " + MOUSE_WIN);
                if(i > 0) {
                    color[i][i][j] = CAT_WIN;
                    queue.add(new int[] {i, i, j, CAT_WIN});
                    System.out.println(i + " " + i + " " + j + " " + CAT_WIN);
                }
            }
        }

        while(!queue.isEmpty()) {
            int[] node = queue.remove();
            int m = node[0], c = node[1], t = node[2], r = node[3];
            List<int[]> parents = parents(graph, m, c, t);
            for(int[] parent: parents) {
                int m2 = parent[0], c2 = parent[1], t2 = parent[2];
                if(color[m2][c2][t2] == DRAW) {
                    if(r == t2) {
                        color[m2][c2][t2] = r;
                        queue.add(new int[] {m2, c2, t2, r});
                    } else {
                        degree[m2][c2][t2]--;
                        if(degree[m2][c2][t2] == 0) {
                            color[m2][c2][t2] = 3 - t2;
                            queue.add(new int[]{m2, c2, t2, 3 - t2});
                        }
                    }
                }
            }
        }
        return color[1][2][MOUSE_TURN];
    }

    // What nodes could play their turn to
    // arrive at node (m, c, t) ?
    public List<int[]> parents(int[][] graph, int m, int c, int t) {
        List<int[]> res = new ArrayList<>();
        if(t == CAT_TURN) {
            for(int nei: graph[m]) {
                res.add(new int[] {nei, c, MOUSE_TURN});
            }
        } else {
            for(int nei: graph[c]) {
                if (nei > 0)
                    res.add(new int[]{m, nei, CAT_TURN});
            }
        }
        return res;
    }

    public static void main(String[] args) {
        Leetcode913 leetcode = new Leetcode913();
        System.out.println(leetcode.catMouseGame(new int[][] {{2,5},{3},{0,4,5},{1,4,5},{2,3},{0,2,3}}));
    }
}


/*
这个解法是官方解法
- 这个问题每个状态可以表示为（mouse, cat, who_is_next)
- 创建degree数组，统计每个状态下，有几个边（注意，猫不可以在0）
- 创建color数组，初始化：老鼠在0处，无论下一个是谁，老鼠赢（1)；猫和老鼠在一起，无论下一个谁，猫赢（不在0处） (2)
- 将所有要么老鼠赢，要么猫赢得放入到一个list中，进行bfs（由于不需要统计步数，所以bfs可以写成每次取出一个）
- 计算所有可能的下一步，对于每一个可能，
- 如果现在的结果是未知（0），那么如果本次结果是猫（或者老鼠）赢，且‘那个可能’的下一步是猫（或者老鼠），那么直接赋值
- 否则，对应的degree状态自减，如果degree为0，则‘那个可能’的下一步是谁，谁就输！

complexity O(n^3): we have n^2 different status, and each status has N next, so it is n^3
 */