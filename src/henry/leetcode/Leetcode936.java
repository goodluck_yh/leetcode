package henry.leetcode;

import java.util.*;

public class Leetcode936 {
    public int[] movesToStamp(String stamp, String target) {
        List<Integer> res = new LinkedList<>();
        String origin = "";
        for(int i = 0; i < target.length(); i++) {
            origin = origin + '?';
        }
        StringBuilder sb = new StringBuilder(target);
        while(!sb.toString().equals(origin)) {
            boolean hasNext = false;
            for(int i = 0; i <= sb.length() - stamp.length(); i++) {
                if(match(stamp, sb.substring(i))) {
                    System.out.println(i);
                    res.add(0, i);
                    for(int j = 0; j < stamp.length(); j++) {
                        sb.setCharAt(i + j, '?');
                    }
                    hasNext = true;
                }
            }
            if(!hasNext)
                return new int[] {};
        }
        return res.stream().mapToInt(i->i).toArray();
    }

    private boolean match(String stamp, String target) {
        boolean allStar = true;
        for(int i = 0; i < stamp.length(); i++) {
            if(target.charAt(i) != '?') {
                allStar = false;
                break;
            }
        }
        if(allStar) return false;
        for(int i = 0; i < stamp.length(); i++) {
            if(target.charAt(i) == '?')
                continue;
            if(target.charAt(i) != stamp.charAt(i))
                return false;
        }
        return true;
    }

}

/*
key idea: reverse process
这个解题思路最大的特点是反过来，不是从???? -> target，而是从target->????
beat 15.43%
 */