package henry.leetcode;

import java.util.*;

/*
In this problem, a tree is an undirected graph that is connected and has no cycles.
The given input is a graph that started as a tree with N nodes (with distinct values 1, 2, ..., N), with one additional edge added. The added edge has two different vertices chosen from 1 to N, and was not an edge that already existed.
The resulting graph is given as a 2D-array of edges. Each element of edges is a pair [u, v] with u < v, that represents an undirected edge connecting nodes u and v.
Return an edge that can be removed so that the resulting graph is a tree of N nodes. If there are multiple answers, return the answer that occurs last in the given 2D-array. The answer edge [u, v] should be in the same format, with u < v.

Example 1:
Input: [[1,2], [1,3], [2,3]]
Output: [2,3]
Explanation: The given undirected graph will be like this:
  1
 / \
2 - 3
Example 2:
Input: [[1,2], [2,3], [3,4], [1,4], [1,5]]
Output: [1,4]
Explanation: The given undirected graph will be like this:
5 - 1 - 2
    |   |
    4 - 3
Note:
The size of the input 2D-array will be between 3 and 1000.
Every integer represented in the 2D-array will be between 1 and N, where N is the size of the input array.
*/
public class Leetcode684 {

    class UnionFind {
        int[] parent;

        public UnionFind(int n ) {
            parent = new int[n];
            for(int i = 0; i < n; i++) {
                parent[i] = i;
            }
        }

        public boolean union(int x, int y) {
            int p1 = find(x), p2 = find(y);
            if(p1 == p2)    return false;
            parent[p1] = p2;
            return true;
        }

        public int find(int x) {
            while(x != parent[x]) {
                x = parent[x];
            }
            return x;
        }
    }

    public int[] findRedundantConnection(int[][] edges) {
        int n = edges.length;
        UnionFind uf = new UnionFind(n);
        for(int i = 0; i < n; i++) {
            if(!uf.union(edges[i][0]-1, edges[i][1]-1)) {
                return edges[i];
            }
        }
        return null;
    }

    // DFS
    public int[] findRedundantConnection1(int[][] edges) {
        int len = edges.length;
        int[][] graph = new int[len][len];
        Arrays.fill(graph, 1);
        for(int i = 0; i < len; i++) {
            int start = edges[i][0] - 1, end = edges[i][1] - 1;
            graph[start][end] = 1;
        }
        int[] visted = new int[len];
        Arrays.fill(visted, 0);
        return null;
    }

    private void dfs(int[][] graph, int[] visted, int start) {

    }

    public static void main(String[] args) {
        Leetcode684 leetcode = new Leetcode684();
        int[][] edges = new int[][] {{1,2}, {2,3}, {3,4}, {1,4}, {1,5}};
        int[] redundant = leetcode.findRedundantConnection(edges);
        System.out.println(redundant[0] + " " + redundant[1]);
    }
}


