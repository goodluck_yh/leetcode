package henry.leetcode;

/*
Given nums = [-2, 0, 3, -5, 2, -1]

sumRange(0, 2) -> 1
sumRange(2, 5) -> -1
sumRange(0, 5) -> -3
 */
public class Leetcode303 {
    int[] data, tree;

    public Leetcode303(int[] nums) {
        this.data = nums;
        this.tree = new int[nums.length *4];
        if(nums.length != 0)
            buildTree(0, 0, this.data.length-1);
    }

    private void buildTree(int treeIdx, int left, int right) {
        System.out.println(treeIdx + " " + left + " " + right);
        if(left == right) {
            tree[treeIdx] = data[left];
            return;
        }
        int lefti = treeIdx * 2 + 1, righti = treeIdx * 2 + 2;
        int mid = (left + right) / 2;
        buildTree(lefti, left, mid);
        buildTree(righti, mid+1, right);
        tree[treeIdx] = tree[lefti] + tree[righti];
    }
    private int query(int treeIdx, int left, int right, int leftq, int rightq) {
        if(left == leftq && right == rightq)
            return this.tree[treeIdx];
        int mid = (left + right) / 2, lefti = treeIdx * 2 + 1, righti = treeIdx * 2 + 2;
        if(leftq > mid) {
            return query(righti, mid+1, right, leftq, rightq);
        } else if (rightq <= mid) {
            return query(lefti, left, mid, leftq, rightq);
        } else {
            return query(lefti, left, mid, leftq, mid) + query(righti, mid+1, right, mid+1, rightq);
        }
    }

    public int sumRange(int i, int j) {
        return query(0, 0, this.data.length-1, i, j);
    }

    public static void main(String[] args) {
        Leetcode303 leetcode = new Leetcode303(new int[] {});
    }
}

/*
beat 100.00%
segment tree
 */