package henry.leetcode;

public class Leetcode998 {
    class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }
    public TreeNode insertIntoMaxTree(TreeNode root, int val) {
        return helper(root, val);
    }

    private TreeNode helper(TreeNode node, int val) {
        if(node == null) {
            return new TreeNode(val);
        }
        if(node.val < val) {
            TreeNode extra = new TreeNode(val);
            extra.left = node;
            return extra;
        } else {
            node.right = helper(node.right, val);
            return node;
        }
    }
}
