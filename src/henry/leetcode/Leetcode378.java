package henry.leetcode;

import java.util.*;

/*
Given a n x n matrix where each of the rows and columns are sorted in ascending order, find the kth smallest element in the matrix.
Note that it is the kth smallest element in the sorted order, not the kth distinct element.

Example:
matrix = [
   [ 1,  5,  9],
   [10, 11, 13],
   [12, 13, 15]
],
k = 8,
return 13.
 */

// option1: pq
public class Leetcode378 {
    public int kthSmallest(int[][] matrix, int k) {
        PriorityQueue<Integer> pq = new PriorityQueue<>((a, b) -> b - a);
        for(int i = 0; i < matrix.length; i++) {
            for(int j = 0; j < matrix[0].length; j++) {
                pq.add(matrix[i][j]);
                if(pq.size() > k) {
                    pq.poll();
                }
            }
        }
        return pq.peek();
    }
}

/*
beat 13%
 */

class Leetcode378_2 {
    public int kthSmallest(int[][] matrix, int k) {
        int lo = matrix[0][0], hi = matrix[matrix.length-1][matrix.length-1];
        while(lo < hi) {
            int mid = (lo + hi) / 2, cnt = 0;
            boolean find = false;
            for(int i = 0; i < matrix.length; i++) {
                int j = 0;
                while(j < matrix.length && matrix[i][j] <= mid) {
                    if(matrix[i][j] == mid)
                        find = true;
                    j++;
                }
                cnt += j;
            }
            if(cnt == k) {
                if(find)
                    return mid;
                else
                    hi = mid - 1;
            }
            else if(cnt < k) lo = mid + 1;
            else    hi = mid;
        }
        return lo;
    }
}
/*
beat 66%
左右夹逼主要问题是在于等于，小于，大于的处理，要先找个例子
 */