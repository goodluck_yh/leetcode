package henry.leetcode;

import java.util.*;

public class Leetcode967 {
    List<Integer> list = new LinkedList<>();
    Set<Integer> set = new HashSet<>();
    public int[] numsSameConsecDiff(int N, int K) {
        helper(N, K, new char[N], 0);
        int[] res = new int[list.size()];
        for(int i = 0; i < list.size(); i++) {
            res[i] = list.get(i);
        }
        return res;
    }

    private void helper(int n, int k, char[] cur, int p) {
        if(p == n) {
            int res = 0;
            for(int i = 0; i < n; i++) {
                res = res * 10 + cur[i] - '0';
            }
            if(!set.contains(res)) {
                set.add(res);
                list.add(res);
            }
            return;
        }
        if(p == 0) {
            int i = 0;
            if(n > 1) {
                i = 1;
            }
            for(; i < 10; i++) {
                cur[0] = (char)('0' + i);
                helper(n, k, cur, 1);
            }
        } else {
            char pre = cur[p-1];
            if(pre + k <= '9') {
                cur[p] = (char)(pre + k);
                helper(n, k, cur, p+1);
            }

            if(pre - k >= '0') {
                cur[p] = (char)(pre - k);
                helper(n, k, cur, p+1);
            }
        }
    }
}
