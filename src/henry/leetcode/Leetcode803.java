package henry.leetcode;

/*
We have a grid of 1s and 0s; the 1s in a cell represent bricks.  A brick will not drop if and only if it is directly connected to the top of the grid, or at least one of its (4-way) adjacent bricks will not drop.

We will do some erasures sequentially. Each time we want to do the erasure at the location (i, j), the brick (if it exists) on that location will disappear, and then some other bricks may drop because of that erasure.

Return an array representing the number of bricks that will drop after each erasure in sequence.

Example 1:
Input:
grid = [[1,0,0,0],[1,1,1,0]]
hits = [[1,0]]
Output: [2]
Explanation:
If we erase the brick at (1, 0), the brick at (1, 1) and (1, 2) will drop. So we should return 2.
 */

public class Leetcode803 {
    class UnionFind {
        int[] parent;
        int[] size;

        UnionFind(int len) {
            parent = new int[len];
            size = new int[len];
            for(int i = 0; i < len; i++) {
                parent[i] = i;
                size[i] = 1;
            }
        }

        void union(int i1, int j1, int i2, int j2, int m, int n) {
            int x1 = i1 * m + j1 + 1, x2 = i2 * m + j2 + 1;
            int p1 = find(x1), p2 = find(x2);
            if(p1 == p2)    return;
            if(p1 < p2) {
                size[p1] += size[p2];
                parent[p2] = p1;
            } else {
                size[p2] += size[p1];
                parent[p1] = p2;
            }
        }

        int find(int x) {
            while (parent[x] != x) {
                x = parent[x];
            }
            return x;
        }
    }

    public int[] hitBricks(int[][] grid, int[][] hits) {
        // set falling bricks as 2
        for(int i = 0; i < hits.length; i++) {
            if(grid[hits[i][0]][hits[i][1]] == 1)
                grid[hits[i][0]][hits[i][1]] = 2;
        }
        // calc size in the final
        UnionFind uf = new UnionFind(grid.length * grid[0].length + 1);
        for(int i = 0; i < grid.length; i++) {
            for(int j = 0; j < grid.length; j++) {
                if(grid[i][j] == 1) {
                    unionAround(grid, i, j, uf);
                }
            }
        }
        int size = uf.size[0];
        // calc size in reverse order
        int[] res = new int[hits.length];
        for(int i = hits.length - 1; i >= 0; i--) {
            if(grid[hits[i][0]][hits[i][1]] == 2) {
                grid[hits[i][0]][hits[i][1]] = 1;
                unionAround(grid, hits[i][0], hits[i][1], uf);
                if(uf.size[0] > size + 1)
                    res[i] = uf.size[0] - size - 1;
                size = uf.size[0];
            }
        }
        return res;
    }

    private void unionAround(int[][] grid, int i, int j, UnionFind uf) {
        int[] dx = new int[] {-1, 0, 1, 0};
        int[] dy = new int[] {0, 1, 0, -1};
        for(int k = 0; k < 4; k++) {
            int i1 = i + dx[k], j1 = j + dy[k];
            if(i1 < 0 || i1 >= grid.length || j1 < 0 || j1 >= grid[0].length)
                continue;
            if(grid[i1][j1] == 1)
                uf.union(i, j, i1, j1, grid.length, grid[0].length);
        }
        if(i == 0) {
            uf.union(0, -1, i, j, grid.length, grid[0].length);
        }
    }
}

/*
这个题目不好做。看了别人的代码很久才明白什么意思！
这个题目的提示就是，uf算法除了可以判断连通性之外，还可以判断连通分量的大小
另外uf算法，只能用于增加，不能用于移除一个点
 */