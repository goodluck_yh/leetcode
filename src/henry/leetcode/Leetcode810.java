package henry.leetcode;

import java.util.*;

/*
We are given non-negative integers nums[i] which are written on a chalkboard.  Alice and Bob take turns erasing exactly one number from the chalkboard, with Alice starting first.  If erasing a number causes the bitwise XOR of all the elements of the chalkboard to become 0, then that player loses.  (Also, we'll say the bitwise XOR of one element is that element itself, and the bitwise XOR of no elements is 0.)
Also, if any player starts their turn with the bitwise XOR of all the elements of the chalkboard equal to 0, then that player wins.
Return True if and only if Alice wins the game, assuming both players play optimally.

Example:
Input: nums = [1, 1, 2]
Output: false
Explanation:
Alice has two choices: erase 1 or erase 2.
If she erases 1, the nums array becomes [1, 2]. The bitwise XOR of all the elements of the chalkboard is 1 XOR 2 = 3. Now Bob can remove any element he wants, because Alice will be the one to erase the last element and she will lose.
If Alice erases 2 first, now nums becomes [1, 1]. The bitwise XOR of all the elements of the chalkboard is 1 XOR 1 = 0. Alice will lose.s
 */

public class Leetcode810 {
    HashMap<String, Boolean> map = new HashMap<>();
    public boolean xorGame(int[] nums) {
        Set<Integer> set = new HashSet<>();
        for(int i = 0; i < nums.length; i++) {
            int num = nums[i];
            if(set.contains(num))
                set.remove(num);
            else
                set.add(num);
        }
        nums = new int[set.size()];
        int idx = 0;
        for(int i : set) {
            nums[idx++] = i;
        }
        if(nums.length == 0)    return true;
        //if(nums.length == 1)    return false;
        int[] cnt = new int[16];
        int even = 0;
        for(int i = 0; i < nums.length; i++) {
            int num = nums[i];
            even = handleNum(num, cnt, true);
        }
        boolean flag = helper(cnt, nums, new boolean[nums.length], even);
        return flag;
    }

    private boolean helper(int[] cnt, int[] nums, boolean[] visited, int even) {
        String key = convert(visited);
        if(even == 0) {
            map.put(key, true);
            return true;
        }

        if(map.containsKey(key))
            return map.get(key);
        for(int i = 0; i < nums.length; i++) {
            if(visited[i])  continue;
            int num = nums[i];
            int new_even = handleNum(num, cnt, false);
            visited[i] = true;
            boolean flag = helper(cnt, nums, visited, new_even);

            visited[i] = false;
            handleNum(num, cnt, true);
            if(!flag) {
                map.put(key, true);
                return true;
            }
        }
        map.put(key, false);
        return false;
    }

    private int handleNum(int num, int[] cnt, boolean add) {
        int res = 0;
        for(int i = 0; i < 16; i++) {
            int temp = num & 1;
            if(add)
                cnt[i] = cnt[i] + temp;
            else
                cnt[i] = cnt[i] - temp;
            if(cnt[i] % 2 == 1)
                res++;
            num = num >> 1;
        }
        return res;
    }

    private String convert(boolean[] visited) {
        StringBuffer sb = new StringBuffer();
        for(int i = 0; i < visited.length; i++) {
            boolean v = visited[i];
            if(v)
                sb.append('y');
            else
                sb.append('n');
            sb.append('#');
        }
        return sb.toString();
    }

    public static void main(String[] args) {
        Leetcode810 leetcode = new Leetcode810();
        System.out.println(leetcode.xorGame(new int[] {1,1,2,3,4}));
    }
}

/*
上面这个解法，绞尽脑汁还是没法提高效率跑完所有test case
 */

class Leetcode810_2 {
    public boolean xorGame(int[] nums) {
        int xor = 0;
        for (int i: nums) xor ^= i;
        return xor == 0 || nums.length % 2 == 0;
    }
}

/*
上面的这个解法太秀了
解释如下：
-这题一般情况下可以考虑遍历搜索，不过这题给的范围1 <= N <= 1000.基本说明了有其他比较trick的做法。
我们分步考虑下，如果Alice要赢得比赛，只有Bob删除任一个数字的情况下，剩下的数字异或之和为0.则我们假设轮到Bob的剩下所有数字的异或值为a，
则为了保证删除任一个数字的情况下，剩下的数字异或之和为0，则这些数字都要为a，并且轮到Bob的时候一定只剩下奇数个数，如果为偶数个，在Bob开始的时候就已经赢了。
同理，如果Bob要赢，则到Alice要为奇数个一样的数。这样我们可以看出，如果某个人会输，一定是在剩下奇数个数的时候，即在某个人开始的时候，如果有偶数个数，该人就不可能在这步被将死。
而某个人为奇数个数的时候，就算这步不被将死，则由于另一个人剩下偶数个数，不会被将死。这样递归，最后输的一定是奇数个数的持有人。所以只要Alice开始的时候是有偶数个数，她就不会输。
（一开局就赢的情况而外考虑）
！
 */