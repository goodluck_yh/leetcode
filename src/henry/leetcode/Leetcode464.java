package henry.leetcode;

/*
n the "100 game," two players take turns adding, to a running total, any integer from 1..10. The player who first causes the running total to reach or exceed 100 wins.

What if we change the game so that players cannot re-use integers?

For example, two players might take turns drawing from a common pool of numbers of 1..15 without replacement until they reach a total >= 100.

Given an integer maxChoosableInteger and another integer desiredTotal, determine if the first player to move can force a win, assuming both players play optimally.

You can always assume that maxChoosableInteger will not be larger than 20 and desiredTotal will not be larger than 300.
 */

public class Leetcode464 {

    public boolean canIWin(int maxChoosableInteger, int desiredTotal) {
        if(maxChoosableInteger * (maxChoosableInteger-1) < desiredTotal)
            return false;
        Boolean[] memory = new Boolean[1 << (maxChoosableInteger+1)];
        return helper(maxChoosableInteger, desiredTotal, 0, 0, memory);
    }

    private boolean helper(int choose, int total, int cur, int used, Boolean[] memo) {
        if(memo[used] != null)
            return memo[used];

        for(int i = choose; i > 0; i--) {
            if((used & (1 << i)) == 0) {
                if(cur + i >= total) {
                    memo[used] = true;
                    return true;
                }
                boolean res = !helper(choose, total, cur + i, used | (1 << i), memo);
                if(res) {
                    memo[used] = true;
                    return true;
                }
            }
        }
        memo[used] = false;
        return false;
    }
}

/*
这个题目虽然是medium，但是实际上是很好的题目。这个答案我是看了其他人的答案， 自己写不出来。总结如下：
- 如果发现递推方程不好写，那么考虑自顶向下。比如这个题目，当我们选择了某一个数字，这个数字不会放回，导致dp不好写
- 在自顶向下时，必须会用到hashmap，对于key，我们可以考虑用int，但我们看到一个数据集是32以下的时候，除了想到复杂度可能很高以外，要考虑转为int
- 再后来我们也不需要HashMap，因为已经是int作为key了， 所以直接可以用数组，这里有一个技巧，就是要声明称Boolean数组
 */