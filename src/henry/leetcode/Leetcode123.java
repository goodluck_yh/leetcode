package henry.leetcode;

public class Leetcode123 {
    public int maxProfit(int[] prices) {
        if(prices.length == 0)  return 0;
        int[] p1 = new int[prices.length];
        // including i, the max profix from 0 to i
        int max = -1, min = Integer.MAX_VALUE, profit = 0;
        for(int i = 0; i < p1.length; i++) {
            if(min > prices[i]) {
                min = prices[i];
                max = prices[i];
            }
            if(max < prices[i])
                max = prices[i];
            profit = Math.max(profit, max - min);
            p1[i] = profit;
        }

        int[] p2 = new int[prices.length];
        max = -1;
        min = Integer.MAX_VALUE;
        profit = 0;
        for(int i = p2.length-1; i >= 0; i--) {
            if(max < prices[i]) {
                min = prices[i];
                max = prices[i];
            }
            if(min > prices[i])
                min = prices[i];
            profit = Math.max(profit, max - min);
            p2[i] = profit;
        }

        max = p2[0];
        for(int i = 1; i < prices.length; i++) {
            max = Math.max(max, p1[i-1] + p2[i]);
        }
        return max;
    }
}

/*
这个解法很牛逼！
 */