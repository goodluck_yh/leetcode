package henry.leetcode;

import java.util.*;

/*
Find the length of the longest substring T of a given string (consists of lowercase letters only) such that every character in T appears no less than k times.

Example 1:

Input:
s = "aaabb", k = 3

Output:
3

The longest substring is "aaa", as 'a' is repeated 3 times.
 */
public class Leetcode395 {
    public int longestSubstring(String s, int k) {
        if(s.length() < k)  return 0;
        HashMap<Character, Integer> map = new HashMap<>();
        char[] chs = s.toCharArray();
        for(char ch : chs) {
            map.put(ch, map.getOrDefault(ch, 0) + 1);
        }
        Set<Character> blacklist = new HashSet<>();
        for(char ch : map.keySet()) {
            if(map.get(ch) < k) {
                blacklist.add(ch);
            }
        }
        if(blacklist.size() == 0)   return chs.length;
        int res = 0, cur = 0;
        for(int i = 0; i < chs.length; i++) {
            if(blacklist.contains(chs[i])) {
                res = Math.max(res, longestSubstring(s.substring(cur, i), k));
                cur = i + 1;
            }
        }
        res = Math.max(res, longestSubstring(s.substring(cur, chs.length), k));
        return res;
    }
}

/*
一开始没有想到dc的做法，这个也是string的一个新思路
有一个常见bug，如果for里面，写if，那么在for结束之后，可能还需要在做一次if里面的语句
复杂度0(n^2)

最多含有k个元素，使用的是滑动窗口
最少含有k个元素，使用的是dc
 */