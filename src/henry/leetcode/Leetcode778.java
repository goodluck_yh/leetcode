package henry.leetcode;

/*
On an N x N grid, each square grid[i][j] represents the elevation at that point (i,j).
Now rain starts to fall. At time t, the depth of the water everywhere is t. You can swim from a square to another 4-directionally adjacent square if and only if the elevation of both squares individually are at most t. You can swim infinite distance in zero time. Of course, you must stay within the boundaries of the grid during your swim.
You start at the top left square (0, 0). What is the least time until you can reach the bottom right square (N-1, N-1)?

Example 1:
Input: [[0,2],[1,3]]
Output: 3
Explanation:
At time 0, you are in grid location (0, 0).
You cannot go anywhere else because 4-directionally adjacent neighbors have a higher elevation than t = 0.

You cannot reach point (1, 1) until time 3.
When the depth of water is 3, we can swim anywhere inside the grid.
 */

import java.util.PriorityQueue;

public class Leetcode778 {
    int[] dx = new int[] {-1, 0, 1, 0};
    int[] dy = new int[] {0, 1, 0, -1};
    public int swimInWater(int[][] grid) {
        PriorityQueue<int[]> pq = new PriorityQueue<>((a,b)-> grid[a[0]][a[1]] - grid[b[0]][b[1]]);
        boolean[][] visited = new boolean[grid.length][grid[0].length];
        pq.add(new int[] {0, 0});
        visited[0][0] = true;
        int res = 0;
        while(pq.size() != 0) {
            int[] idx = pq.poll();
            if(res < grid[idx[0]][idx[1]])
                res = grid[idx[0]][idx[1]];
            if(idx[0] == grid.length-1 && idx[1] == grid[0].length-1)
                return res;
            for(int i = 0; i < 4; i++) {
                int x_new = idx[0] + dx[i];
                int y_new = idx[1] + dy[i];
                if(x_new >= 0 && x_new < grid.length && y_new >= 0 && y_new < grid.length && !visited[x_new][y_new]) {
                    pq.add(new int[] {x_new, y_new});
                    visited[x_new][y_new] = true;
                }
            }
        }
        return -1;
    }
}

/*
dfs
beat 65%
 */