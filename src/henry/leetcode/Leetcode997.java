package henry.leetcode;

/*
Input: N = 2, trust = [[1,2]]
Output: 2
 */
public class Leetcode997 {
    public int findJudge(int N, int[][] trust) {
        int[] in = new int[N], out = new int[N];
        for(int[] t: trust) {
            int a = t[0], b = t[1];
            in[b]++;
            out[a]++;
        }
        for(int i = 0; i < N; i++) {
            if(in[i] == N - 1 && out[i] == 0)
                return i;
        }
        return -1;
    }
}
