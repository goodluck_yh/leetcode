package henry.leetcode;

public class Leetcode1014 {
    public int shipWithinDays(int[] weights, int D) {
        int left = 0, right = 0;
        for(int w: weights) {
            left = Math.max(left, w);
            right += w;
        }

        while(left < right) {
            int mid = (left + right) / 2, cur = 0, need = 1;
            for(int w: weights) {
                if(cur + w < mid) {
                    cur += w;
                } else if(cur + w == mid) {
                    need++;
                    cur = 0;
                } else {
                    need++;
                    cur = w;
                }
            }
            if(need > D)    left = mid + 1;
            else if(need == D)  right = mid;
            else    right = mid;
        }
        return left;
    }
}
