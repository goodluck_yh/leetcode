package henry.leetcode;

/*
Let's call any (co
B.length >= 3
There exists some 0 < i < B.length - 1 such that B[0] < B[1] < ... B[i-1] < B[i] > B[i+1] > ... > B[B.length - 1]
(Note that B could be any subarray of A, including the entire array A.)
Given an array A of integers, return the length of the longest mountain.
Return 0 if there is no mountain.

Example 1:
Input: [2,1,4,7,3,2,5]
Output: 5
Explanation: The largest mountain is [1,4,7,3,2] which has length 5.

Example 2:
Input: [2,2,2]
Output: 0
Explanation: There is no mountain.

Note:
0 <= A.length <= 10000
0 <= A[i] <= 10000
Follow up:
Can you solve it using only one pass?
Can you solve it in O(1) space?
 */

public class Leetcode845 {
    public int longestMountain(int[] A) {
        if(A.length <= 2) return 0;
        int[] increase = new int[A.length];
        int[] decrease = new int[A.length];
        for(int i = 1; i < increase.length; i++) {
            if(A[i] <= A[i-1])  increase[i] = 0;
            else    increase[i] = increase[i-1] + 1;
        }

        for(int i = decrease.length-2; i >= 0; i--) {
            if(A[i] <= A[i+1])  decrease[i] = 0;
            else    decrease[i] = decrease[i+1] + 1;
        }
        int res = 0;
        for(int i = 0; i < A.length; i++) {
            if(increase[i] == 0 || decrease[i] == 0)    continue;
            res = Math.max(res, increase[i] + decrease[i] + 1);
        }
        return res;
    }
}

/*
思路： 如果题目要求找到一个值，比左右都要大，常见思路时左右循环各一遍
复杂度 O(n)
follow up 不好回答， 这个解法不是通解，也很难想
    public int longestMountain(int[] A) {
        int res = 0, up = 0, down = 0;
        for (int i = 1; i < A.length; ++i) {
            if (down > 0 && A[i - 1] < A[i] || A[i - 1] == A[i]) up = down = 0;
            if (A[i - 1] < A[i]) up++;
            if (A[i - 1] > A[i]) down++;
            if (up > 0 && down > 0 && up + down + 1 > res) res = up + down + 1;
        }
        return res;
    }
 */