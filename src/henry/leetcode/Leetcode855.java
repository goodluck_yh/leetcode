package henry.leetcode;

import java.util.*;

/*
In an exam room, there are N seats in a single row, numbered 0, 1, 2, ..., N-1.
When a student enters the room, they must sit in the seat that maximizes the distance to the closest person.  If there are multiple such seats, they sit in the seat with the lowest number.  (Also, if no one is in the room, then the student sits at seat number 0.)
Return a class ExamRoom(int N) that exposes two functions: ExamRoom.seat() returning an int representing what seat the student sat in, and ExamRoom.leave(int p) representing that the student in seat number p now leaves the room.  It is guaranteed that any calls to ExamRoom.leave(p) have a student sitting in seat p.

Example 1:
Input: ["ExamRoom","seat","seat","seat","seat","leave","seat"], [[10],[],[],[],[],[4],[]]
Output: [null,0,9,4,2,null,5]
Explanation:
ExamRoom(10) -> null
seat() -> 0, no one is in the room, then the student sits at seat number 0.
seat() -> 9, the student sits at the last seat number 9.
seat() -> 4, the student sits at the last seat number 4.
seat() -> 2, the student sits at the last seat number 2.
leave(4) -> null
seat() -> 5, the student sits at the last seat number 5.
​​​​​
Note:
1 <= N <= 10^9
ExamRoom.seat() and ExamRoom.leave() will be called at most 10^4 times across all test cases.
Calls to ExamRoom.leave(p) are guaranteed to have a student currently sitting in seat number p.
 */

public class Leetcode855 {
    private int seats;
    TreeSet<Integer> used = new TreeSet<>();
    public Leetcode855(int N) {
        seats = N-1;
    }

    public int seat() {
        if(used.size() == 0) {
            used.add(0);
            return 0;
        }

        int max = 0, i = -1;
        if(used.first() > max) {
            max = used.first();
            i = 0;
        }

        if(seats - used.last() > max) {
            max = seats - used.last();
            i = seats;
        }
        int pre = -1;
        for(int seat: used) {
            if(pre == -1) {
                pre = seat;
            } else {
                if((seat - pre) / 2 > max || ((seat - pre) / 2 == max && i == seats)) {
                    max = (seat - pre) / 2;
                    i = (seat - pre) / 2 + pre;
                }
                pre = seat;
            }
        }
        used.add(i);
        return i;
    }

    public void leave(int p) {
        used.remove(p);
    }
}

/*
看到N可以10^9,基本可以排除所有o(N)以上解法，后来仔细想一想，可以不可以利用线段来解题
我只要知道目前所有的座位，就可以知道他们的间隔，就可以计算出结果

复杂度：
    初始化: O(1)
    seat: O(n) n为元素个数
    leave: O(lgn) n为元素个数

TreeSet 操作： add, remove, first, last

看了一下答案，完全一样
 */
