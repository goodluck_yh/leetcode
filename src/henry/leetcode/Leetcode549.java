package henry.leetcode;

/*
Given a binary tree, you need to find the length of Longest Consecutive Path in Binary Tree.

Especially, this path can be either increasing or decreasing. For example, [1,2,3,4] and [4,3,2,1] are both considered valid, but the path [1,2,4,3] is not valid. On the other hand, the path can be in the child-Parent-child order, where not necessarily be parent-child order.

Example 1:
Input:
        1
       / \
      2   3
Output: 2
Explanation: The longest consecutive path is [1, 2] or [2, 1].
Example 2:
Input:
        2
       / \
      1   3
Output: 3
Explanation: The longest consecutive path is [1, 2, 3] or [3, 2, 1].
 */

public class Leetcode549 {
    class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }

    private int res = 1;
    public int longestConsecutive(TreeNode root) {
        if(root == null)
            return 0;
        helper(root);
        return res;
    }

    private int[] helper(TreeNode node) {
        if(node == null)    return new int[] {0, 0};
        if(node.left == null && node.right == null) return new int[] {1, 1};
        if(node.left == null) {
            int[] cnt = helper(node.right);
            if(node.val == node.right.val + 1) {
                int[] ret = new int[] {cnt[0] + 1, 1};
                res = Math.max(res, ret[0]);
                return ret;
            } else if (node.val + 1 == node.right.val) {
                int[] ret = new int[] {1, cnt[1] + 1};
                res = Math.max(res, ret[1]);
                return ret;
            } else
                return new int[] {1, 1};
        }
        if(node.right == null) {
            int[] cnt = helper(node.left);
            if(node.val == node.left.val + 1) {
                int[] ret = new int[] {cnt[0] + 1, 1};
                res = Math.max(res, ret[0]);
                return ret;
            } else if (node.val + 1 == node.left.val) {
                int[] ret = new int[] {1, cnt[1] + 1};
                res = Math.max(res, ret[1]);
                return ret;
            } else
                return new int[] {1, 1};
        }

        int[] cnt_l = helper(node.left);
        int[] cnt_r = helper(node.right);

        if(node.val - 1 == node.left.val && node.val + 1 == node.right.val) {
            res = Math.max(res, cnt_l[0] + 1 + cnt_r[1]);
            int[] ret = new int[] {cnt_l[0] + 1, 1 + cnt_r[1]};
            return ret;
        }
        if(node.val + 1 == node.left.val && node.val - 1 == node.right.val) {
            res = Math.max(res, cnt_l[1] + 1 + cnt_r[0]);
            int[] ret = new int[] {cnt_r[0] + 1, 1 + cnt_l[1]};
            return ret;
        }

        if(node.val - 1 == node.left.val && node.val - 1 == node.right.val) {
            int bigger = Math.max(cnt_l[0], cnt_r[0]);
            res = Math.max(res, bigger+1);
            int[] ret = new int[] {bigger + 1, 1};
            return ret;
        }

        if(node.val + 1 == node.left.val && node.val + 1 == node.right.val) {
            int bigger = Math.max(cnt_l[1], cnt_r[1]);
            res = Math.max(res, bigger + 1);
            int[] ret = new int[] {1, bigger + 1};
            return ret;
        }

        if(node.val - 1 == node.left.val) {
            res = Math.max(res, cnt_l[0] + 1);
            int[] ret = new int[] {cnt_l[0] + 1, 1};
            // if(node.val == 22 && node.left.val == 21 && node.right.val == 21)
            //     System.out.println(node.val + " " + ret[0] + " " + ret[1]);
            return ret;
        }

        if(node.val + 1 == node.left.val) {
            res = Math.max(res, cnt_l[1] + 1);
            int[] ret = new int[] {1, cnt_l[1] + 1};
            return ret;
        }

        if(node.val - 1 == node.right.val) {
            res = Math.max(res, cnt_r[0] + 1);
            int[] ret = new int[] {cnt_r[0] + 1, 1};
            return ret;
        }

        if(node.val + 1 == node.right.val) {
            res = Math.max(res, cnt_r[1] + 1);
            int[] ret = new int[] {1, cnt_r[1] + 1};
            return ret;
        }
        return new int[] {1, 1};
    }
}

/*
以上我的解法，很麻烦！
思路没问题，下面解法也是一个思路，但是好些很多！
 */

class Leetcode549_2 {
    class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }
    int max = 0;
    public int longestConsecutive(TreeNode root) {
        helper(root);
        return max;
    }
    public int[] helper(TreeNode root) {
        if (root == null) {
            return new int[2];
        }
        int[] left = helper(root.left);
        int[] right = helper(root.right);
        int inc = 1, dec = 1;
        if (root.left != null) {
            if (root.val - root.left.val == 1) {
                inc = left[0] + 1;
            } else if(root.left.val - root.val == 1) {
                dec = left[1] + 1;
            }
        }
        if (root.right != null) {
            if (root.val - root.right.val == 1) {
                inc = Math.max(inc, right[0] + 1);
            } else if (root.right.val - root.val == 1) {
                dec = Math.max(dec, right[1] + 1);
            }
        }
        max = Math.max(max, inc + dec - 1);
        return new int[]{inc, dec};
    }
}