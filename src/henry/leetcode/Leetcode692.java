package henry.leetcode;

import java.util.*;

public class Leetcode692 {
    public List<String> topKFrequent(String[] words, int k) {
        List<Set<String>> list = new LinkedList<>();
        HashMap<String, Integer> map = new HashMap<>();

        for(String s: words) {
            if(!map.containsKey(s)) {
                map.put(s, 1);
                if(list.size() < 1) {
                    list.add(new HashSet<>());
                }
                list.get(0).add(s);
            } else {
                int fre = map.get(s);
                map.put(s, fre+1);
                list.get(fre-1).remove(s);
                if(list.size() < fre+1)
                    list.add(new HashSet<>());
                list.get(fre).add(s);
            }
        }
        List<String> res = new LinkedList<>();
        for(int i = list.size() - 1; i >= 0; i--) {
            List<String> temp = new ArrayList();
            temp.addAll(list.get(i));
            Collections.sort(temp);
            res.addAll(temp);
            if(res.size() >= k)
                break;
        }
        while(res.size() != k) {
            res.remove(res.size() - 1);
        }
        return res;
    }
}

/*
我的这个做法其实就是桶排序，除了桶排序，常见做法还有利用priority queue
 */