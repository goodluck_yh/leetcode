package henry.leetcode;

import java.util.*;

public class Leetcode973 {
    public int[][] kClosest(int[][] points, int K) {
        PriorityQueue<int[]> pq = new PriorityQueue<>((b,a) -> a[0]*a[0] + a[1]*a[1] - b[0]*b[0] - b[1]*b[1]);
        for(int[] point : points) {
            pq.offer(point);
            if(pq.size () > K) {
                int[] temp = pq.poll();
                System.out.println(temp[0] + " " + temp[1]);
            }
        }
        int[][] res = new int[K][2];
        for(int i = 0; i < K; i++) {
            res[i] = pq.poll();
        }
        return res;
    }
}
