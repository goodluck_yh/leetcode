package henry.leetcode;

import java.util.*;

public class Leetcode787 {
    class Flight {
        int des;
        int price;

        public Flight(int des, int price) {
            this.des = des;
            this.price = price;
        }
    }

    public int findCheapestPrice(int n, int[][] flights, int src, int dst, int K) {
        // src -> fligh
        Map<Integer, List<Flight>> map = new HashMap<>();

        for(int[] info: flights) {
            if(!map.containsKey(info[0])) {
                map.put(info[0], new LinkedList<>());
            }
            map.get(info[0]).add(new Flight(info[1], info[2]));
        }

        int[] cost = new int[n];
        Arrays.fill(cost, Integer.MAX_VALUE);
        cost[src] = 0;

        List<Integer> list = new LinkedList<>();
        list.add(src);
        for(int i = 0; i <= K; i++) {
            List<Integer> next = new LinkedList<>();
            int[] pre = new int[n];
            for(int j = 0; j < n; j++) {
                pre[j] = cost[j];
            }
            for(int s: list) {
                List<Flight> neis = map.get(s);

                if(neis != null && neis.size() != 0){
                    for(Flight f: neis) {
                        int cur = pre[s] + f.price;
                        if(cur < cost[f.des]) {
                            cost[f.des] = cur;
                            next.add(f.des);
                        }
                    }
                }

            }
            list = next;
        }
        return cost[dst] == Integer.MAX_VALUE ? -1 : cost[dst];
    }
}

/*
我这个题目的解法是BFS，原因我是看到最多为K步，所以很容易想到BFS
但是BFS复杂度不好想。
讨论区也没有看到复杂度
最坏复杂度是O(n^k)

注意：有一类bug是根据前一个数组计算下一个数组，一定要防止前一个数组可变！

最短路劲问题： https://www.cnblogs.com/godfray/p/4077146.html
 */