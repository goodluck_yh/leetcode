package henry.leetcode;

/*
You are given an array of positive and negative integers. If a number n at an index is positive, then move forward n steps. Conversely, if it's negative (-n), move backward n steps. Assume the first element of the array is forward next to the last element, and the last element is backward next to the first element. Determine if there is a loop in this array. A loop starts and ends at a particular index with more than 1 element along the loop. The loop must be "forward" or "backward'.

Example 1: Given the array [2, -1, 1, 2, 2], there is a loop, from index 0 -> 2 -> 3 -> 0.
 */
public class Leetcode457 {
    public boolean circularArrayLoop(int[] nums) {
        int len = nums.length;
        for(int i = 0; i < len; i++) {
            if(nums[i] == 0)
                continue;
            int slow = i, fast = i, cnt = 0;
            boolean isPos = nums[i] > 0;
            do {
                int tempSlow = slow;
                slow = (nums[slow] + slow + len) % len;

                if((isPos && nums[fast] <= 0) || (!isPos && nums[fast] >= 0))
                    break;
                fast = (nums[fast] + fast + len) % len;
                if((isPos && nums[fast] <= 0) || (!isPos && nums[fast] >= 0))
                    break;
                fast = (nums[fast] + fast + len) % len;
                cnt++;
            } while(slow != fast);
            if( slow == fast && cnt > 1)
                return true;

            slow = i;
            while(nums[slow] != nums[fast]) {
                int temp = slow;
                slow = (nums[slow] + slow + len) % len;
                nums[temp] = 0;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        Leetcode457 leetcode457 = new Leetcode457();
        leetcode457.circularArrayLoop(new int[] {2, -1, 1, 2, 2});
    }
}

/*
这个题目没想到，最后用的是快慢指针。
在实现的时候，想一遍设置0，一边遍历，发现不行！
 */