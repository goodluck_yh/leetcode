package henry.leetcode;

public class Leetcode964 {
    public int leastOpsExpressTarget(int x, int target) {
        return helper(x, target) - 1;
    }

    private int helper(int x, int target) {
        if(target == 0) return 0;
        if(target == 1) return 2;
        if(target == x) return 1;
        if(target < x) {
            return Math.min(2 * target, 1 + 2 * (x - target));
        }
        int num = (int)(Math.log(target) / Math.log(x));
        int temp = x;
        for(int i = 1; i < num; i++) {
            temp = temp * x;
        }

        int gap1 = target - temp;
        int gap2 = temp * x - target;
        if(gap2 < target)
            return Math.min(num + helper(x, gap1), num + 1 + helper(x, gap2));
        else
            return num + helper(x, gap1);
    }

    public static void main(String[] args) {
        Leetcode964 leetcode = new Leetcode964();
        System.out.println(leetcode.leastOpsExpressTarget(100, 100000000));
    }
}
