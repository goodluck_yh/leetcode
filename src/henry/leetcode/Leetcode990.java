package henry.leetcode;

import java.util.*;

public class Leetcode990 {
    class UnionFind {
        int[] p;

        UnionFind() {
            p = new int[26];
            for(int i = 0; i < 26; i++)
                p[i] = i;
        }

        void union(char a, char b) {
            int pa = find(a), pb = find(b);
            if(pa != pb)
                p[pa] = pb;
        }

        int find(char a) {
            int ia = a - 'a';
            while(p[ia] != ia) {
                ia = p[ia];
            }
            return ia;
        }
    }

    public boolean equationsPossible(String[] equations) {
        UnionFind uf = new UnionFind();
        List<String> list = new LinkedList<>();
        for(int i = 0; i < equations.length; i++) {
            char a = equations[i].charAt(0), b = equations[i].charAt(3);
            String op = equations[i].substring(1, 3);
            if(op.equals("!=")) {
                list.add(a + "" + b);
            } else {
                uf.union(a, b);
            }
        }

        for(String e: list) {
            char a = e.charAt(0), b = e.charAt(1);
            int ia = uf.find(a), ib = uf.find(b);
            if(ia == ib)
                return false;
        }
        return true;
    }
}
