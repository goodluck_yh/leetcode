package henry.leetcode;

import java.util.*;

public class Leetcode974 {
    public int subarraysDivByK(int[] A, int K) {
        int[] sum = new int[A.length];
        int res = 0;
        HashMap<Integer, Integer> map = new HashMap<>();

        for(int i = 0; i < A.length; i++) {
            if(i == 0) {
                sum[0] = A[0];
            } else
                sum[i] = sum[i-1] + A[i];
            int rem = Math.abs((sum[i] % K + K) % K);
            map.put(rem, map.getOrDefault(rem, 0) + 1);
        }

        for(int i: map.keySet()) {
            if(i == 0) {
                int num = map.get(i);
                int temp = 0;
                for(int j = 1; j <= num; j++)   temp = temp + j;
                res += temp;
            } else {
                int num = map.get(i);
                int temp = 0;
                for(int j = 1; j < num; j++)   temp = temp + j;
                res +=temp;
            }
        }

        return res;
    }
}
