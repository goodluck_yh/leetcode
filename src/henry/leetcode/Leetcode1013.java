package henry.leetcode;

public class Leetcode1013 {
    public int numPairsDivisibleBy60(int[] time) {
        int[] cnt = new int[60];
        for(int i = 0; i < time.length; i++) {
            cnt[time[i] % 60]++;
        }
        int res = 0;
        for(int i = 1; i < 30; i++) {
            res = res + cnt[i] * cnt[60 - i];
        }
        res = res + (cnt[0]-1) * cnt[0] / 2;
        res = res + (cnt[30]-1) * cnt[30] / 2;
        return res;
    }
}
