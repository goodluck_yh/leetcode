package henry.leetcode;

/*
Input: [8,5,1,7,10,12]
Output: [8,5,10,1,7,null,12]
 */

import java.util.*;
public class Leetcode1008 {
    class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }

    public TreeNode bstFromPreorder(int[] preorder) {
        List<Integer> pd = new LinkedList<>();
        for(int i = 0; i < preorder.length; i++) {
            pd.add(preorder[i]);
        }
        return helper(pd);
    }

    private TreeNode helper(List<Integer> pd) {
        if(pd.size() == 0)
            return null;
        int val = pd.get(0);
        TreeNode node = new TreeNode(val);
        List<Integer> left = new LinkedList<>();
        List<Integer> right = new LinkedList<>();
        for(int num: pd) {
            if(num < val)
                left.add(num);
            else
                right.add(num);
        }
        node.left = helper(left);
        node.right = helper(right);
        return node;
    }
}
