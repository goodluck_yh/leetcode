package henry.leetcode;

/*
Input: 10
Output: 12
Explanation: 12 = 10 * 9 / 8 + 7 - 6 * 5 / 4 + 3 - 2 * 1
 */
public class Leetcode1006 {
    public int clumsy(int N) {
        if(N == 1)
            return 1;
        if(N == 2)
            return 2;
        if(N == 3)
            return 6;
        int firstTwoOp = N * (N - 1) / (N - 2);
        return firstTwoOp + helper(N - 3);
    }

    private int helper(int n) {
        if(n == 0)
            return 0;
        if(n == 1)
            return 1;
        else if(n == 2)
            return 2 - 1;
        else if(n == 3)
            return 3 - 2 * 1;
        else if(n == 4)
            return 4 - 3 * 2 / 1;
        return n - (n-1) * (n-2) / (n-3) + helper(n-4);
    }
}
