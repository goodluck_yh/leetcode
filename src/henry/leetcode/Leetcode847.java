package henry.leetcode;

import java.util.*;

/*
Input: [[1,2,3],[0],[0],[0]]
Output: 4
Explanation: One possible path is [1,0,2,0,3]
 */
public class Leetcode847 {
    class State {
        int node;
        int nodes;
        State(int node, int nodes) {
            this.node = node;
            this.nodes = nodes;
        }
    }

    public int shortestPathLength(int[][] graph) {
        int step = 0, N = graph.length;
        LinkedList<State> list = new LinkedList<>();
        boolean[][] visited = new boolean[N][1 << N];
        for(int i = 0; i < N; i++) {
            list.add(new State(i, (1 << i)));
            visited[i][1 << i] = true;
        }
        while(!list.isEmpty()) {
            int size = list.size();
            while(size-- > 0) {
                State state = list.poll();
                int node = state.node, nodes = state.nodes;
                if(nodes == (1 << N) - 1)
                    return step;
                for(int nei: graph[node]) {
                    int nextNodes = nodes | (1 << nei);
                    if(!visited[nei][nextNodes]) {
                        visited[nei][nextNodes] = true;
                        list.add(new State(nei, nextNodes));
                    }
                }
            }
            step++;
        }
        return -1;
    }

    public static void main(String[] args) {
        Leetcode847 leetcode = new Leetcode847();
        System.out.println(leetcode.shortestPathLength(new int[][] {{1,2,3},{0},{0},{0}}));
    }
}


/*
int N = graph.length, total = (1 << N);
        int[][] dp = new int[total][N];
        for(int i = 0; i < total; i++) {
            for(int j = 0; j < N; j++) {
                dp[i][j] = Integer.MAX_VALUE;
            }
        }
        for(int i = 0; i < N; i++) {
            dp[(1 << i)][i] = 0;
        }

        HashMap<Integer, HashSet<Integer>> edges = new HashMap<>();
        for(int i = 0; i < graph.length; i++) {
            for(int j = 0; j < graph[i].length; j++) {
                int start = i, end = graph[i][j];
                if(!edges.containsKey(start))   edges.put(start, new HashSet<>());
                edges.get(start).add(end);
            }
        }

        for(int i = 0; i < total; i++) {
            List<Integer> curNodes = new LinkedList<>();
            int ii = i, bit = 0;
            while(ii > 0) {
                if((ii & 1) == 1) {
                    curNodes.add(bit);
                }
                ii = ii >> 1;
                bit++;
            }
            if(curNodes.size() == 1)    continue;
            for(int x = 0; x < curNodes.size(); x++) {
                for(int y = x+1; y < curNodes.size(); y++ ) {
                    int m = curNodes.get(x), n = curNodes.get(y);
                    if(edges.get(m).contains(n)) {
                        ii = i - (1 << n);
                        if(dp[ii][m] != Integer.MAX_VALUE) {
                            dp[i][n] = dp[ii][m] + 1;
                        }
                        ii = i - (1 << m);
                        if(dp[ii][n] != Integer.MAX_VALUE) {
                            dp[i][m] = dp[ii][n] + 1;
                        }
                    }
                }
            }
        }
        int res = Integer.MAX_VALUE;
        for(int i = 0; i < N; i++) {
            res = Math.min(res, dp[total-1][i]);
        }
        return res;
 */