package henry.leetcode;

import java.util.Arrays;

/*
To some string S, we will perform some replacement operations that replace groups of letters with new ones (not necessarily the same size).
Each replacement operation has 3 parameters: a starting index i, a source word x and a target word y.  The rule is that if x starts at position i in the original string S, then we will replace that occurrence of x with y.  If not, we do nothing.
For example, if we have S = "abcd" and we have some replacement operation i = 2, x = "cd", y = "ffff", then because "cd" starts at position 2 in the original string S, we will replace it with "ffff".
Using another example on S = "abcd", if we have both the replacement operation i = 0, x = "ab", y = "eee", as well as another replacement operation i = 2, x = "ec", y = "ffff", this second operation does nothing because in the original string S[2] = 'c', which doesn't match x[0] = 'e'.
All these operations occur simultaneously.  It's guaranteed that there won't be any overlap in replacement: for example, S = "abc", indexes = [0, 1], sources = ["ab","bc"] is not a valid test case.

Example 1:
Input: S = "abcd", indexes = [0,2], sources = ["a","cd"], targets = ["eee","ffff"]
Output: "eeebffff"
Explanation: "a" starts at index 0 in S, so it's replaced by "eee".
"cd" starts at index 2 in S, so it's replaced by "ffff".

Example 2:
Input: S = "abcd", indexes = [0,2], sources = ["ab","ec"], targets = ["eee","ffff"]
Output: "eeecd"
Explanation: "ab" starts at index 0 in S, so it's replaced by "eee".
"ec" doesn't starts at index 2 in the original S, so we do nothing.

Notes:
0 <= indexes.length = sources.length = targets.length <= 100
0 < indexes[i] < S.length <= 1000
All characters in given inputs are lowercase letters.
 */

public class Leetcode833 {
    class Info {
        int index;
        String source;
        String target;
        public Info(int index, String source, String target){
            this.index = index;
            this.source = source;
            this.target = target;
        }
    }

    public String findReplaceString(String S, int[] indexes, String[] sources, String[] targets) {
        if(indexes.length == 0) return S;
        Info[] infos = new Info[indexes.length];
        for(int i = 0; i < indexes.length; i++) {
            infos[i] = new Info(indexes[i], sources[i], targets[i]);
        }
        Arrays.sort(infos, (a, b) -> a.index - b.index);
        for(int i = 0; i < indexes.length; i++) {
            indexes[i] = infos[i].index;
            sources[i] = infos[i].source;
            targets[i] = infos[i].target;
        }
        StringBuffer sb = new StringBuffer();
        int cur = 0;
        for(int i = 0; i < indexes.length;) {
            if(indexes[i] > cur) {
                sb.append(S.substring(cur, indexes[i]));
                cur = indexes[i];
            } else {
                String source = sources[i];
                int len = source.length();
                if(indexes[i] + len <= S.length()) {
                    if(S.substring(indexes[i], indexes[i]+len).equals(source)) {
                        sb.append(targets[i]);
                    } else {
                        sb.append(S.substring(indexes[i], indexes[i] + len));
                    }
                    cur = indexes[i] + len;
                } else {
                    break;
                }
                i++;
            }
        }
        if(cur != S.length())
            sb.append(S.substring(cur, S.length()));
        return sb.toString();
    }

    public String findReplaceString1(String S, int[] indexes, String[] sources, String[] targets) {
        int[] match = new int[S.length()];
        Arrays.fill(match, -1);
        for(int i = 0; i < indexes.length; i++) {
            if(indexes[i] + sources[i].length() <= S.length() && S.substring(indexes[i], indexes[i] + sources[i].length()).equals(sources[i])) {
                match[indexes[i]] = i;
            }
        }

        StringBuffer sb = new StringBuffer();
        for(int i = 0; i < S.length(); ) {
            if(match[i] != -1) {
                sb.append(targets[match[i]]);
                i = i + sources[match[i]].length();
            } else {
                sb.append(S.charAt(i));
                i++;
            }
        }
        return sb.toString();
    }
}

/*
我自己的版本，先排序，在按照顺序构造string
复杂度O(NlgN + M*N)

官方版本：声明一个数组（和S大小相等），记录这一位是否要替换
复杂度O(M*N)
 */