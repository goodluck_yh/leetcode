package henry.leetcode;

/*
On a horizontal number line, we have gas stations at positions stations[0], stations[1], ..., stations[N-1], where N = stations.length.
Now, we add K more gas stations so that D, the maximum distance between adjacent gas stations, is minimized.
Return the smallest possible value of D.

Example:
Input: stations = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], K = 9
Output: 0.500000

Note:
stations.length will be an integer in range [10, 2000].
stations[i] will be an integer in range [0, 10^8].
K will be an integer in range [1, 10^6].
Answers within 10^-6 of the true value will be accepted as correct.
 */
public class Leetcode774 {
    public double minmaxGasDist(int[] stations, int K) {
        double lo = 0.000001, hi = 0;
        for(int i = 1; i < stations.length; i++) {
            hi = Math.max(hi, stations[i]-stations[i-1]);
        }
        while((hi - lo) > 0.000001) {
            double mid = (lo + hi) / 2;
            int cnt = 0;
            for(int i = 1; i < stations.length; i++) {
                double gap = stations[i] - stations[i-1];
                int c = (int)(gap / mid);
                cnt += c;
            }
            if(cnt > K) {
                lo = mid;
            } else {
                hi = mid;
            }
        }
        return lo;
    }
}

/*
这个题目二分查找，想法不难，但是bug free不容易
binary search 比较适合难于直接计算，但是验证比较方便的题目！
 */
