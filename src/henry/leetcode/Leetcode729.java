package henry.leetcode;

import java.util.*;

public class Leetcode729 {
    TreeSet<int[]> map = new TreeSet<>((a, b) -> a[0] - b[0]);

    public Leetcode729() {

    }

    public boolean book(int start, int end) {
        int[] book = new int[] {start, end};
        int[] floor = map.floor(book), ceil = map.ceiling(book);
        if(floor != null && floor[1] > book[0]) return false;
        if(ceil != null && ceil[0] < book[1])   return false;
        map.add(book);
        return true;
    }
}

/*
上面是TreeSet的做法，主要是利用了TreeSet 的floor and ceiling 方法
复杂度O(lgn)
看了答案，最快的算法是手动建立一颗树，速度很快！

这个题目是线段插入等一类题目的代表.
 */