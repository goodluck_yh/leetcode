package henry.leetcode;

import java.util.*;

/*
Given a blacklist B containing unique integers from [0, N), write a function to return a uniform random integer from [0, N) which is NOT in B.
Optimize it such that it minimizes the call to system’s Math.random().

Note:
1 <= N <= 1000000000
0 <= B.length < min(100000, N)
[0, N) does NOT include N. See interval notation.

Example 1:
Input:
["Solution","pick","pick","pick"]
[[1,[]],[],[],[]]
Output: [null,0,0,0]

Example 2:
Input:
["Solution","pick","pick","pick"]
[[2,[]],[],[],[]]
Output: [null,1,1,1]
 */
public class Leetcode710 {
    int n;
    int[] bl;
    Random rand;
    public Leetcode710(int N, int[] blacklist) {
        n = N - blacklist.length;
        bl = blacklist;
        Arrays.sort(bl);
        rand = new Random();
    }

    public int pick() {
        int target = rand.nextInt(n), start = target, end = n + bl.length - 1;
        while(start < end) {
            int mid = (start + end) / 2;
            int idx = Arrays.binarySearch(bl, mid), flag = idx;
            if(idx < 0)
                idx = -(idx + 1);
            int actual = mid - idx;
            if(actual == target) {
                if(flag < 0)
                    return mid;
                else
                    start = mid + 1;
            } else if(actual > target) {
                end = mid - 1;
            } else
                start = mid + 1;
        }
        return start;
    }
}

/*
我自己的做法是random一个数字，该数字范围是0-有效数字个数（N-blacklist 长度）
然后进行二分查找
 */

class Solution {

    int M;
    Random r;
    Map<Integer, Integer> map;

    public Solution(int N, int[] blacklist) {
        map = new HashMap();
        for (int b : blacklist) // O(B)
            map.put(b, -1);
        M = N - map.size();

        for (int b : blacklist) { // O(B)
            if (b < M) { // re-mapping
                while (map.containsKey(N - 1))
                    N--;
                map.put(b, N - 1);
                N--;
            }
        }

        r = new Random();
    }

    public int pick() {
        int p = r.nextInt(M);
        if (map.containsKey(p))
            return map.get(p);
        return p;
    }
}

/*
上面这个解法我是看别人的！
这个解法做了个部分map，很好的思路！
我自己的做法是利用了bs，相对来说比较复杂

这个做法是置换法
1 找出实际长度（N-blacklist长度）
2 对于在blacklist中，小于实际长度的数字进行替换，替换方法为：有一个指针从后往前找，找出第一个不在blacklist中的数字，进行替换
 */