package henry.leetcode;

/*
An integer interval [a, b] (for integers a < b) is a set of all consecutive integers from a to b, including a and b.
Find the minimum size of a set S such that for every integer interval A in intervals, the intersection of S with A has size at least 2.

Example 1:
Input: intervals = [[1, 3], [1, 4], [2, 5], [3, 5]]
Output: 3
Explanation:
Consider the set S = {2, 3, 4}.  For each interval, there are at least 2 elements from S in the interval.
Also, there isn't a smaller size set that fulfills the above condition.
Thus, we output the size of this set, which is 3.
 */

import java.util.*;

public class Leetcode757 {
    public int intersectionSizeTwo(int[][] intervals) {
        Arrays.sort(intervals, (a,b) -> a[1] == b[1] ? b[0] - a[0] : a[1] - b[1]);
        int p1 = -1, p2 = -1, res = 0;
        for(int[] interval: intervals) {
            if(p1 == -1 && p2 == -1) {
                p1 = interval[interval.length - 2];
                p2 = interval[interval.length - 1];
                res += 2;
            } else {
                if(p1 >= interval[0]) {
                    continue;
                } else if (p2 >= interval[0]) {
                    p1 = p2;
                    p2 = interval[interval.length-1];
                    res++;
                } else {
                    p1 = interval[interval.length - 2];
                    p2 = interval[interval.length - 1];
                    res += 2;
                }
            }
        }
        return res;
    }
}

/*
beat 70%
对于二维数组其中第二维长度是2， 有一个一般性的解法就是按照第二维升序，第一维降序。
另一个相似的题目Russian Doll Envelopes
 */
