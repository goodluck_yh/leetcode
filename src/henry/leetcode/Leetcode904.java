package henry.leetcode;

/*
In a row of trees, the i-th tree produces fruit with type tree[i].
You start at any tree of your choice, then repeatedly perform the following steps:
Add one piece of fruit from this tree to your baskets.  If you cannot, stop.
Move to the next tree to the right of the current tree.  If there is no tree to the right, stop.
Note that you do not have any choice after the initial choice of starting tree: you must perform step 1, then step 2, then back to step 1, then step 2, and so on until you stop.
You have two baskets, and each basket can carry any quantity of fruit, but you want each basket to only carry one type of fruit each.
What is the total amount of fruit you can collect with this procedure?

Example 1:
Input: [1,2,1]
Output: 3
Explanation: We can collect [1,2,1].

Example 2:
Input: [0,1,2,2]
Output: 3
Explanation: We can collect [1,2,2].
If we started at the first tree, we would only collect [0, 1].
Example 3:

Input: [1,2,3,2,2]
Output: 4
Explanation: We can collect [2,3,2,2].
If we started at the first tree, we would only collect [1, 2].

Example 4:
Input: [3,3,3,1,2,1,1,2,3,3,4]
Output: 5
Explanation: We can collect [1,2,1,1,2].
If we started at the first tree or the eighth tree, we would only collect 4 fruits.

Note:
1 <= tree.length <= 40000
0 <= tree[i] < tree.length
 */

public class Leetcode904 {
    public int totalFruit(int[] tree) {
        if (tree.length <= 2) return tree.length;
        int type1 = -1, type2 = -1, i = -1, res = 2;
        for (int idx = 0; idx < tree.length; idx++) {
            if (tree[idx] == type1 || tree[idx] == type2)
                continue;
            if (type1 == -1) {
                type1 = tree[idx];
                i = idx;
            } else if (type2 == -1) {
                type2 = tree[idx];
            } else {
                res = Math.max(res, idx - i);
                int j = idx - 1;
                while (j >= 0 && tree[j] == tree[idx - 1]) j--;
                i = j + 1;
                if (i == idx) {
                    type1 = tree[idx];
                    type2 = -1;
                } else {
                    type1 = tree[idx - 1];
                    type2 = tree[idx];
                }

            }
        }
        return Math.max(res, tree.length - i);
    }

    public static void main(String[] args) {
        Leetcode904 leetcode = new Leetcode904();
        System.out.println(leetcode.totalFruit(new int[] {1,2,3,2,2}));
    }
}

/*
思路正确，但是bug很多，其实一个不太容易想到的bug是当出现第三个数字，保留的那个type其实不确定是type1 还是 type2
 */
