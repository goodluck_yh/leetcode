package henry.leetcode;

public class Leetcode995 {
    /*
    greedy
    A = [0,0,0,1,0,1,1,0], K = 3

     */
    public int minKBitFlips(int[] A, int K) {
        int res = 0;
        for(int i = 0; i + K - 1< A.length; i++) {
            if(A[i] == 1) {
                continue;
            }
            res++;
            flip(A, K, i);
        }

        for(int i = 0; i < K; i++) {
            if(A[A.length - 1 - i] == 0)
                return -1;
        }
        return res;
    }

    private void flip(int[] A, int K, int start) {
        for(int i = 0; i < K; i++) {
            if(A[start + i] == 1)
                A[start + i] = 0;
            else
                A[start + i] = 1;
        }
    }
}
