package henry.leetcode;

/*
Given many words, words[i] has weight i.
Design a class WordFilter that supports one function, WordFilter.f(String prefix, String suffix). It will return the word with given prefix and suffix with maximum weight. If no word exists, return -1.

Examples:
Input:
WordFilter(["apple"])
WordFilter.f("a", "e") // returns 0
WordFilter.f("b", "") // returns -1
Note:
words has length in range [1, 15000].
For each test case, up to words.length queries WordFilter.f may be made.
words[i] has length in range [1, 10].
prefix, suffix have lengths in range [0, 10].
words[i] and prefix, suffix queries consist of lowercase letters only.
 */

public class Leetcode745 {
    public static void main(String[] args) {
        WordFilter wordFilter = new WordFilter(new String[]{"bbabbababa"});
        System.out.println(wordFilter.f("babbab", ""));
    }
}

class WordFilter {

    class Trie {
        Trie[] children = new Trie[256];
        int[] weight = new int[256];
        Trie() {
            for(int i = 0; i < 256; i ++) {
                weight[i] = -1;
            }
        }
    }

    Trie root = new Trie();

    public WordFilter(String[] words) {
        for(int i = 0; i < words.length; i++) {
            String word = words[i];
            insert(word, i);
            String others = "#" + word;
            insert(others, i);
            for(int j = word.length()-1; j >= 0; j--) {
                others = word.charAt(j) + others;
                insert(others, i);
            }
        }
    }

    private void insert(String word, int weight) {
        Trie cur = root;
        for(int i = 0; i < word.length(); i++) {
            cur.weight[word.charAt(i)] = weight;
            if(cur.children[word.charAt(i)] == null) {
                cur.children[word.charAt(i)] = new Trie();
            }
            cur = cur.children[word.charAt(i)];
        }
    }

    public int f(String prefix, String suffix) {
        String target = suffix + "#" + prefix;
        Trie cur = root;
        int res = 0;
        for(int i = 0; i < target.length(); i++) {
            if(cur.weight[target.charAt(i)] == -1) {
                return -1;
            }
            res = cur.weight[target.charAt(i)];
            cur = cur.children[target.charAt(i)];
        }
        return res;
    }
}

/*
trie 的一个技巧，在寻找后缀的时候，我们可以把后缀变成前缀，避免使用两个set 求交集
 */
