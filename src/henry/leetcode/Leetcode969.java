package henry.leetcode;

import java.util.*;

public class Leetcode969 {
    public List<Integer> pancakeSort(int[] A) {
        List<Integer> res = new LinkedList<>();
        for(int i = A.length; i >= 1; i--) {
            int index = 0;
            while(index < i && A[index] != i)
                index++;
            if(index + 1 == i)  continue;
            res.add(index+1);
            res.add(i);
            reverse(A, index+1);
            reverse(A, i);
        }
        return res;
    }

    private void reverse(int[] A, int len) {
        for (int i = 0, j = len - 1; i < j; i++, j--) {
            int t = A[i];
            A[i] = A[j];
            A[j] = t;
        }
    }
}

/*
这个题目感觉很难，其实是没有找到正确的方法。
如果要移动的话，可以找到最大值，翻转，然后在整体翻转即可。例如
[3,2,4,1] => [4,2,3,1] => [1,3,2,4]
 */