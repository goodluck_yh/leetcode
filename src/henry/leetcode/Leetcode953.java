package henry.leetcode;

import java.util.Arrays;
import java.util.Comparator;

/*
In an alien language, surprisingly they also use english lowercase letters, but possibly in a different order. The order of the alphabet is some permutation of lowercase letters.
Given a sequence of words written in the alien language, and the order of the alphabet, return true if and only if the given words are sorted lexicographicaly in this alien language.

Example 1:
Input: words = ["hello","leetcode"], order = "hlabcdefgijkmnopqrstuvwxyz"
Output: true
Explanation: As 'h' comes before 'l' in this language, then the sequence is sorted.

Example 2:
Input: words = ["word","world","row"], order = "worldabcefghijkmnpqstuvxyz"
Output: false
Explanation: As 'd' comes after 'l' in this language, then words[0] > words[1], hence the sequence is unsorted.

Example 3:
Input: words = ["apple","app"], order = "abcdefghijklmnopqrstuvwxyz"
Output: false
Explanation: The first three characters "app" match, and the second string is shorter (in size.) According to lexicographical rules "apple" > "app", because 'l' > '∅', where '∅' is defined as the blank character which is less than any other character (More info).

Note:
1 <= words.length <= 100
1 <= words[i].length <= 20
order.length == 26
All characters in words[i] and order are english lowercase letters.
 */

public class Leetcode953 {
    public boolean isAlienSorted(String[] words, String order) {
        String[] sortWords = new String[words.length];
        for(int i = 0; i < words.length; i++) {
            sortWords[i] = words[i];
        }

        Arrays.sort(sortWords, new Comparator<String>() {
            public int compare(String o1, String o2) {
                for(int i = 0; i < o1.length() && i < o2.length(); i++) {
                    int index1 = order.indexOf(o1.charAt(i));
                    int index2 = order.indexOf(o2.charAt(i));
                    if(index1 != index2)    return index1 - index2;
                }
                return o1.length() - o2.length();
            }
        });

        for(int i = 0; i < words.length; i++) {
            if(!words[i].equals(sortWords[i])) {
                return false;
            }
        }
        return true;
    }
}

/*
114th weekly contest
 */
