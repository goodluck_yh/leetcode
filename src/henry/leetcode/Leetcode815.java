package henry.leetcode;

import java.util.*;

/*
We have a list of bus routes. Each routes[i] is a bus route that the i-th bus repeats forever. For example if routes[0] = [1, 5, 7], this means that the first bus (0-th indexed) travels in the sequence 1->5->7->1->5->7->1->... forever.
We start at bus stop S (initially not on a bus), and we want to go to bus stop T. Travelling by buses only, what is the least number of buses we must take to reach our destination? Return -1 if it is not possible.

Example:
Input:
routes = [[1, 2, 7], [3, 6, 7]]
S = 1
T = 6
Output: 2
Explanation:
The best strategy is take the first bus to the bus stop 7, then take the second bus to the bus stop 6.
 */

public class Leetcode815 {
    public int numBusesToDestination(int[][] routes, int S, int T) {
        if(S == T)
            return 0;
        boolean[] visited_rou = new boolean[routes.length];
        Set<Integer> visited_loc = new HashSet<>();
        List<Integer> list = new LinkedList<>();
        int res = 0;
        for(int i = 0; i < routes.length; i++) {
            boolean find = isFound(routes[i], S);
            if(find) {
                visited_rou[i] = true;
                for(int loc : routes[i]) visited_loc.add(loc);
            }
        }
        list.addAll(visited_loc);

        while(list.size() != 0) {
            res++;
            List<Integer> next = new LinkedList<>();
            for(int loc : list) {
                if(loc == T)
                    return res;
                for(int i = 0; i < routes.length; i++) {
                    if(visited_rou[i])  continue;
                    boolean find = isFound(routes[i], loc);
                    if(find) {
                        for(int j = 0; j < routes[i].length; j++) {
                            if(!visited_loc.contains(routes[i][j])) {
                                visited_loc.add(routes[i][j]);
                                next.add(routes[i][j]);
                            }
                        }
                    }

                }
            }
            list = next;
        }
        return -1;
    }

    private boolean isFound(int[] route, int target) {
        boolean find = false;
        for(int j = 0; j <route.length; j++) {
            if(route[j] == target) {
                find = true;
                break;
            }
        }
        return find;
    }

    public static void main(String[] args) {
        Leetcode815 leetcode = new Leetcode815();
        System.out.println(leetcode.numBusesToDestination(new int[][]{{7,12},{4,5,15},{6},{15,19},{9,12,13}}, 15, 12));
    }
}

/*
这个题目思路是对的，但是仅仅beat了 1%
主要的改进是可以用一个map记录 站点 -》 线路的关系
 */