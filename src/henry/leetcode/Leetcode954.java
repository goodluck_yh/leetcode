package henry.leetcode;

import java.util.TreeMap;

/*
Given an array of integers A with even length, return true if and only if it is possible to reorder it such that A[2 * i + 1] = 2 * A[2 * i] for every 0 <= i < len(A) / 2.

Example 1:
Input: [3,1,3,6]
Output: false

Example 2:
Input: [2,1,2,6]
Output: false

Example 3:
Input: [4,-2,2,-4]
Output: true
Explanation: We can take two groups, [-2,-4] and [2,4] to form [-2,-4,2,4] or [2,4,-2,-4].

Example 4:
Input: [1,2,4,16,8,4]
Output: false

Note:
0 <= A.length <= 30000
A.length is even
-100000 <= A[i] <= 100000
 */

public class Leetcode954 {
    public boolean canReorderDoubled(int[] A) {
        TreeMap<Integer, Integer> neg = new TreeMap<>();
        TreeMap<Integer, Integer> pos = new TreeMap<>();
        int numNeg = 0, numPos = 0;
        for(int i: A) {
            if(i > 0){
                numPos++;
                int cur = pos.getOrDefault(i, 0);
                pos.put(i, cur + 1);
            }
            if(i < 0){
                numNeg++;
                int cur = neg.getOrDefault(i, 0);
                neg.put(i, cur + 1);
            }
        }
        if(numPos % 2 != 0 || numNeg % 2 != 0)  return false;

        if(!isValid(neg,false))   return false;
        if(!isValid(pos, true))   return false;
        return true;
    }

    private boolean isValid(TreeMap<Integer, Integer> map, boolean isPos) {
        while(map.size() != 0) {
            int key;
            if(isPos) {
                key = map.firstKey();
            } else {
                key = map.lastKey();
            }
            if(map.get(key) == 0){
                map.remove(key);
                continue;
            }
            if(!map.containsKey(key * 2) || map.get(key * 2) < map.get(key)) {
                return false;
            }

            if(map.get(key * 2) == map.get(key)) {
                map.remove(key * 2);
            } else {
                map.put(key*2, map.get(key * 2)-map.get(key));
            }
            map.remove(key);
        }
        return true;
    }
}

/*
114th weekly contest
 */