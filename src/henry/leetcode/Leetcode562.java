package henry.leetcode;

public class Leetcode562 {
    public int longestLine(int[][] M) {
        int res = 0;
        for(int i = 0; i < M.length; i++) {
            for(int j = 0; j < M[0].length; j++) {
                if(M[i][j] == 1) {
                    int cnt = getCnt(M, i, j);
                    res = Math.max(res, cnt);
                }
            }
        }
        return res;
    }

    private int getCnt(int[][] M, int i, int j) {
        int res = 1;
        //horizental
        if(i - 1 < 0 || M[i-1][j] == 0) {
            int cnt = 0, index = i;
            while(index < M.length && M[index][j] == 1) {
                cnt++;
                index++;
            }
            res = Math.max(res, cnt);
        }

        // vertical
        if(j - 1 < 0 || M[i][j-1] == 0) {
            int cnt = 0, index = j;
            while(index < M[0].length && M[i][index] == 1) {
                cnt++;
                index++;
            }
            res = Math.max(res, cnt);
        }

        //diagonal
        if(i - 1 < 0 || j - 1 < 0 || M[i-1][j-1] == 0) {
            int cnt = 0, x = i, y = j;
            while(x < M.length && y < M[0].length && M[x][y] == 1) {
                cnt++;
                x++;
                y++;
            }
            res = Math.max(res, cnt);
        }

        //anti-diagonal
        if(i - 1 < 0 || j + 1 >= M[0].length || M[i-1][j+1] == 0) {
            int cnt = 0, x = i, y = j;
            while(x < M.length && y >= 0 && M[x][y] == 1) {
                cnt++;
                x++;
                y--;
            }
            res = Math.max(res, cnt);
        }

        return res;
    }
}

/*
这个是最容易想到的方法，这个复杂度是（m*n + k) k是数组中1个个数
如何做到O(m*n) ? -> dp
后来发现也没快多少
 */

class Leetcode562_2 {
    public int longestLine(int[][] M) {
        if(M.length == 0 || M[0].length == 0)   return 0;
        int[][][] dp = new int[M.length][M[0].length][4];
        int res = 0;
        for(int i = 0; i < M.length; i++) {
            for(int j = 0; j < M[0].length; j++) {
                if(M[i][j] == 0)    continue;
                dp[i][j][0] = i-1 >= 0 ?  dp[i-1][j][0] + 1 : 1;
                dp[i][j][1] = j-1 >= 0 ?  dp[i][j-1][1] + 1 : 1;
                dp[i][j][2] = i-1 >= 0 && j-1 >= 0 ?  dp[i-1][j-1][2] + 1 : 1;
                dp[i][j][3] = i-1 >= 0 && j+1 < M[0].length ?  dp[i-1][j+1][3] + 1 : 1;
                for(int k=0; k<4; k++){
                    res = Math.max(res, dp[i][j][k]);
                }
            }
        }
        return res;
    }
}