package henry.leetcode;

import java.util.*;

public class Leetcode681 {
    public String nextClosestTime(String time) {
        char[] chs = time.toCharArray();
        Set<Integer> set = new HashSet<>();
        for(char ch : chs) {
            if(ch != ':')
                set.add(ch - '0');
        }
        List<Integer> list = new ArrayList<>();
        list.addAll(set);
        Collections.sort(list);
        List<Integer> mins = getMinsOrHours(list, 60);
        String[] times = time.split(":");
        int min_int = Integer.parseInt(times[1]);
        int i;
        for(i = mins.size() - 1; i >= 0; i--) {
            if(mins.get(i) <= min_int)
                break;
        }
        if(i != mins.size() - 1){
            if(mins.get(i+1) < 10)
                return times[0] + ":0" + mins.get(i+1);
            else
                return times[0] + ":" + mins.get(i+1);
        }


        List<Integer> hours = getMinsOrHours(list, 24);
        int hour_int = Integer.parseInt(times[0]);
        for(i = hours.size() - 1; i >= 0; i--) {
            if(hours.get(i) <= hour_int)
                break;
        }
        if(i != hours.size() - 1) {
            StringBuffer sb = new StringBuffer();
            if(hours.get(i+1) < 10) {
                sb.append("0");
            }
            sb.append(hours.get(i+1));
            sb.append(":");
            if(mins.get(0) < 10) {
                sb.append("0");
            }
            sb.append(mins.get(0));
            return sb.toString();
        }

        StringBuffer sb = new StringBuffer();
        if(hours.get(0) < 10) {
            sb.append("0");
        }
        sb.append(hours.get(0));
        sb.append(":");
        if(mins.get(0) < 10) {
            sb.append("0");
        }
        sb.append(mins.get(0));
        return sb.toString();

    }

    private List<Integer> getMinsOrHours(List<Integer> nums, int max) {
        List<Integer> res = new ArrayList<>();
        for(int i = 0; i < nums.size(); i++) {
            for(int j = 0; j < nums.size(); j++) {
                int temp = nums.get(i) * 10 + nums.get(j);
                if(temp < max && temp >= 0)
                    res.add(temp);
            }
        }
        return res;
    }
}

