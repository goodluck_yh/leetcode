package henry.leetcode;

/*
We are given a list schedule of employees, which represents the working time for each employee.
Each employee has a list of non-overlapping Intervals, and these intervals are in sorted order.
Return the list of finite intervals representing common, positive-length free time for all employees, also in sorted order.

Example 1:
Input: schedule = [[[1,2],[5,6]],[[1,3]],[[4,10]]]
Output: [[3,4]]
Explanation:
There are a total of three employees, and all common
free time intervals would be [-inf, 1], [3, 4], [10, inf].
We discard any intervals that contain inf as they aren't finite.
 */

import java.util.*;

public class Leetcode759 {
    class Interval {
        int start;
        int end;

        Interval() {
            start = 0;
            end = 0;
        }
    }

    public List<Interval> employeeFreeTime(List<List<Interval>> schedule) {
        TreeMap<Integer, Integer> map = new TreeMap<>();
        List<Interval> res = new ArrayList<>();
        for(List<Interval> person: schedule) {
            for(Interval time: person) {
                map.put(time.start, map.getOrDefault(time.start, 0) + 1);
                map.put(time.end, map.getOrDefault(time.end, 0) - 1);
            }
        }

        Integer start = null, cur = 0;
        for(int time: map.keySet()) {
            if(cur == 0 && start != null) {
                Interval temp = new Interval();
                temp.start = start;
                temp.end = time;
                res.add(temp);
                start = null;
            }
            cur = cur + map.get(time);
            if(cur == 0)    start = time;
        }

        return res;
    }
}

/*
beat: 12.96%
 */