package henry.leetcode;

import java.util.*;

public class Leetcode986 {
    class Interval {
        int start;
        int end;

        Interval(int a, int b) {
            start = a;
            end = b;
        }
    }

    public Interval[] intervalIntersection(Interval[] A, Interval[] B) {
        TreeMap<Integer, Integer> map = new TreeMap<>();
        for(Interval i : A) {
            map.put(i.start, map.getOrDefault(i.start, 0) + 1);
            map.put(i.end, map.getOrDefault(i.end, 0) - 1);
        }

        for(Interval i : B) {
            map.put(i.start, map.getOrDefault(i.start, 0) + 1);
            map.put(i.end, map.getOrDefault(i.end, 0) - 1);
        }

        List<int[]> list = new LinkedList<>();
        int cur = 0, start = -1;
        for(int idx: map.keySet()) {
            int cnt = map.get(idx);
            cur = cur + cnt;
            if(cur > 1) {
                if(start != -1)
                    start = idx;
            } else if(cnt <= 1) {
                if(start != -1) {
                    list.add(new int[]{start, idx-1});
                    start = -1;
                }
            }
        }
        Interval[] res = new Interval[list.size()];
        for(int i = 0; i < res.length; i++) {
            res[i] = new Interval(list.get(i)[0], list.get(i)[1]);
        }
        return res;
    }

    public static void main(String[] args) {
        Leetcode986 leetcode = new Leetcode986();
        
    }
}
