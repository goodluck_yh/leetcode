package henry.leetcode;

/*
Nearly every one have used the Multiplication Table. But could you find out the k-th smallest number quickly from the multiplication table?
Given the height m and the length n of a m * n Multiplication Table, and a positive integer k, you need to return the k-th smallest number in this table.

Example 1:
Input: m = 3, n = 3, k = 5
Output:
Explanation:
The Multiplication Table:
1	2	3
2	4	6
3	6	9

The 5-th smallest number is 3 (1, 2, 2, 3, 3).
 */
public class Leetcode668 {
    public int findKthNumber(int m, int n, int k) {
        int lo = 1, hi = m * n;
        while(lo < hi) {
            int mid = (lo +hi) / 2, cnt = 0, candidate = 1;
            for(int i = 1; i <= m; i++) {
                int c1 = count(i, n, mid);
                cnt += c1;
                if(i * c1 >= candidate)
                    candidate = i * c1;
            }
            if(cnt == k)
                return candidate;
            if(cnt > k) { // bug
                hi = mid;
            } else {
                lo = mid + 1;
            }
        }
        return lo;
    }

    private int count(int m, int n, int target) {
        return Math.min(target / m , n);
    }

}

/*
这个题目是binary search
有两个地方很傻：
- count一开始我也是二分查找，其实直接计算就好了
- 代码中标注bug的地方
 */