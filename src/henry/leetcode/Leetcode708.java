package henry.leetcode;

public class Leetcode708 {
    class Node {
        public int val;
        public Node next;

        public Node() {}

        public Node(int _val,Node _next) {
            val = _val;
            next = _next;
        }
    };

    public Node insert(Node head, int insertVal) {
        if(head == null) {
            Node node = new Node(insertVal, null);
            node.next = node;
            return node;
        }
        if(head.next == head) {
            Node node = new Node(insertVal, head);
            head.next = node;
            return head;
        }

        Node n = head.next;
        int min = Integer.MAX_VALUE, max = Integer.MIN_VALUE;
        while(n != head && n.val <= n.next.val) {
            n = n.next;
            min = Math.min(min, n.val);
            max = Math.min(max, n.val);
        }
        if(min == max) {
            Node node = new Node(insertVal, n);
            n.next = node;
            return head;
        }

        if(insertVal > n.next.val && insertVal < n.val) {
            while(insertVal > n.next.val) n = n.next;
            Node node = new Node(insertVal, n.next);
            n.next = node;
        } else {
            Node node = new Node(insertVal, n.next);
            n.next = node;
        }

        Node temp = n;
        while(temp.next != n) {
            System.out.print(temp.val + " ");
            temp = temp.next;
        }

        return head;
    }
}


class LeetCode708_2 {
    class Node2 {
        public int val;
        public Node2 next;

        public Node2() {}

        public Node2(int _val, Node2 _next) {
            val = _val;
            next = _next;
        }
    };

    public Node2 insert(Node2 head, int insertVal) {
        if(head == null) {
            Node2 node = new Node2(insertVal, null);
            node.next = node;
            return node;
        }
        Node2 n = head;
        while(n.next != head) {
            if(insertVal == n.val || (insertVal > n.val && insertVal < n.next.val) || (n.val > n.next.val && (insertVal > n.val || insertVal < n.next.val))) {
                Node2 node = new Node2(insertVal, n.next);
                n.next = node;
                System.out.println(insertVal == n.val);
                System.out.println(insertVal > n.val && insertVal < n.next.val);
                System.out.println(n.val > n.next.val && insertVal > n.val || insertVal < n.next.val);

                return head;
            }
            n = n.next;
        }

        Node2 node = new Node2(insertVal, head);
        n.next = node;

        return head;
    }
}

/*
解法2是one-pass解法
 */