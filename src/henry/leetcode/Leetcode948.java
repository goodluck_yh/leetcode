package henry.leetcode;

import java.util.Arrays;

/*
You have an initial power P, an initial score of 0 points, and a bag of tokens.
Each token can be used at most once, has a value token[i], and has potentially two ways to use it.
If we have at least token[i] power, we may play the token face up, losing token[i] power, and gaining 1 point.
If we have at least 1 point, we may play the token face down, gaining token[i] power, and losing 1 point.
Return the largest number of points we can have after playing any number of tokens.

Example 1:
Input: tokens = [100], P = 50
Output: 0

Example 2:
Input: tokens = [100,200], P = 150
Output: 1

Example 3:
Input: tokens = [100,200,300,400], P = 200
Output: 2

Note:
tokens.length <= 1000
0 <= tokens[i] < 10000
0 <= P < 10000
 */
public class Leetcode948 {
    public int bagOfTokensScore(int[] tokens, int P) {
        Arrays.sort(tokens);
        int start = 0, end = tokens.length-1, cur = P, res = 0;
        while(start <= end) {
            if(cur >= tokens[start]) {
                res++;
                cur -= tokens[start];
                start++;
            }
            else if (start != end && res > 0 ){
                cur = cur + tokens[end] - tokens[start];
                start++;
                end--;
            } else {
                break;
            }
        }
        return res;
    }

    public static void main(String[] args) {
        Leetcode948 leetcode = new Leetcode948();
        int[] tokens = new int[] {100, 200, 300, 400};
        int P = 200;
        System.out.println(leetcode.bagOfTokensScore(tokens, P));
    }
}

/*
一些小心得：
    - 题目意思好弄懂
    - 根据tokens长度，猜测，可能的复杂度是O(n^2) 但是实际是O(nlgn)
    - 分析思路时，想了想会不会是DP，后来发现要维护两个数组，应该不是dp，之后想到了贪心算法
 */