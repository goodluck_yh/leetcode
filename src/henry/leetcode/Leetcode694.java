package henry.leetcode;

import java.util.*;

public class Leetcode694 {
    public int numDistinctIslands(int[][] grid) {
        Set<String> islands = new HashSet<>();
        for(int i = 0; i < grid.length; i++) {
            for(int j = 0; j < grid[0].length; j++) {
                if(grid[i][j] != 0) {
                    List<int[]> island = new LinkedList<>();
                    findIsland(grid, i, j, island, 0, 0);
                    StringBuffer sb = new StringBuffer();
                    for(int[] temp : island) {
                        sb.append(temp[0] + "#" + temp[1] + "#");
                    }
                    String s = sb.toString();
                    if(!islands.contains(s))
                        islands.add(s);
                }
            }
        }
        return islands.size();
    }

    private void findIsland(int[][] grid, int i, int j, List<int[]> island, int x, int y) {
        if(i + x < 0 || i + x >= grid.length || j + y < 0 || j + y >= grid[0].length && grid[i + x][j + y] == 0)
            return;
        island.add(new int[] {x, y});
        grid[i][j] = 0;
        findIsland(grid, i, j, island, x+1, y);
        findIsland(grid, i, j, island, x, y+1);
        findIsland(grid, i, j, island, x-1, y);
        findIsland(grid, i, j, island, x, y-1);
    }
}

/*
这个题目的难点在于如何判断两个island是否相等，没有特别好的办法， 基本想法是根据左上角的点计算偏移量
 */