package henry.leetcode;

public class Leetcode776 {
    public class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }

    public TreeNode[] splitBST(TreeNode root, int V) {
        return helper(root, V);
    }

    private TreeNode[] helper(TreeNode node, int v) {
        if(node == null) {
            return new TreeNode[] {null, null};
        }
        // split on top
        if(node.val == v) {
            TreeNode small = node;
            TreeNode big = node.right;
            small.right = null;
            return new TreeNode[] {small, big};
        }
        else if(node.val > v) {
            TreeNode big = node;
            node = node.left;
            big.left = null;
            TreeNode[] next = helper(node, v);
            big.left = next[1];
            return new TreeNode[] {next[0], big};
        }
        else {
            TreeNode small = node;
            node = node.right;
            small.right = null;
            TreeNode[] next = helper(node, v);
            small.right = next[0];
            return new TreeNode[] {small, next[1]};
        }
    }
}

/*
典型树的题目，递归求解
有一个bug， 要check null
 */