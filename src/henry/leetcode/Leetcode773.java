package henry.leetcode;

/*
On a 2x3 board, there are 5 tiles represented by the integers 1 through 5, and an empty square represented by 0.
A move consists of choosing 0 and a 4-directionally adjacent number and swapping it.
The state of the board is solved if and only if the board is [[1,2,3],[4,5,0]].
Given a puzzle board, return the least number of moves required so that the state of the board is solved. If it is impossible for the state of the board to be solved, return -1.

Examples:

Input: board = [[1,2,3],[4,0,5]]
Output: 1
Explanation: Swap the 0 and the 5 in one move.
 */

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class Leetcode773 {
    int[] dx = {0,1,0,-1};
    int[] dy = {-1,0,1,0};
    public int slidingPuzzle(int[][] board) {
        Set<String> set = new HashSet<>();
        String s = convert(board);
        set.add(s);
        List<String> list = new LinkedList<>();
        list.add(s);
        int res = 0;
        while(list.size() != 0) {
            List<String> next = new LinkedList<>();
            for(int i = 0; i < list.size(); i++) {
                s = list.get(i);

                if(s.equals("123450"))
                    return res;
                board = convertback(s);
                // find 0
                int x = 0, y = 0;
                for(x = 0; x < 2; x++) {
                    boolean flag = false;
                    for(y = 0; y < 3; y++) {
                        if(board[x][y] == 0){
                            flag = true;
                            break;
                        }
                    }
                    if(flag)
                        break;
                }
                // move
                for(int j = 0; j < 4; j++) {
                    int x_new = x + dx[j], y_new = y + dy[j];
                    if(x_new >= 0 && x_new < 2 && y_new >= 0 && y_new < 3) {
                        int temp = board[x_new][y_new];
                        board[x_new][y_new] = 0;
                        board[x][y] = temp;
                        String s_new = convert(board);
                        if(!set.contains(s_new)) {
                            set.add(s_new);
                            next.add(s_new);
                        }
                        board[x][y] = 0;
                        board[x_new][y_new] = temp;
                    }
                }
            }
            res++;
            list = next;
        }
        return -1;
    }

    private String convert(int[][] board) {
        StringBuffer sb = new StringBuffer();
        for(int i = 0; i < board.length; i++) {
            for(int j = 0 ; j < board[0].length; j++) {
                sb.append(board[i][j]);
            }
        }
        return sb.toString();
    }

    private int[][] convertback(String s) {
        int[][] board = new int[2][3];
        for(int i = 0 ; i < s.length(); i++) {
            int num = s.charAt(i) - '0';
            board[i/3][i%3] = num;
        }
        return board;
    }

    public static void main(String[] args) {
        Leetcode773 leetcode = new Leetcode773();
        System.out.println(leetcode.slidingPuzzle(new int[][] {{1,2,3}, {4,0,5}}));
    }
}

/*
标准的bfs
 */
