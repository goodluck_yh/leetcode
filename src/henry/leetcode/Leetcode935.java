package henry.leetcode;

import java.util.Arrays;

/*
935. Knight Dialer
具体题目描述中有图片，可见https://leetcode.com/problems/knight-dialer/
 */

public class Leetcode935 {
    public int knightDialer(int N) {
        if(N == 1)  return 10;
        if(N == 2)  return 20;
        int mod = 1_000_000_007;
        int[][] nextPoss = new int[][] {{4, 6}, {6, 8}, {7, 9}, {4, 8}, {3, 9, 0}, {}, {1, 7, 0}, {2, 6}, {1, 3}, {2, 4}};
        long[] dp = new long[10];
        Arrays.fill(dp, 2L);
        dp[5] = 0L;
        dp[4] = 3L;
        dp[6] = 3L;
        for(int i = 3; i <= N; i++) {
            long[] dp1 = new long[10];
            for(int j = 0; j < 10; j++) {
                for(int k = 0; k < nextPoss[j].length; k++) {
                    dp1[nextPoss[j][k]] += dp[j];
                }
            }
            for(int j = 0; j < 10; j++) {
                dp1[j] = dp1[j] % mod;
            }
            dp = dp1;
        }

        long res = 0L;
        for(int i = 0; i < 10; i++) {
            res = res + dp[i];
            if(res > mod)   res = res % mod;
        }
        return (int)(res);
    }

    public static void main(String[] args) {
        Leetcode935 leetcode = new Leetcode935();
        System.out.println(leetcode.knightDialer(100));
    }
}

/*
总结：
    这个题目没有解出来。我想到了应该是dp，在纠结如何写转移方程，后发现每一步都是什么元素很重要。如果不记录下来，不能正确计算下一个状态。
    我卡在这里，不知道如果进行。
    答案中的解答记录了0-9所有的个数。这个解法为其他类似题目提供了参考。
 */
