package henry.leetcode;

import java.util.*;

public class Leetcode971 {
    class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }
    List<Integer> res = new LinkedList<>();
    public List<Integer> flipMatchVoyage(TreeNode root, int[] voyage) {
        boolean success = helper(root, voyage, 0, voyage.length-1);
        if(success)
            return res;
        else{
            res = new LinkedList<>();
            res.add(-1);
            return res;
        }
    }

    private boolean helper(TreeNode node, int[] voyage, int start, int end) {
        if(node == null) {
            if(start > end)
                return true;
            else
                return false;
        }

        if(start > end)
            return false;
        if(node.val != voyage[start])
            return false;
        if(node.left == null && node.right == null) {
            return true;
        } else if(node.left == null) {
            return helper(node.right, voyage, start+1, end);
        } else if(node.right == null) {
            return helper(node.left, voyage, start+1, end);
        } else{
            if(voyage[start+1] == node.left.val) {
                int index = start+1;
                while(index <= end && voyage[index] != node.right.val)
                    index++;
                return helper(node.left, voyage, start+1, index-1) && helper(node.right, voyage, index, end);
            } else if(voyage[start+1] == node.right.val) {
                res.add(node.val);
                int index= start+1;
                while(index <= end && voyage[index] != node.left.val)
                    index++;
                return helper(node.left, voyage, index, end) && helper(node.right, voyage, start+1, index-1);
            } else {
                return false;
            }
        }
    }
}
