package henry.leetcode;

import java.util.*;

public class Leetcode428 {
    class Node {
        public int val;
        public List<Node> children;

        public Node() {}

        public Node(int _val,List<Node> _children) {
            val = _val;
            children = _children;
        }
    };

    public String serialize(Node root) {
        return help1(root);
    }

    private String help1(Node root) {
        if(root == null)
            return "[]";
        String s = root.val + "[";
        for(Node n: root.children) {
            s += help1(n);
        }
        s += "]";
        return s;
    }


    // Decodes your encoded data to tree.
    public Node deserialize(String data) {
        return help2(data);
    }

    private Node help2(String data) {
        if(data.equals("[]"))
            return null;
        Node node = new Node();
        int val = Integer.parseInt(data.substring(0, data.indexOf('[')));
        node.val = val;
        node.children = new LinkedList<>();
        data = data.substring(data.indexOf('[')+1, data.length() - 1);
        //System.out.println(data);
        while(data.length() > 0) {
            int idx = data.indexOf('[') + 1, left = 1;
            while(left != 0) {
                if(data.charAt(idx) == '[')
                    left++;
                if(data.charAt(idx) == ']')
                    left--;
                idx++;
            }
            String sub = data.substring(0, idx);
            data = data.substring(idx, data.length());
            node.children.add(help2(sub));
        }
        return node;
    }
}
