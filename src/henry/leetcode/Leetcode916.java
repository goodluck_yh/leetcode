package henry.leetcode;

import java.util.*;

public class Leetcode916 {
    public List<String> wordSubsets(String[] A, String[] B) {
        List<String> list = new LinkedList<>();
        int[] count = new int[26];
        for(String sub: B) {
            int[] cc = new int[26];
            char[] chs = sub.toCharArray();
            for(int i = 0; i < chs.length; i++) {
                int idx = chs[i] - 'a';
                cc[idx]++;
                if(cc[idx] > count[idx])
                    count[idx] = cc[idx];
            }
        }

        for(int i = 0; i < A.length; i++) {
            int[] cc = new int[26];
            char[] chs = A[i].toCharArray();
            for(int j = 0; j < chs.length; j++) {
                int idx = chs[j] - 'a';
                cc[idx]++;
            }
            boolean flag = true;
            for(int j = 0; j < 26; j++) {
                if(count[j] != 0 && count[j] > cc[j]) {
                    flag = false;
                    break;
                }
            }
            if(flag)
                list.add(A[i]);
        }

        return list;
    }

    public static void main(String[] args) {
        Leetcode916 leetcode = new Leetcode916();
        //["amazon","apple","facebook","google","leetcode"]
        //["e","oo"]
        leetcode.wordSubsets(new String[] {"amazon","apple","facebook","google","leetcode"}, new String[] {"e","oo"});
    }
}
