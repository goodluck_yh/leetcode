package henry.leetcode;

import java.util.*;

/*
Implement a MyCalendarThree class to store your events. A new event can always be added.
Your class will have one method, book(int start, int end). Formally, this represents a booking on the half open interval [start, end), the range of real numbers x such that start <= x < end.
A K-booking happens when K events have some non-empty intersection (ie., there is some time that is common to all K events.)
For each call to the method MyCalendar.book, return an integer K representing the largest integer such that there exists a K-booking in the calendar.
Your class will be called like this: MyCalendarThree cal = new MyCalendarThree(); MyCalendarThree.book(start, end)
Example 1:

MyCalendarThree();
MyCalendarThree.book(10, 20); // returns 1
MyCalendarThree.book(50, 60); // returns 1
MyCalendarThree.book(10, 40); // returns 2
MyCalendarThree.book(5, 15); // returns 3
MyCalendarThree.book(5, 10); // returns 3
MyCalendarThree.book(25, 55); // returns 3
Explanation:
The first two events can be booked and are disjoint, so the maximum K-booking is a 1-booking.
The third event [10, 40) intersects the first event, and the maximum K-booking is a 2-booking.
The remaining events cause the maximum K-booking to be only a 3-booking.
Note that the last event locally causes a 2-booking, but the answer is still 3 because
eg. [10, 20), [10, 40), and [5, 15) are still triple booked.
 */
public class Leetcode732 {
}

class MyCalendarThree_imos {
    TreeMap<Integer, Integer> map = new TreeMap<>();
    public MyCalendarThree_imos() {

    }

    public int book(int start, int end) {
        map.put(start, map.getOrDefault(start, 0) + 1);
        map.put(end, map.getOrDefault(end, 0) - 1);
        int cur = 0, res = 0;
        for(int i: map.keySet()) {
            int cnt = map.get(i);
            cur = cur + cnt;
            res = Math.max(res, cur);
        }
        return res;
    }
}

/*
这个算法比较慢，因为每一次都要从新计算
此题可以使用segment tree 或者 binary index tree

segment tree vs binary index tree

Anything that can be done using a BIT can also be done using a segment tree : BIT stores cumulative quantities for certain intervals.
Segment tree stores cumulative quantities for those intervals and more.
In particular, if we are creating a data structure to deal with an array of size N=2^K, the BIT will have cumulative quantities for N intervals
whereas the segment tree will have cumulative values for 2N-1 intervals

There are things that a segment tree can do but a BIT cannot : A BIT essentially works with cumulative quantities.
When the cumulative quantity for interval [i..j] is required, it is found as the difference between cumulative quantities for [1...j] and [1...i-1].
This works only because addition has an inverse operation.
You cannot do this if the operation is non-invertible (such as max).
On the other hand, every interval on a segment tree can be found as union of disjoint intervals and no inverse operation is required

A BIT requires only half as much memory as a segment tree : In cases where you have masochistic memory constraints, you are almost stuck with using a BIT

Though BIT and segment tree operations are both O(log(n)), the segment tree operations have a larger constant factor :
This should not matter for most cases. But once again, if you have masochistic time constraints, you might want to switch from a segment tree to a BIT.
The constant factor might become more of a problem if the BIT/Segment tree is multidimensional.

With practice, coding either will be very fast : If you have coded a segment tree 100 times, you will get it very fast the next time you do it.
So no need to worry about code being long.
 */

