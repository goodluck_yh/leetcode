package henry.leetcode;

public class Leetcode807 {
    public int maxIncreaseKeepingSkyline(int[][] grid) {
        int[] x = new int[grid.length];
        int[] y = new int[grid.length];
        for(int i = 0; i < grid.length; i++) {
            for(int j = 0; j < grid.length; j++) {
                x[j] = Math.max(x[j], grid[i][j]);
                y[i] = Math.max(y[i], grid[i][j]);
            }
        }

        int res = 0;
        for(int i = 0; i < grid.length; i++) {
            for(int j = 0; j < grid.length; j++) {
                int max = Math.min(x[j], y[i]);
                res = res + max - grid[i][j];
            }
        }
        return res;
    }
}

/*
easy
 */
