package henry.leetcode;

import java.util.*;

public class Leetcode652 {
    class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }

    public List<TreeNode> findDuplicateSubtrees(TreeNode root) {
        List<TreeNode> res = new LinkedList<>();
        HashMap<String, Integer> map = new HashMap<>();
        postOrder(root, res, map);
        return res;
    }

    private String postOrder(TreeNode node, List<TreeNode> res, HashMap<String, Integer> map) {
        if(node == null)
            return "#";
        String serials = node.val + "," + postOrder(node.left, res, map) + "," + postOrder(node.right, res, map);
        if(map.getOrDefault(serials, 0) == 1) {
            res.add(node);
        }

        map.put(serials, map.getOrDefault(serials, 0) + 1);
        return serials;
    }
}

/*
Find Duplicate Subtrees
思路，后续遍历及序列化
序列化是我忽略的方法， 也导致了这个题目没有办法做出来
 */