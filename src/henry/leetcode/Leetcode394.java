package henry.leetcode;
/*
Given an encoded string, return it's decoded string.
The encoding rule is: k[encoded_string], where the encoded_string inside the square brackets is being repeated exactly k times. Note that k is guaranteed to be a positive integer.
You may assume that the input string is always valid; No extra white spaces, square brackets are well-formed, etc.
Furthermore, you may assume that the original data does not contain any digits and that digits are only for those repeat numbers, k. For example, there won't be input like 3a or 2[4].

Examples:
s = "3[a]2[bc]", return "aaabcbc".
s = "3[a2[c]]", return "accaccacc".
s = "2[abc]3[cd]ef", return "abcabccdcdcdef".
 */

public class Leetcode394 {
    public String decodeString(String s) {
        if(s.length() == 0) return "";

        StringBuffer sb = new StringBuffer();
        int index = 0;
        while(index < s.length()) {
            // if it is letter
            if((s.charAt(index) >= 'a' && s.charAt(index) <= 'z') || (s.charAt(index) >= 'A' && s.charAt(index) <= 'Z')) {
                sb.append(s.charAt(index));
                index++;
                continue;
            }
            // if it is number
            int end = index;
            // get number
            while(end < s.length() && s.charAt(end) >= '0' && s.charAt(end) <= '9') end++;
            int k = Integer.parseInt(s.substring(index, end));
            index = end;
            // get inner
            int leftMore = 0;
            do {
                if(s.charAt(end) == '[')
                    leftMore++;
                else if(s.charAt(end) == ']')
                    leftMore--;
                end++;
            }while(leftMore != 0);
            String inner = decodeString(s.substring(index+1, end-1));
            index = end;
            // append k times
            for(int i = 0; i < k; i++) {
                sb.append(inner);
            }
        }
        return sb.toString();
    }
}

/*
比较容易，整体思路没有，唯一的错误是A-Z没有support
 */