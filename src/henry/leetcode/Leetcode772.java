package henry.leetcode;

/*
Implement a basic calculator to evaluate a simple expression string.
The expression string may contain open ( and closing parentheses ), the plus + or minus sign -, non-negative integers and empty spaces .
The expression string contains only non-negative integers, +, -, *, / operators , open ( and closing parentheses ) and empty spaces . The integer division should truncate toward zero.
You may assume that the given expression is always valid. All intermediate results will be in the range of [-2147483648, 2147483647].

Some examples:
"1 + 1" = 2
" 6-4 / 2 " = 4
"2*(5+5*2)/3+(6/2+8)" = 21
"(2+6* 3+5- (3*14/7+2)*5)+3"=-12
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class Leetcode772 {
    public int calculate(String s) {
        return helper(s);
    }

    private int helper(String exp) {
        int idx = 0;
        exp = exp.trim();
        Stack<Long> nums = new Stack<>();
        Stack<Character> ops = new Stack<>();
        while(idx < exp.length()) {
            // handle space
            while(idx < exp.length() && exp.charAt(idx) == ' ') idx++;
            // +, -, *, / operators ,
            if(exp.charAt(idx) == '+' || exp.charAt(idx) == '-' || exp.charAt(idx) == '*' || exp.charAt(idx) == '/') {
                ops.add(exp.charAt(idx++));
                continue;
            }
            // open ( and closing parentheses )
            else if(exp.charAt(idx) == '(') {
                int left = 0, idx_dup = idx;
                do {
                    if(exp.charAt(idx) == '(')  left++;
                    else if(exp.charAt(idx) == ')') left--;
                    idx++;
                } while(left != 0);
                nums.add((long)helper(exp.substring(idx_dup+1, idx-1)));
            }
            // num
            else {
                int idx_dup = idx;
                while(idx < exp.length() && exp.charAt(idx) >= '0' && exp.charAt(idx) <= '9')
                    idx++;
                nums.add(Long.parseLong(exp.substring(idx_dup, idx)));
            }
            if(ops.size() != 0 && ops.peek() == '*' ) {
                long num2 = nums.pop(), num1 = nums.pop();
                ops.pop();
                nums.add(num1 * num2);
            } else if(ops.size() != 0 && ops.peek() == '/') {
                long num2 = nums.pop(), num1 = nums.pop();
                ops.pop();
                nums.add(num1 / num2);
            }
        }
        Stack<Long> nums1 = new Stack();
        Stack<Character> ops1 = new Stack();

        while(nums.size() != 0)
            nums1.add(nums.pop());

        while(ops.size() != 0)
            ops1.add(ops.pop());
        nums = nums1;
        ops = ops1;

        while(ops.size() != 0) {
            long num1 = nums.pop(), num2 = nums.pop();
            char ch = ops.pop();
            if(ch == '+') {
                nums.add(num1 + num2);
            } else {
                nums.add(num1 - num2);
            }
        }
        long res = nums.peek();
        return (int)(res);
    }

    public static void main(String[] args) {
        Leetcode772 leetcode = new Leetcode772();
        System.out.println(leetcode.calculate(" 2-1 + 2 "));
    }

}

/*
有一些bug，但是不难想
corner case: "0-2147483648" "2-1+2"
请注意的是，stack可以用来对付乘除，但是不能用来对付加减
 */
