package henry.leetcode;

/*
You are given an integer array A.  From some starting index, you can make a series of jumps.
The (1st, 3rd, 5th, ...) jumps in the series are called odd numbered jumps, and the (2nd, 4th, 6th, ...) jumps in the series are called even numbered jumps.
You may from index i jump forward to index j (with i < j) in the following way:

During odd numbered jumps (ie. jumps 1, 3, 5, ...), you jump to the index j such that A[i] <= A[j] and A[j] is the smallest possible value.
If there are multiple such indexes j, you can only jump to the smallest such index j.
During even numbered jumps (ie. jumps 2, 4, 6, ...), you jump to the index j such that A[i] >= A[j] and A[j] is the largest possible value.
If there are multiple such indexes j, you can only jump to the smallest such index j.
(It may be the case that for some index i, there are no legal jumps.)
A starting index is good if, starting from that index, you can reach the end of the array (index A.length - 1) by jumping some number of times (possibly 0 or more than once.)

Return the number of good starting indexes.

Example 1:
Input: [10,13,12,14,15]
Output: 2
Explanation:
From starting index i = 0, we can jump to i = 2 (since A[2] is the smallest among A[1], A[2], A[3], A[4] that is greater or equal to A[0]), then we can't jump any more.
From starting index i = 1 and i = 2, we can jump to i = 3, then we can't jump any more.
From starting index i = 3, we can jump to i = 4, so we've reached the end.
From starting index i = 4, we've reached the end already.
In total, there are 2 different starting indexes (i = 3, i = 4) where we can reach the end with some number of jumps.
 */


import java.util.TreeMap;

public class Leetcode975 {
    public int oddEvenJumps(int[] A) {
        boolean[] even = new boolean[A.length], odd = new boolean[A.length];
        odd[odd.length - 1] = true;
        even[even.length - 1] = true;
        TreeMap<Integer, Integer> map = new TreeMap<>();
        map.put(A[A.length-1], A.length-1);
        for(int i = A.length - 2; i >= 0; i--) {
            // handle odd
            Integer next = map.ceilingKey(A[i]);
            if(next == null)
                odd[i] = false;
            else
                odd[i] = even[map.get(next)];
            // handle even
            next = map.floorKey(A[i]);
            if(next == null)
                even[i] = false;
            else
                even[i] = odd[map.get(next)];
            // handle map
            map.put(A[i], i);
        }

        int res = 0;
        for(int i = 0; i < A.length; i++) {
            if(odd[i])
                res++;
        }
        return res;
    }

    public static void main(String[] args) {
        Leetcode975 leetcode = new Leetcode975();
        System.out.println(leetcode.oddEvenJumps(new int[] {1,2,3,2,1,4,4,5}));
    }
}

/*
beat 83.83%
这个题目是自己做出来的，算法不难想，但是实现上有些小困难！
 */