package henry.leetcode;

/*
Input: "aabcbc"
Output: true
Explanation:
We start with the valid string "abc".
Then we can insert another "abc" between "a" and "bc", resulting in "a" + "abc" + "bc" which is "aabcbc".
 */
import java.util.*;

public class Leetcode1003 {
    HashMap<String, Boolean> map = new HashMap<>();
    public boolean isValid(String S) {
        if(S.length() % 3 != 0)  return false;
        if(S.length() == 3) {
            return S.equals("abc");
        }
        if(map.containsKey(S))
            return map.get(S);
        for(int i = 0; i < S.length(); i++) {
            String sub = S.substring(i);
            if(sub.startsWith("abc")) {
                boolean flag = isValid(S.substring(0, i) + sub.substring(3));
                map.put(S, flag);
                return flag;
            }
        }
        map.put(S, false);
        return false;
    }
}


/*
HashMap<String, Boolean> map = new HashMap<>();
    public boolean isValid(String S) {
        if(S.length() % 3 != 0)  return false;
        if(S.length() == 3) {
            return S.equals("abc");
        }
        if(map.containsKey(S))
            return map.get(S);
        if(S.charAt(0) != 'a' || S.charAt(S.length() - 1) != 'c') {
            map.put(S, false);
            return false;
        }

        int a = 0, b = 0, c = 0;
        for(int i = 0; i < S.length(); i++) {
            if(S.charAt(i) == 'a')
                a++;
            else if(S.charAt(i) == 'b')
                b++;
            else
                c++;
        }

        if(a != b || b != c || c != a) {
            map.put(S, false);
            return false;
        }

        for(int i = 0; i < S.length() - 2; i++) {
            String sub = S.substring(i);
            if(sub.startsWith("abc")) {
                boolean flag = isValid(S.substring(0, i) + sub.substring(3));
                if(flag) {
                    map.put(S, true);
                    return true;
                }
            }
        }
        map.put(S, false);
        return false;
    }
 */