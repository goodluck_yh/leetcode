package henry.leetcode;

public class Leetcode689 {
    public int[] maxSumOfThreeSubarrays(int[] nums, int k) {
        // calc the k sum first
        int[] sum = new int[nums.length - k + 1];

        for(int i = 0; i < nums.length; i++) {
            if(i <= k-1) {
                sum[0] += nums[i];
            } else {
                sum[i-k+1] = sum[i-k] - nums[i-k] + nums[i];
            }

        }

        // calc the increasing in the left
        int[] left = new int[nums.length - k + 1];
        left[0] = 0;
        int max = 0;
        for(int i = 1; i < left.length; i++) {
            if(sum[i] > sum[max]) {
                max = i;
            }
            left[i] = max;
        }

        // calc the decreasing in the right
        int[] right = new int[nums.length - k + 1];
        right[right.length-1] = right.length-1;
        max = right.length-1;
        for(int i = right.length - 2; i >= 0; i--) {
            if(sum[i] >= sum[max]) {
                max = i;
            }
            right[i] = max;
        }

        int[] res = {-1, -1, -1};
        max = -100000;
        // loop from left to right
        for(int i = k; i <= nums.length - 2*k; i++) {
            int temp = sum[i] + sum[left[i-k]] + sum[right[i+k]];
            //System.out.println(left[i-k] + " " + i + " " + right[i+k] + " " + temp);
            if(max < temp) {
                res[0] = left[i-k];
                res[1] = i;
                res[2] = right[i+k];
                max = temp;
            }
        }

        return res;

    }
}
