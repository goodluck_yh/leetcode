package henry.leetcode;

/*
UTF-8 Validation
 */

public class Leetcode393 {
    public boolean validUtf8(int[] data) {
        return helper(data, 0);
    }

    private boolean helper(int[] data, int idx) {
        if(idx >= data.length)  return true;
        if(data[idx] >= 255) return false; //bug1
        // if start with 0;
        if(data[idx] < 128)
            return helper(data, idx + 1);

        // if start with 1
        int cnt = 0, benchmark = 128, increase = 128;
        while(data[idx] >= benchmark) {
            cnt++;
            increase = increase / 2;
            benchmark += increase;
        }

        if(cnt > 4 || cnt == 1) return false;
        if(idx + cnt -1 >= data.length)    return false;  //bug2
        for(int i = 1; i < cnt; i++) {
            if(data[idx + i] < 128 || data[idx + i] >= 192) return false;
        }

        return helper(data, idx + cnt);
    }
}

/*
写了两个bug，comment了出来
 */