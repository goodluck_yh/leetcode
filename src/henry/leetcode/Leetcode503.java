package henry.leetcode;

import java.util.*;

/*
Given a circular array (the next element of the last element is the first element of the array), print the Next Greater Number for every element. The Next Greater Number of a number x is the first greater number to its traversing-order next in the array, which means you could search circularly to find its next greater number. If it doesn't exist, output -1 for this number.

Example 1:
Input: [1,2,1]
Output: [2,-1,2]
Explanation: The first 1's next greater number is 2;
The number 2 can't find next greater number;
The second 1's next greater number needs to search circularly, which is also 2.
 */
public class Leetcode503 {
    public int[] nextGreaterElements(int[] nums) {
        int[] arr = new int[nums.length * 2], res = new int[nums.length];
        Arrays.fill(res, -1);
        Stack<Integer> stack = new Stack<>();
        for(int i = 0; i < arr.length; i++) {
            while(!stack.isEmpty() && nums[stack.peek() % nums.length] < nums[i % nums.length]) {
                int index = stack.pop();
                if(index < nums.length)
                    res[index] = nums[i % nums.length];
            }
            stack.push(i);
        }
        return res;
    }
}

/*
和leetcode496一样
循环数组的一般思路： length*2
beat不多，但是是主流算法
 */
