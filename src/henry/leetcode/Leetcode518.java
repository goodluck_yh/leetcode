package henry.leetcode;

import java.util.*;

/*
You are given coins of different denominations and a total amount of money. Write a function to compute the number of combinations that make up that amount. You may assume that you have infinite number of each kind of coin.



Example 1:

Input: amount = 5, coins = [1, 2, 5]
Output: 4
Explanation: there are four ways to make up the amount:
5=5
5=2+2+1
5=2+1+1+1
5=1+1+1+1+1
 */
public class Leetcode518 {
    HashMap<Integer, HashMap<Integer,Integer>> map;
    public int change(int amount, int[] coins) {
        map = new HashMap<>();
        return helper(amount, coins, 0, 0);
    }

    private int helper(int amount, int[] coins, int cur, int index) {
        if(cur == amount){
            return 1;
        }
        if(cur > amount || index == coins.length)
            return 0;

        if(map.containsKey(cur) && map.get(cur).containsKey(index))
            return map.get(cur).get(index);

        int res = 0, c = cur;
        while(c <= amount) {
            int temp = helper(amount, coins, c, index+1);
            if(temp != -1) {
                res = res + temp;
            }
            c += coins[index];
        }
        if(!map.containsKey(cur))
            map.put(cur, new HashMap<>());
        map.get(cur).put(index, res);
        return res;
    }
}

/*
这个题目一开始我穷举，发现超时。后来迅速发现可以dp,但是由于写的是递归，就直接自顶向下了
 */