package henry.leetcode;

/*
An N x N board contains only 0s and 1s. In each move, you can swap any 2 rows with each other, or any 2 columns with each other.
What is the minimum number of moves to transform the board into a "chessboard" - a board where no 0s and no 1s are 4-directionally adjacent? If the task is impossible, return -1.

Examples:
Input: board = [[0,1,1,0],[0,1,1,0],[1,0,0,1],[1,0,0,1]]
Output: 2
Explanation:
One potential sequence of moves is shown below, from left to right:

0110     1010     1010
0110 --> 1010 --> 0101
1001     0101     1010
1001     0101     0101

The first move swaps the first and second column.
The second move swaps the second and third row.
 */
public class Leetcode782 {
    public int movesToChessboard(int[][] board) {
        // check every square
        for(int i = 1; i < board.length; i++) {
            for(int j = 1; j < board.length; j++) {
                if((board[0][0] ^ board[0][j] ^ board[i][0] ^ board[i][j]) != 0) {
                    return -1;
                }
            }
        }
        // check first row and first column
        int rowNum = 0, colNum = 0;
        for(int i = 0; i < board.length; i++) {
            if(board[0][i] == 1) {
                rowNum++;
            }
            if(board[i][0] == 1) {
                colNum++;
            }
        }
        if(board.length % 2 == 0) {
            if(rowNum * 2 != board.length || colNum * 2 != board.length)
                return -1;
        } else {
            if((rowNum * 2 - 1 != board.length && rowNum * 2 + 1 != board.length) || (colNum * 2 - 1 != board.length && colNum * 2 + 1 != board.length))
                return -1;
        }
        // check min move
        rowNum = 0;
        colNum = 0;
        for(int i = 0; i < board.length; i++) {
            if(board[0][i] == i % 2) {
                rowNum++;
            }
            if(board[i][0] == i % 2) {
                colNum++;
            }
        }
        if (board.length % 2 == 1) {
            if (rowNum % 2 == 1) rowNum = board.length - rowNum;
            if (colNum % 2 == 1) colNum = board.length - colNum;
        } else {
            rowNum = Math.min(board.length - rowNum, rowNum);
            colNum = Math.min(board.length - colNum, colNum);
        }
        return (rowNum + colNum) / 2;
    }
}

/*
这一题目主要考Math！基本上想不出来，记住就好了！
 */