package henry.leetcode;

public class Leetcode972 {
    public boolean isRationalEqual(String S, String T) {
        String[] s = S.split("\\."), t = T.split("\\.");
        // int intPart1 = Integer.parseInt(s[0]), intPart2 = Integer.parseInt(t[0]);
        // if(Math.abs(intPart1 - intPart2) > 1)
        //     return false;

        String[] after1 = splitAfterDot(s.length == 1 ? "" : s[1]), after2 = splitAfterDot(t.length == 1 ? "" : t[1]);
        after1[1] = removeDuplicate(after1[1]);
        after2[1] = removeDuplicate(after2[1]);

        after1 = shorten(after1[0], after1[1]);
        after2 = shorten(after2[0], after2[1]);

        String[] all1 = handleNine(s[0], after1[0], after1[1]);
        String[] all2 = handleNine(t[0], after2[0], after2[1]);

        for(int i = 0; i < all1.length; i++) {
            if(!all1[i].equals(all2[i]))
                return false;
        }
        return true;
    }

    private String[] handleNine(String first, String second, String third) {
        if(third.equals("9")) {
            if(!second.equals("")) {
                int s = Integer.parseInt(second);
                s++;
                second = "" + s;
            } else {
                int f = Integer.parseInt(first);
                f++;
                first = "" + f;
            }
            third = "";
        }
        return new String[] {first, second, third};
    }

    private String[] splitAfterDot(String s) {
        String[] res = new String[2];
        if(s.equals("")) {
            res[0] = "";
            res[1] = "";
            return res;
        }
        if(s.indexOf('(') == -1) {
            res[0] = s;
            res[1] = "";
            return res;
        }
        int first = s.indexOf('(');
        res[0] = s.substring(0, first);
        res[1] = s.substring(first+1, s.length() - 1);
        return res;
    }

    private String[] shorten(String nonrep, String rep) {
        while(nonrep.length() > 0 && rep.length() > 0 && nonrep.charAt(nonrep.length() - 1) == rep.charAt(rep.length() - 1)) {
            nonrep = nonrep.substring(0, nonrep.length() - 1);
            rep = rep.substring(rep.length() - 1, rep.length()) + rep.substring(0, rep.length()-1);
        }
        String[] res = new String[2];
        res[0] = nonrep;
        res[1] = rep;
        return res;
    }

    private String removeDuplicate(String repeats) {
        int len = repeats.length();
        for(int i = 1; i < len; i++) {
            if(len % i == 0) {
                String sub = repeats.substring(0, i);
                String newStr = "";
                int cnt = len / i;
                for(int j = 0; j < cnt; j++) {
                    newStr = newStr + sub;
                }
                if(newStr.equals(repeats)) {
                    return sub;
                }
            }
        }
        return repeats;
    }

    public static void main(String[] args) {
        Leetcode972 leetcode = new Leetcode972();
        System.out.println(leetcode.isRationalEqual("250.(36)",  "250.(3636)"));
    }
}
