package henry.leetcode;

import java.util.*;

public class Leetcode989 {
    public List<Integer> addToArrayForm(int[] A, int K) {
        List<Integer> k = getArr(K);
        List<Integer> a = new LinkedList<>();
        for(int i = 0; i < A.length; i++)
            a.add(A[i]);

        List<Integer> res = new LinkedList<>();
        boolean flag = false;
        int ia = a.size()-1, ik = k.size()-1;
        while(ia >= 0 && ik >= 0) {
            int temp = a.get(ia) + k.get(ik);
            if(flag)
                temp++;
            if(temp >= 10)
                flag = true;
            else
                flag = false;
            ia--;
            ik--;
            res.add(0, temp % 10);
        }

        while(ia >= 0) {
            int temp = a.get(ia);
            if(flag)
                temp++;
            if(temp >= 10)
                flag = true;
            else
                flag = false;
            ia--;
            res.add(0, temp % 10);
        }

        while(ik >= 0) {
            int temp = k.get(ik);
            if(flag)
                temp++;
            if(temp >= 10)
                flag = true;
            else
                flag = false;
            ik--;
            res.add(0, temp % 10);
        }

        if(flag)
            res.add(0, 1);

        return res;
    }

    private List<Integer> getArr(int k) {
        List<Integer> list = new LinkedList<>();
        while(k != 0) {
            list.add(0, k % 10);
            k = k / 10;
        }
        if(list.size() == 0) {
            list.add(0);
        }

        return list;
    }

    public static void main(String[] args) {
        Leetcode989 leetcode = new Leetcode989();
        List<Integer> list = leetcode.addToArrayForm(new int[] {9,9,9}, 1);
        for(int i : list) {
            System.out.print(i + " ");
        }
    }
}
