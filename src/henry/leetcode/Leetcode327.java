package henry.leetcode;

public class Leetcode327 {
    public SegmentTree getTree(int[] arr) {
        return new SegmentTree(arr);
    }

    class SegmentTree {
        int n;
        int[] st;

        public SegmentTree(int[] arr) {
            n = arr.length;
            st = new int[4 * n];
            build(arr, 0, arr.length - 1, 0);
        }

        public int getSum(int start, int end) {
            if(start < 0 || end >= n)   return -1;
            return getSumUtil(start, end, 0, n-1, 0);
        }

        public void update(int i, int newVal) {
            if(i < 0 || i >= n) return;
            int origin = getSum(i, i);
            int diff = newVal - origin;
            updateUtil(i, diff, 0, n-1, 0);
        }

        private void updateUtil(int i, int diff, int arr_s, int arr_e, int st_i) {
            if(i > arr_e || i < arr_s)  return;
            st[st_i] += diff;
            int mid = (arr_s + arr_e) / 2;
            updateUtil(i, diff, arr_s, mid, st_i * 2 + 1);
            updateUtil(i, diff, mid+1, arr_s, st_i * 2 + 2);
        }

        private int getSumUtil(int start, int end, int arr_s, int arr_e, int st_i){
            if(start > arr_e || end < arr_s)    return 0;
            if(arr_s == start && arr_e == end)  return st[st_i];
            int mid = (arr_s + arr_e) / 2;
            return getSumUtil(start, Math.min(end, mid), arr_s, mid, st_i * 2 + 1)
                    + getSumUtil(Math.max(mid+1, start), end, mid+1, arr_e, st_i * 2 + 2);
        }

        private int build(int[] arr, int arr_s, int arr_e, int st_i) {
            if(arr_s == arr_e) {
                st[st_i] = arr[arr_s];
                return st[st_i];
            }
            int mid = (arr_s + arr_e) / 2;
            st[st_i] = build(arr, arr_s, mid, st_i * 2 + 1) + build(arr, mid+1, arr_e, st_i * 2 + 2);
            return st[st_i];
        }
    }

    public static void main(String[] args) {
        int[] arr = {1, 3, 5, 7, 9, 11};
        Leetcode327 leetcode = new Leetcode327();
        SegmentTree st = leetcode.getTree(arr);
        st.update(1, 1);
        System.out.println(st.getSum(0, 5));
    }

}

/*
线段树空间4*n的证明：https://blog.csdn.net/gl486546/article/details/78243098
废了半天力气，其实这个题目不能使用segment tree!
 */