package henry.leetcode;

public class Leetcode307 {
    int[] data, tree;

    public Leetcode307(int[] nums) {
        this.data = nums;
        tree = new int[4 * this.data.length];
        if(this.data.length != 0)
            buildTree(0, 0, data.length-1);
    }

    private void buildTree(int treeIdx, int left, int right) {
        if(left == right) {
            tree[treeIdx] = data[left];
            return;
        }
        int lefti = treeIdx * 2 + 1, righti = treeIdx * 2 + 2;
        int mid = (left + right) / 2;
        buildTree(lefti, left, mid);
        buildTree(righti, mid+1, right);
        tree[treeIdx] = tree[lefti] + tree[righti];
    }

    public void update(int i, int val) {
        int old = data[i];
        int gap = val - old;
        data[i] = val;
        update(0, 0, data.length-1, i, gap);
    }

    private void update(int treeIdx, int left, int right, int target, int gap) {
        if(left == right) {
            tree[treeIdx] += gap;
            return;
        }

        int lefti = treeIdx * 2 + 1, righti = treeIdx * 2 + 2, mid = (left + right) / 2;
        if(target <= mid) {
            update(lefti, left, mid, target, gap);
        } else {
            update(righti, mid+1, right, target, gap);
        }
        tree[treeIdx] += gap;
    }

    public int sumRange(int i, int j) {
        return query(0, 0, data.length-1, i, j);
    }

    private int query(int treeIdx, int left, int right, int leftq, int rightq) {
        if(left == leftq && right == rightq) {
            return tree[treeIdx];
        }
        int mid = (left + right) / 2, lefti = treeIdx * 2 + 1, righti = treeIdx * 2 + 2;
        if(leftq > mid) {
            return query(righti, mid+1, right, leftq, rightq);
        } else if(rightq <= mid) {
            return query(lefti, left, mid, leftq, rightq);
        } else {
            return query(lefti, left, mid, leftq, mid) + query(righti, mid+1, right, mid+1, rightq);
        }
    }
}

/*
beat 100%
segment tree
 */