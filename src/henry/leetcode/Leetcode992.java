package henry.leetcode;

import java.util.HashMap;

/*
Given an array A of positive integers, call a (contiguous, not necessarily distinct) subarray of A good if the number of different integers in that subarray is exactly K.
(For example, [1,2,3,1,2] has 3 different integers: 1, 2, and 3.)
Return the number of good subarrays of A.

Example 1:
Input: A = [1,2,1,2,3], K = 2
Output: 7
Explanation: Subarrays formed with exactly 2 different integers: [1,2], [2,1], [1,2], [2,3], [1,2,1], [2,1,2], [1,2,1,2].
 */
public class Leetcode992 {
    public int subarraysWithKDistinct(int[] A, int K) {
        return helper(A, K) - helper(A, K-1);
    }

    private int helper(int[] a, int k) {
        int start = 0, end = 0, res = 0;
        HashMap<Integer, Integer> map = new HashMap<>();
        for(; end < a.length; end++) {
            if(map.getOrDefault(a[end], 0) == 0)
                k--;
            map.put(a[end], map.getOrDefault(a[end], 0) + 1);
            while(k < 0) {
                map.put(a[start], map.get(a[start]) - 1);
                if(map.get(a[start]) == 0)
                    k++;
                start++;
            }
            res = res + end - start + 1;
        }
        return res;
    }
}

/*
想到了滑动窗口，没想到这个解法。这个解法的核心是：
如果想要找到仅含有k则等同于atmost(k) - atmost(k-1)
 */