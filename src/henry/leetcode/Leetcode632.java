package henry.leetcode;

/*
Input:[[4,10,15,24,26], [0,9,12,20], [5,18,22,30]]
Output: [20,24]
Explanation:
List 1: [4, 10, 15, 24,26], 24 is in range [20,24].
List 2: [0, 9, 12, 20], 20 is in range [20,24].
List 3: [5, 18, 22, 30], 22 is in range [20,24].

0 4 5 -> 4 5 9 -> 5 9 10 -> 9 10 18 -> 10 12 18 -> ...

 */
import java.util.*;

public class Leetcode632 {
    public int[] smallestRange(List<List<Integer>> nums) {
        int[] res = new int[2];
        int N = nums.size();
        int[] idxs = new int[N];
        PriorityQueue<Integer> small = new PriorityQueue<>((a, b) -> nums.get(a).get(idxs[a]) - nums.get(b).get(idxs[b]));
        PriorityQueue<Integer> big = new PriorityQueue<>((a, b) -> nums.get(b).get(idxs[b]) - nums.get(a).get(idxs[a]));
        for(int i = 0; i < nums.size(); i++) {
            small.add(i);
            big.add(i);
            idxs[i] = 1;
        }
        for(int i: small) {
            System.out.println(i);
        }

        res[0] = nums.get(small.peek()).get(idxs[small.peek()]-1);
        res[1] = nums.get(big.peek()).get(idxs[big.peek()]-1);
        while(true) {
            int idx = small.peek();
            small.poll();
            big.remove(Integer.valueOf(idx));
            if(idxs[idx] == nums.get(idx).size())
                break;
            small.add(idx+1);
            big.add(idx+1);
            if(big.peek() - small.peek() < res[1] - res[0]) {
                res[1] = nums.get(big.peek()).get(idxs[big.peek()]-1);
                res[0] = nums.get(small.peek()).get(idxs[small.peek()]-1);
            }
            idxs[idx]++;
        }
        return res;
    }

}
