package henry.leetcode;

import java.util.Arrays;

/*
This question is the same as "Max Chunks to Make Sorted" except the integers of the given array are not necessarily distinct, the input array could be up to length 2000, and the elements could be up to 10**8.
Given an array arr of integers (not necessarily distinct), we split the array into some number of "chunks" (partitions), and individually sort each chunk.  After concatenating them, the result equals the sorted array.
What is the most number of chunks we could have made?

Example 1:
Input: arr = [5,4,3,2,1]
Output: 1
Explanation:
Splitting into two or more chunks will not return the required result.
For example, splitting into [5, 4], [3, 2, 1] will result in [4, 5, 1, 2, 3], which isn't sorted.
 */
public class Leetcode768 {
    public int maxChunksToSorted(int[] arr) {
        int[] arr_d = new int[arr.length];
        for(int i = 0; i < arr.length; i++) {
            arr_d[i] = arr[i];
        }
        Arrays.sort(arr_d);
        int res = 0, sum = 0, target = 0;
        for(int i = 0; i < arr.length; i++) {
            sum += arr[i];
            target += arr_d[i];
            if(sum == target) {
                res++;
                sum = 0;
                target = 0;
            }
        }
        return res;
    }
}

/*
复杂度o(nlgn), beat 45%
corner case: [0,3,0,3,2]
 */

class Leetcode768_2 {
    public int maxChunksToSorted(int[] arr) {
        // min is 0...i min value
        // max is i...n-1 max value
        int[] min = new int[arr.length], max = new int[arr.length];
        max[0] = arr[0];
        for(int i = 1; i < arr.length; i++) {
            max[i] = Math.max(max[i-1], arr[i]);
        }

        min[arr.length-1] = arr[arr.length-1];
        for(int i = arr.length-2; i >= 0; i--) {
            min[i] = Math.min(min[i+1], arr[i]);
        }

        int res = 1;
        for(int i = 0; i < arr.length-1; i++) {
            if(max[i] <= min[i+1]) {
                res++;
            }
        }
        return res;
    }
}

/*
复杂度o(n) beat 64%
Algorithm: Iterate through the array, each time all elements to the left are smaller (or equal) to all elements to the right, there is a new chunck.
Use two arrays to store the left max and right min to achieve O(n) time complexity. Space complexity is O(n) too.
This algorithm can be used to solve ver1 too.
 */