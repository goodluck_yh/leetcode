package henry.leetcode;

/*
For some fixed N, an array A is beautiful if it is a permutation of the integers 1, 2, ..., N, such that:
For every i < j, there is no k with i < k < j such that A[k] * 2 = A[i] + A[j].
Given N, return any beautiful array A.  (It is guaranteed that one exists.)

Example 1:
Input: 4
Output: [2,1,4,3]

Example 2:
Input: 5
Output: [3,1,2,5,4]

Note:
1 <= N <= 1000
*/

public class Leetcode932 {
    public int[] beautifulArray(int N) {
        int[] array = new int[] {1};
        while(array.length < 2 * N) {
            int[] next = new int[array.length * 2];
            int idx = 0;
            for(int j = 0; j < array.length; j++) {
                next[idx++] = array[j] * 2 - 1;
            }
            for(int j = 0; j < array.length; j++) {
                next[idx++] = array[j] * 2;
            }
            array = next;
        }
        int[] res = new int[N];
        int index = 0;
        for(int i = 0; i < array.length; i++) {
            if(array[i] <= N) {
                res[index++] = array[i];
            }
        }
        return res;
    }

    public static void main(String[] args) {
        Leetcode932 leetcode = new Leetcode932();
        long l1 = System.currentTimeMillis();
        int[] res = leetcode.beautifulArray(100);
        long l2 = System.currentTimeMillis();
        System.out.println(l2-l1);
        for(int v: res) {
            System.out.print(v+" ");
        }
    }
}

/*
这个题目没有想出来，我自己想到的可能的方法是利用奇偶或者大小来构造。
看到了http://www.noteanddata.com/leetcode-932-Beautiful-Array-java-solution-note.html ，恍然大悟
这个题目数学上容易证明：
    - 如果一个数组为漂亮数组，则每个元素*2也是漂亮数组
    - 如果一个数组为漂亮数组，则每个元素*2-1也是漂亮数组
    - 如果有一个全为奇数的漂亮数组和一个全为偶数的漂亮数组，那么它们拼接后还是漂亮数组
有了以上的证明之后，则可以轻松构造出漂亮数组
复杂度（我一开始也没想明白）： O（n) -> 相当于我们在构造一颗完全树，则节点为1+2+4...+n => O(n)

总结：
    - 这个题目我想到了奇偶，但是我还是在依序构造，但是答案是分别构造奇偶，这个为其他构造题目提供了新思路
    - 复杂度的想法很好！
 */