package henry.leetcode;

import java.util.*;

public class Leetcode863_2 {
    public class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }

    HashMap<Integer, List<Integer>> graph = new HashMap<>();
    public List<Integer> distanceK(TreeNode root, TreeNode target, int K) {
        dfs(root);
        return bfs(target.val, K);
    }

    private List<Integer> bfs(int begin, int k) {
        int step = 0;
        Set<Integer> visited = new HashSet<>();
        List<Integer> list = new LinkedList<>();
        list.add(begin);
        visited.add(begin);
        for(int i = 0; i < k; i++) {
            List<Integer> newList = new LinkedList<>();
            for(int j: list) {
                List<Integer> nei = graph.get(j);
                for(int n: nei) {
                    if(!visited.contains(n)) {
                        newList.add(n);
                        visited.add(n);
                    }
                }
            }
            list = newList;
        }
        return list;
    }

    private void dfs(TreeNode root) {
        if(root == null)    return;
        if(!graph.containsKey(root.val)) {
            graph.put(root.val, new LinkedList<>());
        }
        if(root.left != null) {
            graph.get(root.val).add(root.left.val);
            if(!graph.containsKey(root.left.val)) {
                graph.put(root.left.val, new LinkedList<>());
            }
            graph.get(root.left.val).add(root.val);
            dfs(root.left);
        }
        if(root.right != null) {
            graph.get(root.val).add(root.right.val);
            if(!graph.containsKey(root.right.val)) {
                graph.put(root.right.val, new LinkedList<>());
            }
            graph.get(root.right.val).add(root.val);
            dfs(root.right);
        }
    }
}
