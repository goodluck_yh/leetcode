package henry.leetcode;

/*
Given a string that consists of only uppercase English letters, you can replace any letter in the string with another letter at most k times. Find the length of a longest substring containing all repeating letters you can get after performing the above operations.

Note:
Both the string's length and k will not exceed 104.
Example 1:
Input:
s = "ABAB", k = 2
Output:
4
Explanation:
Replace the two 'A's with two 'B's or vice versa.
 */

public class Leetcode424 {
    public int characterReplacement(String s, int k) {
        int[] cnt = new int[26];
        int res = 0, maxCnt = 0, start = 0;
        for(int i = 0; i < s.length(); i++) {
            maxCnt = Math.max(maxCnt, ++cnt[s.charAt(i) - 'A']);
            while(i - start + 1 - maxCnt > k) {
                --cnt[s.charAt(start++) - 'A'];
                maxCnt = cnt[0];
                for(int j = 1; j < 26; j++) {
                    maxCnt = Math.max(maxCnt, cnt[j]);
                }
            }
            res = Math.max(res, i - start + 1);
        }
        return res;
    }
}

/*
sliding windowing algorithm

The problem says that we can make at most k changes to the string (any character can be replaced with any other character). So, let's say there were no constraints like the k. Given a string convert it to a string with all same characters with minimal changes.
The answer to this is
    length of the entire string - number of times of the maximum occurring character in the string
Given this, we can apply the at most k changes constraint and maintain a sliding window such that
    (length of substring - number of times of the maximum occurring character in the substring) <= k

动态窗口是一类题目的解法，典型问题是在某一个区间内，有某种要求，返回最大区间长度！
 */