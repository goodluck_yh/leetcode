package henry.leetcode;

import java.util.*;

public class Leetcode994 {
    int[] dx = new int[] {0, 1, 0, -1};
    int[] dy = new int[] {1, 0, -1, 0};

    public int orangesRotting(int[][] grid) {
        // find all rotten
        List<int[]> list = new LinkedList<>();
        int m = grid.length, n = grid[0].length, step = 0;
        for(int i = 0; i < m; i++) {
            for(int j = 0; j < n; j++) {
                if(grid[i][j] == 2) {
                    list.add(new int[] {i, j});
                }
            }
        }
        // bfs
        while(list.size() != 0) {
            List<int[]> next = new LinkedList<>();
            for(int[] idx : list) {
                int x = idx[0], y = idx[1];
                grid[x][y] = 3; // 3 means rotten & visited
                for(int i = 0; i < 4; i++) {
                    int x1 = x + dx[i], y1 = y + dy[i];
                    if(x1 >= 0 && x1 < m && y1 >= 0 && y1 < n && grid[x1][y1] == 1) {
                        grid[x1][y1] = 2;
                        next.add(new int[] {x1, y1});
                    }
                }
            }
            list = next;
            step++;
        }
        // check whether all rotten
        for(int i = 0; i < m; i++) {
            for(int j = 0; j < n; j++) {
                if(grid[i][j] == 1)
                    return -1;
            }
        }

        return Math.max(0, step - 1);
    }
}
