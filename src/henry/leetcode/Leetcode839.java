package henry.leetcode;

/*
["tars","rats","arts","star"]
 */
public class Leetcode839 {
    public int numSimilarGroups(String[] A) {
        int N = A.length, L = A[0].length();
        UnionFind uf =  new UnionFind(N);
        for(int i = 0; i < N; i++) {
            for(int j = i+1; j < N; j++) {
                String a = A[i], b = A[j];
                int len = 0;
                for(int k = 0; k < L; k++) {
                    if(a.charAt(k) == b.charAt(k))
                        len++;
                }
                if(len + 2 == L) {
                    uf.union(i, j);
                }
            }
        }
        int res = 0;
        for(int i = 0; i < N; i++) {
            if(uf.parent[i] == i)
                res++;
        }
        return res;
    }

    class UnionFind {
        int[] parent;
        UnionFind(int n) {
            parent = new int[n];
            for(int i = 0; i < n; i++) {
                parent[i] = i;
            }
        }

        void union(int a, int b) {
            int pa = find(a), pb = find(b);
            if(pa != pb) {
                parent[pa] = pb;
            }
        }

        int find(int a) {
            while(parent[a] != a)
                a = parent[a];
            return a;
        }
    }
}
