package henry.leetcode;

public class Leetcode701 {
    class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }

    public TreeNode insertIntoBST(TreeNode root, int val) {
        return helper(root, val);
    }

    private TreeNode helper(TreeNode node ,int val) {
        if(node == null) {
            TreeNode res = new TreeNode(val);
            return res;
        }

        if(val > node.val) {
            node.right = helper(node.right, val);
        } else {
            node.left = helper(node.left, val);
        }
        return node;
    }
}

/*
easy question
 */