package henry.leetcode;

import java.util.HashSet;

import java.util.*;

/*
On a broken calculator that has a number showing on its display, we can perform two operations:
Double: Multiply the number on the display by 2, or;
Decrement: Subtract 1 from the number on the display.
Initially, the calculator is displaying the number X.
Return the minimum number of operations needed to display the number Y.

Example 1:
Input: X = 2, Y = 3
Output: 2
Explanation: Use double operation and then decrement operation {2 -> 4 -> 3}.
 */
public class Leetcode991 {
    HashMap<Integer, Integer> map = new HashMap();
    Set<Integer> visited = new HashSet<>();
    int y;
    public int brokenCalc(int X, int Y) {
        if(X >= Y)
            return X - Y;
        return 1 + Y % 2 == 1 ? brokenCalc(X, Y+1) : brokenCalc(X, Y / 2);
    }

}

/*
这是一道数学题，在contest的时候没有想出来。
官方对于这个解法的解释如下：
Instead of multiplying by 2 or subtracting 1 from X, we could divide by 2 (when Y is even) or add 1 to Y.
The motivation for this is that it turns out we always greedily divide by 2:
If say Y is even, then if we perform 2 additions and one division, we could instead perform one division and one addition for less operations [(Y+2) / 2 vs Y/2 + 1].
If say Y is odd, then if we perform 3 additions and one division, we could instead perform 1 addition, 1 division, and 1 addition for less operations [(Y+3) / 2 vs (Y+1) / 2 + 1].
这个解法不太好想！
 */