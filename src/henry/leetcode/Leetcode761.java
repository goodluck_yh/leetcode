package henry.leetcode;

import java.util.*;

public class Leetcode761 {
    class Solution {
        public String makeLargestSpecial(String S) {
            if(S.length() <= 2) return S;
            int i = 0, j = 0, left = 0;
            List<String> list = new LinkedList<>();
            while(j < S.length()) {
                if(S.charAt(j) == '1') left++;
                else    left--;
                j++;

                if(left == 0) {
                    String sub = S.substring(i+1, j-1);
                    list.add("1" + makeLargestSpecial(sub) + "0");
                    i = j;
                }
            }

            Collections.sort(list, Collections.reverseOrder());
            return String.join("", list);
        }
    }
}

/*
hard
复杂度也不好分析！
 */