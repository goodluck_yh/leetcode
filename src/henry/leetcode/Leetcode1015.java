package henry.leetcode;

import java.util.*;

public class Leetcode1015 {
    public int numDupDigitsAtMostN(int N) {
        ArrayList<Integer> L = new ArrayList<>();
        for(int i = N+1; i > 0; i = i / 10)
            L.add(0, i % 10);
        int res = 0;
        for(int i = 0; i < L.size() - 1; i++) {
            res += 9 * A(9, i);
        }

        HashSet<Integer> set = new HashSet<>();
        for(int i = 0; i < L.size(); i++) {
            for(int j = i == 0 ? 1 : 0; j < L.get(i); j++)
                if(!set.contains(j))
                    res += A(9-i, L.size()-i-1);
            if(set.contains(L.get(i)))  break;
            set.add(L.get(i));
        }

        return N - res;
    }

    /**
     * @param m：total num that can be selected
     * @param n: the total length we need to choose
     * e.g. A(9, 3) -> 9 * 8 * 7
     */
    public int A(int m, int n) {
       return n == 0 ? 1 : A(m, n-1) * (m - n + 1);
    }

    public static void main(String[] args) {
        Leetcode1015 leetcode = new Leetcode1015();
        System.out.println(leetcode.numDupDigitsAtMostN(20));
    }
}
