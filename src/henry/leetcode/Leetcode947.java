package henry.leetcode;

import java.util.*;
/*
On a 2D plane, we place stones at some integer coordinate points.  Each coordinate point may have at most one stone.
Now, a move consists of removing a stone that shares a column or row with another stone on the grid.
What is the largest possible number of moves we can make?

Example 1:
Input: stones = [[0,0],[0,1],[1,0],[1,2],[2,1],[2,2]]
Output: 5

Example 2:
Input: stones = [[0,0],[0,2],[1,1],[2,0],[2,2]]
Output: 3

Example 3:
Input: stones = [[0,0]]
Output: 0

Note:

1 <= stones.length <= 1000
0 <= stones[i][j] < 10000
 */

public class Leetcode947 {
    class UnionFind {
        int[] parent;
        int[] size;

        public UnionFind(int n) {
            parent = new int[n];
            size = new int[n];

            for(int i = 0; i < n; i++) {
                parent[i] = i;
            }

            Arrays.fill(size, 1);
        }

        public void union(int x, int y) {
            int p1 = find(x), p2 = find(y);
            if(p1 == p2) return;
            if(size[p1] < size[p2]) {
                parent[p1] = p2;
                size[p2] += size[p1];
            } else {
                parent[p2] = p1;
                size[p1] += size[p2];
            }
        }

        public int find(int x) {
            while(parent[x] != x){
                parent[x] = parent[parent[x]];
                x = parent[x];
            }
            return x;
        }
    }


    // DFS
    public int removeStones1(int[][] stones) {
        int len = stones.length;
        Set<int[]> visited = new HashSet<>();

        int num = 0;
        for(int i = 0; i < len; i++) {
            int[] start = stones[i];
            if(!visited.contains(start)) {
                num++;
                visited.add(start);
                dfs(stones, visited, start);
            }
        }

        return len - num;
    }

    private void dfs(int[][] stones, Set<int[]> visited, int[] start) {
        for(int i = 0; i < stones.length; i++) {
            int[] temp = stones[i];
            if(!visited.contains(temp)) {
                if(temp[0] == start[0] || temp[1] == start[1]) {
                    visited.add(temp);
                    dfs(stones, visited, temp);
                }
            }

        }
    }

    //union-find
    public int removeStones2(int[][] stones) {
        UnionFind uf = new UnionFind(stones.length);
        for(int i = 0; i < stones.length; i++) {
            for(int j = 0; j < stones.length; j++) {
                if(stones[i][0] == stones[j][0] || stones[i][1] == stones[j][1]) {
                    uf.union(i, j);
                }
            }
        }
        int res = 0;
        for(int i = 0; i < stones.length; i++) {
            if(uf.parent[i] == i)   res++;
        }
        return stones.length - res;
    }

    public static void main(String[] args) {
        Leetcode947 leetcode = new Leetcode947();
        int[][] stones = new int[][] {{0,0},{0,1},{1,0},{1,2},{2,1},{2,2}};
        System.out.println(leetcode.removeStones2(stones));
    }
}



/*
tips for DFS:
1 ConcurrentModificationException
    - The reason is we try to modify list while looping the list
    - One possible solution is we can convert it to Iterator, but it does not work in our scenario
    - Another possible solution is we use for(int i = 0; i < list.size();) to loop list, but it also does not work
    - Our final solution is used Set to record visited
2 DFS timeout
    - copy from array to list is very time-consuming (from 422ms to 23 ms)

总结：
    DFS关键词： 递归，起始点，邻接表（或矩阵）, visited
    DFS复杂度： 邻接矩阵（二维数组）复杂度为O(n^2), 邻接表为O(n+e) e是边数

tips for union-find:
关键字： parent数组，三个函数（构造，union，find）
优化: 两种优化，rank union + path compression
    - rank union:
        size数组存储以i为root的树的大小，根据其大小插入，小插入大
    - path compression:
        每次find的时候，如果不是根节点，就将其赋值为祖父节点

Union-find复杂度：
    - 在一般情况下：find 复杂度为O(n),而union调用了find，所以也是O(n)
    - 使用union by rank： 复杂度降低为O(lgn)
*/