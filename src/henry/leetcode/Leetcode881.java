package henry.leetcode;

import java.util.*;

/*
The i-th person has weight people[i], and each boat can carry a maximum weight of limit.
Each boat carries at most 2 people at the same time, provided the sum of the weight of those people is at most limit.
Return the minimum number of boats to carry every given person.  (It is guaranteed each person can be carried by a boat.)

Example 1:
Input: people = [1,2], limit = 3
Output: 1
Explanation: 1 boat (1, 2)

Example 2:
Input: people = [3,2,2,1], limit = 3
Output: 3
Explanation: 3 boats (1, 2), (2) and (3)

Example 3:
Input: people = [3,5,3,4], limit = 5
Output: 4
Explanation: 4 boats (3), (3), (4), (5)

Note:
1 <= people.length <= 50000
1 <= people[i] <= limit <= 30000
 */

public class Leetcode881 {
    public int numRescueBoats(int[] people, int limit) {
        int res = 0, start = 0, end = people.length-1;
        Arrays.sort(people);
        while(start < end) {
            res ++;
            if(people[start] + people[end] <= limit) {
                start++;
                end--;
            } else {
                end--;
            }
        }
        return start == end ? res + 1 : res;
    }
}

/*
这个题目一开始有猜到贪心，但是没有能够证明，后来看到提示，发现果然是贪心。
贪心的证明：
A easy proof to this greedy algorithm:
assuming we let A(the heaviest) and B(the lightest) go first, now if C and D can't be in the same boat, it must be that A and C or A and D can't be in same boat either.
So the greedy algorithm works.
 */
