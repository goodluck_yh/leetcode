package henry.leetcode;

/*
Given a string S, count the number of distinct, non-empty subsequences of S .
Since the result may be large, return the answer modulo 10^9 + 7.

Example 1:
Input: "abc"
Output: 7
Explanation: The 7 distinct subsequences are "a", "b", "c", "ab", "ac", "bc", and "abc".
 */

public class Leetcode940 {
    public int distinctSubseqII(String S) {
        int[] dp = new int[S.length()];
        dp[0] = 1;
        for(int i = 1; i < S.length(); i++) {
            boolean flag = true;
            for(int j = i-1; j >= 0; j--) {
                dp[i] = (int)((dp[i] + (long)dp[j]) % 1000000007);
                if(S.charAt(i) == S.charAt(j)) {
                    flag = false;
                    break;
                }
            }
            // add itself if never encounter
            if(flag)
                dp[i]++;
        }
        int res = 0;
        for(int i = 0; i < dp.length; i++) {
            res = (res + dp[i]) % 1000000007;
        }
        return res;
    }
}

/*
beat 40.46%
 */