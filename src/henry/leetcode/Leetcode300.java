package henry.leetcode;

import java.util.Arrays;

/*
Given an unsorted array of integers, find the length of longest increasing subsequence.

Example:
Input: [10,9,2,5,3,7,101,18]
Output: 4
Explanation: The longest increasing subsequence is [2,3,7,101], therefore the length is 4.
 */
public class Leetcode300 {
    public int lengthOfLIS(int[] nums) {
        int[] tails = new int[nums.length];
        int res = 0;
        for(int num : nums) {
            int i = Arrays.binarySearch(tails, 0, res, num);
            if(i < 0)   i = -(i+1);
            tails[i] = num;
            res = Math.max(res, i+1);
        }
        return res;
    }
}

/*
LIS问题是个经典问题，在其它问题中会用到这个，其实一个是leetcode354
LIS比较容易想到的方法应该是o(n^2)的算法，但是最优算法o(nlgn)
这个算法虽然使用了binary search，但是更多的是这个算法本身很难想，需要记住
另外一个需要注意的是Arrays.binarySearch(arr, start, end, target)这个方法
 */