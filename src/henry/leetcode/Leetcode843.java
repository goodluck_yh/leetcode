package henry.leetcode;

import java.util.*;
/*
["acckzz","ccbazz","eiowzz","abcczz"]
 */
public class Leetcode843 {

    class Master {
        int guess(String s) {
            return 0;
        }
    }

    int[][] similar;
    int N;
    public void findSecretWord(String[] wordlist, Master master) {
        N = wordlist.length;
        similar = new int[N][N];
        for(int i = 0; i < N; i++) {
            for(int j = i+1; j < N; j++) {
                String a = wordlist[i], b = wordlist[j];
                int cnt = 0;
                for(int k = 0; k < 6; k++) {
                    if(a.charAt(k) == b.charAt(k)) {
                        cnt++;
                    }
                }
                similar[i][j] = similar[j][i] = cnt;
            }
        }
        Set<Integer> all = new HashSet<>();
        for(int i = 0; i < N; i++)  all.add(i);
        while(!all.isEmpty()) {
            int cur = getValue(all);
            int dist = master.guess(wordlist[cur]);
            if(dist == 6)
                return;
            all = getNexts(cur, dist, all);
        }
    }

    private Set<Integer> getNexts(int cur, int dist, Set<Integer> all) {
        Set<Integer> res = new HashSet<>();
        for(int i = 0; i < N; i++) {
            if(all.contains(i) && i != cur && similar[cur][i] == dist) {
                res.add(i);
            }
        }
        return res;
    }

    private int getValue(Set<Integer> all) {
        if(all.size() <= 2) return all.iterator().next();
        int res = -1, max = Integer.MAX_VALUE;
        for(int num: all) {
            int[] cnt = new int[6];
            for(int i = 0; i < N; i++) {
                if(num != i && all.contains(i)) {
                    cnt[similar[num][i]]++;
                }
            }
            int temp = 0;
            for(int i: cnt) {
                temp = Math.max(temp, i);
            }
            if(max > temp) {
                max = temp;
                res = num;
            }
        }
        return res;

    }
}
