package henry.leetcode;

import java.util.*;
/*
On a 2-dimensional grid, there are 4 types of squares:
1 represents the starting square.  There is exactly one starting square.
2 represents the ending square.  There is exactly one ending square.
0 represents empty squares we can walk over.
-1 represents obstacles that we cannot walk over.
Return the number of 4-directional walks from the starting square to the ending square, that walk over every non-obstacle square exactly once.

Example 1:
Input: [[1,0,0,0],[0,0,0,0],[0,0,2,-1]]
Output: 2
Explanation: We have the following two paths:
1. (0,0),(0,1),(0,2),(0,3),(1,3),(1,2),(1,1),(1,0),(2,0),(2,1),(2,2)
2. (0,0),(1,0),(2,0),(2,1),(1,1),(0,1),(0,2),(0,3),(1,3),(1,2),(2,2)
 */
public class Leetcode980 {
    int res = 0;
    int[] dx = new int[] {-1, 0, 1, 0};
    int[] dy = new int[] {0, 1, 0, -1};
    public int uniquePathsIII(int[][] grid) {
        int total = 0, x = 0, y = 0;
        Set<Integer> visited = new HashSet<>();
        for(int i = 0; i < grid.length; i++) {
            for(int j = 0; j < grid[0].length; j++) {
                if(grid[i][j] != -1)
                    total++;
                if(grid[i][j] == 1) {
                    x = i;
                    y = j;
                }
            }
        }
        visited.add(x*grid[0].length + y);
        helper(grid, visited, total, x, y);
        return res;
    }

    private void helper(int[][] grid, Set<Integer> visited, int total, int i, int j) {
        if(grid[i][j] == 2) {
            if(visited.size() == total) res++;
            return;
        }

        for(int k = 0; k < 4; k++) {
            int i_new = i + dx[k];
            int j_new = j + dy[k];
            if(i_new < grid.length && i_new >= 0 && j_new < grid[0].length && j_new >= 0 && grid[i_new][j_new] != -1 && !visited.contains(i_new*grid[0].length+j_new)) {
                visited.add(i_new*grid[0].length+j_new);
                helper(grid, visited, total, i_new, j_new);
                visited.remove(i_new*grid[0].length+j_new);
            }
        }
    }
}
