package henry.leetcode;

import java.util.*;

public class Leetcode939 {
    public int minAreaRect(int[][] points) {
        HashMap<Integer, Set<Integer>> map = new HashMap<>();
        for(int i = 0; i < points.length; i++) {
            if(!map.containsKey(points[i][0])) {
                map.put(points[i][0], new HashSet<>());
            }
            map.get(points[i][0]).add(points[i][1]);
        }
        int res = Integer.MAX_VALUE;
        for(int i = 0; i < points.length; i++) {
            for(int j = i+1; j < points.length; j++) {
                int x1 = points[i][0], y1 = points[i][1], x2 = points[j][0],y2 = points[j][1];
                if(x1 != x2 && y1 != y2) {
                    if(map.get(x1).contains(y2) && map.get(x2).contains(y1)) {
                        res = Math.min(res, Math.abs(x1-x2) * Math.abs(y1-y2));
                    }
                }
            }
        }
        return res == Integer.MAX_VALUE ? 0 : res;
    }

    public static void main(String[] args) {
        Leetcode939 leetcode = new Leetcode939();
        int[][] points = new int[][] {{1,1},{1,3},{3,1},{3,3},{4,1},{4,3}};
        long l1 = System.currentTimeMillis();
        System.out.println(leetcode.minAreaRect(points));
        long l2 = System.currentTimeMillis();
        System.out.println(l2-l1);
    }
}

/*
在set中存储array的方法：
    - 拼接字符串
    - HashMap中套Set或者HashMap
 */