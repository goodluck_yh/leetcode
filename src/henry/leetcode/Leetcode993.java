package henry.leetcode;

public class Leetcode993 {
    public class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }

    class Res {
        int level;
        TreeNode parent;
        public Res (int level, TreeNode parent) {
            this.level = level;
            this.parent = parent;
        }
    }

    public boolean isCousins(TreeNode root, int x, int y) {
        if(x == y || x == root.val || y == root.val)
            return false;
        Res res_x = helper(root, x, 0);
        Res res_y = helper(root, y, 0);
        if(res_x == null || res_y == null)
            return false;
        if(res_x.level == res_y.level && res_x.parent != res_y.parent)
            return true;
        return false;
    }

    private Res helper(TreeNode root, int x, int level) {
        if(root == null)
            return null;
        if(root.left != null && root.left.val == x) {
            Res res = new Res(level, root);
            return res;
        }
        if(root.right != null && root.right.val == x) {
            Res res = new Res(level, root);
            return res;
        }

        Res left = helper(root.left, x, level + 1);
        if(left != null)
            return left;
        return helper(root.right, x, level + 1);
    }
}
