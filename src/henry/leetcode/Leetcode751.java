package henry.leetcode;

import java.util.*;

public class Leetcode751 {
    Set<Integer> set1 = new HashSet<>();
    Set<Integer> set2 = new HashSet<>();
    public boolean isBipartite(int[][] graph) {
        boolean[] visited = new boolean[graph.length];
        for(int i = 0; i < visited.length; i++) {
            if(visited[i])  continue;
            boolean flag = helper(true, graph, visited, i);
            if(!flag)   return false;
        }
        return true;
    }

    private boolean helper(boolean isFirst, int[][] graph, boolean[] visited, int i) {
        if(visited[i]) {
            if(isFirst)
                return set1.contains(i);
            else
                return set2.contains(i);
        };
        visited[i] = true;
        if(isFirst) set1.add(i);
        else    set2.add(i);

        for(int nei: graph[i]) {
            boolean flag = helper(!isFirst, graph, visited, nei);
            if(!flag)   return false;
        }
        return true;
    }
}

/*
答案说是用dfs，但是其实跟我这个做法大同小异

仔细读了读，后来发现dfs想法更好！算法意思是利用dfs上色，我们知道父节点颜色，利用父节点颜色给子节点上色，并进行验证
 */