package henry.leetcode;

import java.util.*;

public class Leetcode961 {
    public int repeatedNTimes(int[] A) {
        Set<Integer> set = new HashSet<>();
        for(int i : A) {
            if(set.contains(i)) return i;
            set.add(i);
        }
        return -1;
    }
}
