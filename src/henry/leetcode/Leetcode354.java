package henry.leetcode;

import java.util.*;

/*
You have a number of envelopes with widths and heights given as a pair of integers (w, h). One envelope can fit into another if and only if both the width and height of one envelope is greater than the width and height of the other envelope.
What is the maximum number of envelopes can you Russian doll? (put one inside other)
Note:
Rotation is not allowed.

Example:
Input: [[5,4],[6,4],[6,7],[2,3]]
Output: 3
Explanation: The maximum number of envelopes you can Russian doll is 3 ([2,3] => [5,4] => [6,7]).
 */
public class Leetcode354 {
    public int maxEnvelopes(int[][] e) {
        int len = e.length, res = 1;
        if(len < 2) return len;
        int[] dp = new int[len];
        Arrays.sort(e, (a,b) -> a[0] == b[0] ? a[1] - b[1] : a[0] - b[0]);
        dp[0] = 1;
        for(int i = 1; i < len; i++) {
            dp[i] = 1;
            for(int j = 0; j < i; j++) {
                if(e[i][0] > e[j][0] && e[i][1] > e[j][1]) {
                    dp[i] = Math.max(dp[i], 1 + dp[j]);
                }
            }
            res = Math.max(res, dp[i]);
        }
        return res;
    }
}

/*
基本dp解法，复杂度o(n^2)
 */

class Leetcode354_2 {
    public int maxEnvelopes(int[][] e) {
        int len = e.length, res = 0;
        if(len < 2) return len;
        int[] dp = new int[len];
        Arrays.sort(e, (a,b) -> a[0] == b[0] ? b[1] - a[1] : a[0] - b[0]);
        for(int[] arr: e) {
            int i = Arrays.binarySearch(dp, 0, res, arr[1]);
            if(i < 0)   i = -(i+1);
            dp[i] = arr[1];
            res = Math.max(res, i+1);
        }
        return res;
    }
}

/*
上面这个做法和Leetcode 300 一样，但是不好想！
相当于排序之后，化简成为仅看第二位形成数组的LIS。
 */