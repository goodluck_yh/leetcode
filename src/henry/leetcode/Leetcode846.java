package henry.leetcode;

import java.util.TreeMap;

/*
Alice has a hand of cards, given as an array of integers.
Now she wants to rearrange the cards into groups so that each group is size W, and consists of W consecutive cards.
Return true if and only if she can.

Example 1:
Input: hand = [1,2,3,6,2,3,4,7,8], W = 3
Output: true
Explanation: Alice's hand can be rearranged as [1,2,3],[2,3,4],[6,7,8].

Example 2:
Input: hand = [1,2,3,4,5], W = 4
Output: false
Explanation: Alice's hand can't be rearranged into groups of 4.

Note:
1 <= hand.length <= 10000
0 <= hand[i] <= 10^9
1 <= W <= hand.length
 */

public class Leetcode846 {
    public boolean isNStraightHand(int[] hand, int W) {
        int len = hand.length;
        if(len % W != 0)   return false;
        TreeMap<Integer, Integer> map = new TreeMap<>();
        for(int i : hand) {
            map.put(i, map.getOrDefault(i, 0) + 1);
        }

        while(map.size() != 0) {
            int min = map.firstKey();
            for(int i = 0; i < W; i++) {
                if(!map.containsKey(min+i)) return false;
                if(map.get(min+i) == 1) map.remove(min+i);
                else    map.put(min+i, map.get(min+i) - 1);
            }
        }
        return true;
    }
}

/*
我的思路：TreeMap. 这个题目和954有点相同，如果一道题目，需要每次找到最小值，通过最小值为基点计算，并且元素可以重复，那么使用TreeMap
复杂度 O(MlgM) M是不同元素个数
 */
