package henry.leetcode;

/*
Given strings S and T, find the minimum (contiguous) substring W of S, so that T is a subsequence of W.
If there is no such window in S that covers all characters in T, return the empty string "".
If there are multiple such minimum-length windows, return the one with the left-most starting index.

Example 1:
Input:
S = "abcdebdde", T = "bde"
Output: "bcde"
Explanation:
"bcde" is the answer because it occurs before "bdde" which has the same length.
"deb" is not a smaller window because the elements of T in the window must occur in order.
 */
public class Leetcode727 {
    public String minWindow(String S, String T) {
        char[] s = S.toCharArray(), t = T.toCharArray();
        int[][] dp = new int[s.length + 1][t.length + 1];
        for(int i = 0; i < dp[0].length; i++) {
            dp[0][i] = -1;
        }

        for(int i = 0; i < dp.length; i++) {
            dp[i][0] = i;
        }

        for(int i = 1; i < dp.length; i++) {
            for(int j = 1; j < dp[0].length; j++) {
                if(s[i-1] == t[j-1]) {
                    dp[i][j] = dp[i-1][j-1];
                } else {
                    dp[i][j] = dp[i-1][j];
                }
            }
        }

        int min=Integer.MAX_VALUE, start = -1, end = -1;
        for(int i = 0; i < dp.length; i++) {
            if(dp[i][t.length] != -1) {
                int len = i - dp[i][t.length];
                if(len < min) {
                    min = len;
                    start =  dp[i][t.length];
                    end = i;
                }
            }
        }

        if(start == -1)
            return "";
        return S.substring(start, end);
    }
}
/*
 beat 50.50%
 这个解法思路可以细细品味，因为它从头到尾推导了dp。也通过例子确定了dp最外围的值，很有启发意义！
 http://www.cnblogs.com/grandyang/p/8684817.html
 */

