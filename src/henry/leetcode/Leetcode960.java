package henry.leetcode;

import java.util.*;

public class Leetcode960 {
    public int minDeletionSize(String[] A) {
        Set<String> visited = new HashSet<>();
        List<Boolean[]> list = new LinkedList<>();
        Boolean[] firstArray = new Boolean[A[0].length()];
        Arrays.fill(firstArray, false);
        list.add(firstArray);
        visited.add(Arrays.toString(firstArray));
        int step = 0;
        while(list.size() != 0) {
            List<Boolean[]> temp = new LinkedList<>();
            for(Boolean[] deleted : list) {
                if(verify(deleted, A))    return step;
                temp.addAll(getAllNei(deleted, visited, A[0]));
            }
            step ++;
            list = temp;
        }
        return 1;
    }

    private List<Boolean[]> getAllNei(Boolean[] deleted, Set<String> visited, String str) {
        List<Boolean[]> res = new LinkedList<>();
        for(int i = 0; i < deleted.length; i++) {
            Boolean[] newDeleted = Arrays.copyOf(deleted, deleted.length);
            newDeleted[i] = !newDeleted[i];
            String temp = Arrays.toString(newDeleted);
            if(!visited.contains(temp)){
                visited.add(temp);
                res.add(newDeleted);
            }
        }
        return res;
    }

    private boolean verify(Boolean[] delete, String[] strs) {
        for(String s: strs) {
            StringBuffer sb = new StringBuffer();
            for(int i = 0; i < delete.length; i++) {
                if(!delete[i])  sb.append(s.charAt(i));
            }

            for(int i = 1; i < sb.length(); i++) {
                if(sb.charAt(i) < sb.charAt(i-1))   return false;
            }

            if(delete[0] == true && delete[4] == true && delete[3] == delete[1] == delete[2] == false) {
                System.out.println(sb.toString());
            }
        }
        return true;
    }
}


/*
Leetcode960 是我想的答案，结果是超时，其实仔细分析一下，就会发现，确实会超时！
长度为n， 那么所有的点的数量是2^n
那么复杂度为 O(2^n)
为什么会想到这个解法： 最小值，很容易想到转化为BFS
 */

class Leetocde960_2 {
    public int minDeletionSize(String[] A) {
        int strLen = A[0].length();
        int[] dp = new int[strLen];
        Arrays.fill(dp, 1);
        int max = 1;
        for(int i = 1; i < strLen; i++) {
            for(int j = 0; j < i; j ++) {
                boolean isSorted = true;
                for(int k = 0; k < A.length; k++) {
                    if(A[k].charAt(i) < A[k].charAt(j)){
                        isSorted = false;
                        break;
                    }
                }

                if(isSorted) {
                    dp[i] = Math.max(dp[i], dp[j] +1);
                }
            }
            max = Math.max(max, dp[i]);
        }
        return strLen - max;
    }
}

/*
最优解一般情况下不是BFS，就是DP，由于之前试过BFS，是不对的，那就试了试DP
只用了6分钟，就解出了题目
思路： 跟之前一个字符串求最长上升子序列一样
复杂度 O（N*N)
 */