package henry.leetcode;

import java.util.Arrays;
import java.util.Stack;

public class Leetcode853 {
    class Info {
        int position;
        int speed;
    }

    public int carFleet(int target, int[] position, int[] speed) {
        Info[] infos = new Info[position.length];
        for(int i = 0; i < position.length; i++) {
            infos[i] = new Info();
            infos[i].position = position[i];
            infos[i].speed = speed[i];
        }
        Arrays.sort(infos, (a, b) -> a.position - b.position);

        double[] times = new double[position.length];
        for(int i = 0; i < infos.length; i++) {
            times[i] =(double) (target - infos[i].position) / infos[i].speed;
        }

        Stack<Double> stack = new Stack<>();
        for(int i = 0; i < times.length; i++) {
            while(!stack.isEmpty() && (Math.abs(stack.peek() - times[i]) < 0.000001 || stack.peek() < times[i])) {
                stack.pop();
            }
            stack.add(times[i]);
        }
        return stack.size();
    }
}
