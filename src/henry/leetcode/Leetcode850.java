package henry.leetcode;

import java.util.*;

public class Leetcode850 {
    public int rectangleArea(int[][] rectangles) {
        List<int[]> events = new LinkedList<>();
        final int OPEN = 0, CLOSE = 1;
        for(int[] r: rectangles) {
            int x1 = r[0], y1 = r[1], x2 = r[2], y2 = r[3];
            events.add(new int[] {y1, OPEN, x1, x2});
            events.add(new int[] {y2, CLOSE, x1, x2});
        }
        Collections.sort(events, (a, b)-> a[0] - b[0]);
        int cury = events.get(0)[0];
        long res = 0;
        List<int[]> intervals = new LinkedList<>();
        for(int[] event: events) {
            int y = event[0], type = event[1], x1 = event[2], x2 = event[3];
            int length = getTotalLength(intervals);
            res = (res + (long)length * (y - cury)) % (long)(1e9 + 7);
            if(type == OPEN) {
                intervals.add(new int[]{x1, x2});
                Collections.sort(intervals, (a,b) -> a[0] - b[0]);
            } else {
                for(int i = 0; i < intervals.size(); i++) {
                    if(intervals.get(i)[0] == x1 && intervals.get(i)[1] == x2) {
                        intervals.remove(i);
                        break;
                    }
                }
            }
            cury = y;
        }
        return (int)(res % (1e9 + 7));
    }

    private int getTotalLength(List<int[]> intervals) {
        int length = 0, cur = -1;
        for(int[] interval: intervals) {
            int x1 = interval[0], x2 = interval[1];
            if(cur == -1) {
                length = x2 - x1;
                cur = x2;
            } else {
                cur = Math.max(cur, x1);
                length += Math.max(0, x2 - cur);
                cur = Math.max(cur, x2);
            }
        }
        return length;
    }
}

/*
beat 75.41 %
 */

class Leetcode850_2 {
    public int rectangleArea(int[][] rectangles) {
        List<int[]> events = new LinkedList<>();
        final int OPEN = 0, CLOSE = 1;
        for(int[] r: rectangles) {
            int x1 = r[0], y1 = r[1], x2 = r[2], y2 = r[3];
            events.add(new int[] {y1, OPEN, x1, x2});
            events.add(new int[] {y2, CLOSE, x1, x2});
        }
        Collections.sort(events, (a, b)-> a[0] - b[0]);
        int cury = events.get(0)[0];
        long res = 0;
        List<int[]> intervals = new LinkedList<>();
        for(int[] event: events) {
            int y = event[0], type = event[1], x1 = event[2], x2 = event[3];
            int length = getTotalLength(intervals);
            res = (res + (long)length * (y - cury)) % (long)(1e9 + 7);
            if(type == OPEN) {
                intervals.add(new int[]{x1, x2});
                Collections.sort(intervals, (a,b) -> a[0] - b[0]);
            } else {
                for(int i = 0; i < intervals.size(); i++) {
                    if(intervals.get(i)[0] == x1 && intervals.get(i)[1] == x2) {
                        intervals.remove(i);
                        break;
                    }
                }
            }
            cury = y;
        }
        return (int)(res % (1e9 + 7));
    }

    private int getTotalLength(List<int[]> intervals) {
        int length = 0, cur = -1;
        for(int[] interval: intervals) {
            int x1 = interval[0], x2 = interval[1];
            if(cur == -1) {
                length = x2 - x1;
                cur = x2;
            } else {
                cur = Math.max(cur, x1);
                length += Math.max(0, x2 - cur);
                cur = Math.max(cur, x2);
            }
        }
        return length;
    }
}