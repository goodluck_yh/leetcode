package henry.leetcode;


/*
[
[".",".",".",".",".",".",".","."],
[".",".",".","p",".",".",".","."],
[".",".",".","R",".",".",".","p"],
[".",".",".",".",".",".",".","."],
[".",".",".",".",".",".",".","."],
[".",".",".","p",".",".",".","."],
[".",".",".",".",".",".",".","."],
[".",".",".",".",".",".",".","."]
]

=>
0
 */
public class Leetcode999 {
    public int numRookCaptures(char[][] board) {
        int i = 0, j = 0;
        boolean flag = true;
        for(i = 0; i < 8 && flag; i++) {
            for(j = 0; j < 8; j++) {
                if(board[i][j] == 'R'){
                    flag = false;
                    break;
                }
            }
        }
        i--;
        int res = 0;
        for(int x = i-1; x >= 0; x--) {
            //System.out.println(board[x][j]);
            if(board[x][j] != '.') {
                if(board[x][j] == 'p')
                    res++;
                break;
            }
        }
        //System.out.println(res);
        for(int x =i+1; x < 8; x++) {
            if(board[x][j] != '.') {
                if(board[x][j] == 'p')
                    res++;
                break;
            }
        }
        //System.out.println(res);
        for(int y =j-1; y >= 0; y--) {
            if(board[i][y] != '.') {
                if(board[i][y] == 'p')
                    res++;
                break;
            }
        }
        //System.out.println(res);
        for(int y =j+1; y < 8; y++) {
            if(board[i][y] != '.') {
                if(board[i][y] == 'p')
                    res++;
                break;
            }
        }
        //System.out.println(res);
        return res;
    }
}
