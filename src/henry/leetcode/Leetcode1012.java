package henry.leetcode;

public class Leetcode1012 {
    public int bitwiseComplement(int N) {
        if(N == 0)
            return 1;
        if(N == 1)
            return 0;
        int max = 1, sum = 1;
        while(max * 2 <= N)  {
            max = max * 2;
            sum += max;
        }
        return sum - N;
    }
}
