package henry.leetcode;


/*
Write an iterator that iterates through a run-length encoded sequence.
The iterator is initialized by RLEIterator(int[] A), where A is a run-length encoding of some sequence.  More specifically, for all even i, A[i] tells us the number of times that the non-negative integer value A[i+1] is repeated in the sequence.
The iterator supports one function: next(int n), which exhausts the next n elements (n >= 1) and returns the last element exhausted in this way.  If there is no element left to exhaust, next returns -1 instead.
For example, we start with A = [3,8,0,9,2,5], which is a run-length encoding of the sequence [8,8,8,5,5].  This is because the sequence can be read as "three eights, zero nines, two fives".

Example 1:
Input: ["RLEIterator","next","next","next","next"], [[[3,8,0,9,2,5]],[2],[1],[1],[2]]
Output: [null,8,8,5,-1]

Note:
0 <= A.length <= 1000
A.length is an even integer.
0 <= A[i] <= 10^9
There are at most 1000 calls to RLEIterator.next(int n) per test case.
Each call to RLEIterator.next(int n) will have 1 <= n <= 10^9.
 */

public class Leetcode900 {
    private int[] array;
    private int index;
    public Leetcode900(int[] A) {
        array = A;
        index = 0;
    }

    public int next(int n) {
        for(; index < array.length; index = index + 2) {
            if(array[index] == 0)   continue;
            if(array[index] >= n) {
                array[index] = array[index] - n;
                return array[index+1];
            } else {
                n = n - array[index];
                array[index] = 0;
            }

        }
        return -1;
    }
}

/*
日常水题
 */
