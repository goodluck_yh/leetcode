package henry.leetcode;

import java.util.*;

/*
要求 in-place
 */

public class Leetcode426 {
    class Node {
        public int val;
        public Node left, right;
    };

    List<Node> list = new LinkedList<>();
    public Node treeToDoublyList(Node root) {
        inorder(root);
        if(list.size() == 0)    return null;
        Node head = list.get(0);
        for(int i = 1; i < list.size(); i++) {
            Node c = list.get(i);
            Node p = list.get(i-1);
            p.right = c;
            c.left = p;
        }
        head.left = list.get(list.size() - 1);
        list.get(list.size() - 1).right = head;
        return head;
    }

    private void inorder(Node node) {
        if(node == null)
            return;
        inorder(node.left);
        list.add(node);
        inorder(node.right);
    }
}

class Leetcode426_2 {
    class Node {
        public int val;
        public Node left, right;
    };
    public Node treeToDoublyList(Node root) {
        if(root == null)
            return null;
        Node pre = treeToDoublyList(root.left);
        Node next = treeToDoublyList(root.right);
        root.left = root;
        root.right = root;
        return connect(connect(pre, root), next);
    }

    private Node connect(Node n1, Node n2) {
        if(n1 == null)
            return n2;
        if(n2 == null)
            return n1;
        Node t1 = n1.left;
        Node t2 = n2.left;
        t1.right = n2;
        n2.left = t1;
        t2.right = n1;
        n1.left = t2;
        return n1;
    }
}

/*
解法一自己想的，解法二是看了别人的答案。两者复杂度一样， 但是解法二的思路很好，并且beat100%
 */