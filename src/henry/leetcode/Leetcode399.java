package henry.leetcode;

import java.util.*;

/*
quations are given in the format A / B = k, where A and B are variables represented as strings, and k is a real number (floating point number). Given some queries, return the answers. If the answer does not exist, return -1.0.

Example:
Given a / b = 2.0, b / c = 3.0.
queries are: a / c = ?, b / a = ?, a / e = ?, a / a = ?, x / x = ? .
return [6.0, 0.5, -1.0, 1.0, -1.0 ].

The input is: vector<pair<string, string>> equations, vector<double>& values, vector<pair<string, string>> queries , where equations.size() == values.size(), and the values are positive. This represents the equations. Return vector<double>.

According to the example above:

equations = [ ["a", "b"], ["b", "c"] ],
values = [2.0, 3.0],
queries = [ ["a", "c"], ["b", "a"], ["a", "e"], ["a", "a"], ["x", "x"] ].
The input is always valid. You may assume that evaluating the queries will result in no division by zero and there is no contradiction.
 */

public class Leetcode399 {
    HashMap<String, HashMap<String, Double>> map = new HashMap<>();
    public double[] calcEquation(String[][] equations, double[] values, String[][] queries) {
        Set<String> dict = new HashSet<>();
        for(int i = 0; i < equations.length; i++) {
            dict.add(equations[i][0]);
            dict.add(equations[i][1]);
            HashMap<String, Double> map1 = map.getOrDefault(equations[i][0], new HashMap<>());
            map1.put(equations[i][1], values[i]);
            map.put(equations[i][0], map1);

            map1 = map.getOrDefault(equations[i][1], new HashMap<>());
            map1.put(equations[i][0], 1.0/values[i]);
            map.put(equations[i][1], map1);
        }

        double[] res = new double[queries.length];
        for(int i = 0; i < res.length; i++) {
            if(!dict.contains(queries[i][0]) || !dict.contains(queries[i][1])) {
                res[i] = -1.0;
                continue;
            }

            if(map.containsKey(queries[i][0]) && map.get(queries[i][0]).containsKey(queries[i][1])) {
                res[i] = map.get(queries[i][0]).get(queries[i][1]);
                continue;
            }

            if(queries[i][0].equals(queries[i][1])) {
                res[i] = 1.0;
                continue;
            }

            res[i] = dfs(queries[i][0], queries[i][1], new HashSet<>());
            res[i] = res[i] == Double.MIN_VALUE ? -1.0 : res[i];
        }
        return res;
    }

    private double dfs(String start, String end, Set<String> visited) {
        if(map.containsKey(start) && map.get(start).containsKey(end))
            return map.get(start).get(end);
        if(!map.containsKey(start)) return Double.MIN_VALUE;

        HashMap<String, Double> map1 = map.get(start);
        for(String s: map1.keySet()) {
            if(!visited.contains(s)) {
                visited.add(s);
                double d = dfs(s, end, visited);
                if(d != Double.MIN_VALUE) {
                    double res = map.get(start).get(s) * d;
                    map1.put(end, res);
                    map.put(start, map1);
                    return res;
                }
            }
        }
        return Double.MIN_VALUE;
    }
}

/*
不难想，但是有几个corner case.
- a/a
- 已知 a/b 求 b/a 其他就没有了
 */
