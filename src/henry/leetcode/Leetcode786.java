package henry.leetcode;

import java.util.*;

/*
A sorted list A contains 1, plus some number of primes.  Then, for every p < q in the list, we consider the fraction p/q.
What is the K-th smallest fraction considered?  Return your answer as an array of ints, where answer[0] = p and answer[1] = q.

Examples:
Input: A = [1, 2, 3, 5], K = 3
Output: [2, 5]
Explanation:
The fractions to be considered in sorted order are:
1/5, 1/3, 2/5, 1/2, 3/5, 2/3.
The third fraction is 2/5.
Input: A = [1, 7], K = 1
Output: [1, 7]
 */
public class Leetcode786 {
    public int[] kthSmallestPrimeFraction(int[] A, int K) {
        PriorityQueue<int[]> pq = new PriorityQueue<>((a,b) -> A[a[0]] * A[b[1]] - A[a[1]] * A[b[0]]);
        for(int i = 1; i < A.length; i++) {
            pq.add(new int[] {0, i});
        }
        while(--K > 0) {
            int[] arr = pq.poll();
            pq.add(new int[] {arr[0]+1, arr[1]});
        }
        return new int[] {A[pq.peek()[0]], A[pq.peek()[1]]};
    }
}

/*
解法一： PriorityQueue
这个解法是K smallest 或者 k largest 的常见思路，但是这个题目却并没有使用更常见的大顶堆，而是用了小顶堆。
特点在于，某些情况下， 我们可以不能完全排序，但是却可以部分排序。

另外一个值得一说的地方是PriorityQueue的lambda表达式，它一定要返回int，我们应该避免使用除法，而是用乘法
 */

class Leetcode786_2 {
    public int[] kthSmallestPrimeFraction(int[] A, int K) {
        double hi = 1, lo = 0;
        while(true) {
            double mid = (hi + lo) / 2;
            int cnt = 0, p = 0, q = A.length-1;
            for(int i = 0, j= i+1; i < A.length; i++) {
                while(j < A.length && A[i] > A[j] * mid)
                    j++;
                cnt = cnt + A.length - j;
                if(j < A.length && A[p] * A[j] < A[i] * A[q]) {
                    p = i;
                    q = j;
                }
            }
            if(cnt == K)
                return new int[] {A[p], A[q]};
            else if(cnt > K)
                hi = mid;
            else
                lo = mid;
        }
    }
}

/*
第二种做法，binary search
复杂度不知道，但是根据leetcode结果，很快
http://www.cnblogs.com/grandyang/p/9135156.html
leetcode 668, leetcode 378， leetcode 719 都是这个解法
 */