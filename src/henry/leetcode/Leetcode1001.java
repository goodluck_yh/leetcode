package henry.leetcode;

/*
Input: N = 5, lamps = [[0,0],[4,4]], queries = [[1,1],[1,0]]
Output: [1,0]
Explanation:
Before performing the first query we have both lamps [0,0] and [4,4] on.
The grid representing which cells are lit looks like this, where [0,0] is the top left corner, and [4,4] is the bottom right corner:
1 1 1 1 1
1 1 0 0 1
1 0 1 0 1
1 0 0 1 1
1 1 1 1 1
Then the query at [1, 1] returns 1 because the cell is lit.  After this query, the lamp at [0, 0] turns off, and the grid now looks like this:
1 0 0 0 1
0 1 0 0 1
0 0 1 0 1
0 0 0 1 1
1 1 1 1 1
Before performing the second query we have only the lamp [4,4] on.  Now the query at [1,0] returns 0, because the cell is no longer lit.

Note:
1 <= N <= 10^9
0 <= lamps.length <= 20000
0 <= queries.length <= 20000
 */
import java.util.*;

public class Leetcode1001 {
    int[] dx = {-1, 0, 1, -1, 1, -1, 0 ,1};
    int[] dy = {-1, -1, -1, 0, 0, 1, 1, 1};

    public int[] gridIllumination(int N, int[][] lamps, int[][] queries) {
        HashMap<Integer, Integer> row = new HashMap<>(), column = new HashMap<>(), down = new HashMap<>(), up = new HashMap<>();
        //int[][] board = new int[N][N];
        HashMap<Integer, Set<Integer>> board = new HashMap<>();
        for(int[] lamp: lamps) {
            int x = lamp[0], y = lamp[1];
            row.put(x, row.getOrDefault(x, 0) + 1);
            column.put(y, column.getOrDefault(y, 0) + 1);
            down.put(x-y, down.getOrDefault(x-y, 0) + 1);
            up.put(x+y, up.getOrDefault(x+y, 0) + 1);
            //board[x][y] = 1;
            if(!board.containsKey(x)) board.put(x, new HashSet<>());
            board.get(x).add(y);
        }
        int[] res = new int[queries.length];
        for(int i = 0; i < queries.length; i++) {
            int x = queries[i][0], y = queries[i][1];
            if(row.getOrDefault(x, 0) > 0 || column.getOrDefault(y, 0) > 0 || down.getOrDefault(x-y, 0) > 0 || up.getOrDefault(x+y, 0) > 0) {
                res[i] = 1;
                for(int j = 0; j < 8; j++) {
                    int x1 = x + dx[j], y1 = y + dy[j];
                    //if(x1 >= 0 && x1 < N && y1 >= 0 && y1 < N && board[x1][y1] == 1) {
                    if(x1 >= 0 && x1 < N && y1 >= 0 && y1 < N && board.containsKey(x1) && board.get(x1).contains(y1)) {
                        row.put(x1, row.get(x1) - 1);
                        column.put(y1, column.get(y1) - 1);
                        down.put(x1-y1, down.get(x1-y1) - 1);
                        up.put(x1+y1, up.get(x1+y1) -1);
                        board.get(x1).remove(y1);
                    }
                }
                //if(board[x][y] == 1) {
                if(board.containsKey(x) && board.get(x).contains(y)) {
                    row.put(x, row.get(x) - 1);
                    column.put(y, column.get(y) - 1);
                    down.put(x-y, down.get(x-y) - 1);
                    up.put(x+y, up.get(x+y) -1);
                    board.get(x).remove(y);
                }
            } else {
                res[i] = 0;
            }
        }
        return res;
    }

}
