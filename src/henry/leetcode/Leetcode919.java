package henry.leetcode;

import java.util.*;

/*
A complete binary tree is a binary tree in which every level, except possibly the last, is completely filled, and all nodes are as far left as possible.
Write a data structure CBTInserter that is initialized with a complete binary tree and supports the following operations:
CBTInserter(TreeNode root) initializes the data structure on a given tree with head node root;
CBTInserter.insert(int v) will insert a TreeNode into the tree with value node.val = v so that the tree remains complete, and returns the value of the parent of the inserted TreeNode;
CBTInserter.get_root() will return the head node of the tree.

Example 1:
Input: inputs = ["CBTInserter","insert","get_root"], inputs = [[[1]],[2],[]]
Output: [null,1,[1,2]]

Example 2:
Input: inputs = ["CBTInserter","insert","insert","get_root"], inputs = [[[1,2,3,4,5,6]],[7],[8],[]]
Output: [null,3,4,[1,2,3,4,5,6,7,8]]

Note:
The initial given tree is complete and contains between 1 and 1000 nodes.
CBTInserter.insert is called at most 10000 times per test case.
Every value of a given or inserted node is between 0 and 5000.
 */

public class Leetcode919 {
    public class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }

    private TreeNode root;
    private List<TreeNode> secLast;
    private List<TreeNode> last;

    public Leetcode919(TreeNode root) {
        this.root = root;
        secLast = new LinkedList<>();
        last = new LinkedList<>();
        secLast.add(root);
        int full = 1;
        while (true) {
            full = full * 2;
            for (TreeNode n : secLast) {
                if (n.left != null) last.add(n.left);
                if (n.right != null) last.add(n.right);
            }
            if (last.size() != full) {
                for (int i = 0; i < last.size() / 2; i++) {
                    secLast.remove(0);
                }
                break;
            }
            secLast = last;
            last = new LinkedList<>();
        }
    }

    public int insert(int v) {
        TreeNode n = secLast.get(0);
        TreeNode newNode = new TreeNode(v);
        if (n.left == null) {
            n.left = newNode;
        } else {
            n.right = newNode;
            secLast.remove(0);
        }
        last.add(newNode);
        if (secLast.size() == 0) {
            secLast = last;
            last = new LinkedList<>();
        }
        return n.val;
    }

    public TreeNode get_root() {
        return root;
    }
}

/*
题目不难，考点是层遍历。唯一的bug是初始化时，需要删除倒数第二行list中已经有左右节点的节点
 */
