package henry.leetcode;

public class Leetcode702 {
    class ArrayReader {
        int[] arr;
        public ArrayReader(int[] arr) {
            this.arr = new int[10000];
            // init this.arr with arr
            for(int i = 0; i < arr.length; i++) {

            }
        }

        public int get(int i) {
            return arr[i];
        }
    }

    public int search(ArrayReader reader, int target) {
        int length = findLength(reader);
        if(length == 0) return -1;
        System.out.println(length);
        return findEle(reader, target, length);
    }

    private int findEle(ArrayReader reader, int target, int length) {
        int start = 0, end = length - 1;
        int mid;
        while(start < end) {
            mid = (start + end) / 2;
            if(reader.get(mid) == target)
                return mid;
            if(reader.get(mid) > target) {
                end = mid - 1;
            } else {
                start = mid + 1;
            }
        }
        if(reader.get(start) == target)
            return start;
        if(reader.get(end) == target)
            return end;
        return -1;
    }

    private int findLength(ArrayReader reader) {
        int start = 0, end = 10000;
        int mid;
        while(start + 1 < end) {
            mid = (start + end) / 2;
            if(reader.get(mid) != 2147483647) {
                start = mid + 1;
            } else {
                end = mid;
            }
        }
        if(reader.get(start) == 2147483647)
            return start;
        return end;
    }
}
