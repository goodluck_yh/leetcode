package henry.leetcode;

import java.util.Random;

/*
Given an array w of positive integers, where w[i] describes the weight of index i, write a function pickIndex which randomly picks an index in proportion to its weight.
Note:
1 <= w.length <= 10000
1 <= w[i] <= 10^5
pickIndex will be called at most 10000 times.

Example 1:
Input:
["Solution","pickIndex"]
[[[1]],[]]
Output: [null,0]

Example 2:
Input:
["Solution","pickIndex","pickIndex","pickIndex","pickIndex","pickIndex"]
[[[1,3]],[],[],[],[],[]]
Output: [null,0,1,1,1,0]
Explanation of Input Syntax:

The input is two lists: the subroutines called and their arguments. Solution's constructor has one argument, the array w. pickIndex has no arguments. Arguments are always wrapped with a list, even if there aren't any.
 */

public class Leetcode528 {
    private int[] presum;
    private Random rand = new Random();
    public Leetcode528(int[] w) {
        presum = new int[w.length];
        presum[0] = w[0];
        for(int i = 1; i < w.length; i++) {
            presum[i] = presum[i-1] + w[i];
        }
    }

    public int pickIndex() {
        int nextInt = rand.nextInt(presum[presum.length-1]);
        return binarySearch(nextInt);
    }

    private int binarySearch(int target) {
        int lo = 0, hi = presum.length - 1;
        while(lo + 1 < hi) {
            int mid = (lo + hi) / 2;
            if(presum[mid] == target)   return mid;
            if(presum[mid] > target) hi = mid;
            else lo = mid + 1;
        }
        if(lo == hi)    return lo;
        return presum[lo] > target ? lo : hi;
    }

    public static void main(String[] args) {
        Leetcode528 leetcode = new Leetcode528(new int[] {3, 14, 1, 7});
        System.out.println(leetcode.pickIndex());
        System.out.println(leetcode.pickIndex());
        System.out.println(leetcode.pickIndex());
        System.out.println(leetcode.pickIndex());
        System.out.println(leetcode.pickIndex());
    }
}

/*
和Leetcode 497 是一类题目，本质是加权随机抽样。同样的问题没有跑过test case
 */
