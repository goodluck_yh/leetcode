package henry.leetcode;

import java.util.HashMap;

public class Leetcode879 {
    HashMap<String, Integer> map = new HashMap<>();
    int[] group, profit;
    int mod = (int)1e9 + 7;
    public int profitableSchemes(int G, int P, int[] group, int[] profit) {
        this.group = group;
        this.profit = profit;
        return helper(G, P, group.length - 1);
    }

    private int helper(int g, int p, int idx) {
        String key = g + "#" + (p > 0 ? p : 0 )+ "#" + idx;
        if(map.containsKey(key)) {
            return map.get(key);
        }
        if(idx == -1) {
            return p <= 0 ? 1 : 0;
        }
        int skip = helper(g, p, idx-1), notSkip = g >= group[idx] ? helper(g-group[idx], p-profit[idx], idx-1) : 0;
        int sum = (skip + notSkip) % mod;
        map.put(key, sum);
        return sum;
    }
}

/*
beat 2.92%
 */