package henry.leetcode;

import java.util.HashMap;

public class Leetcode957 {
    public int[] prisonAfterNDays(int[] cells, int N) {
        HashMap<Integer, Integer> cellsToIndex = new HashMap<>();
        HashMap<Integer, Integer> indexToCell = new HashMap<>();
        cellsToIndex.put(convert(cells), 0);
        for(int i = 1; i <= N; i++) {
            int[] newCells = new int[cells.length];
            newCells[0] = newCells[7] = 0;
            for(int j = 1; j < 7; j++) {
                if(cells[j-1] == cells[j+1])
                    newCells[j] = 1;
                else
                    newCells[j] = 0;
            }
            cells = newCells;
            int convertRes = convert(cells);
            if(cellsToIndex.containsKey(convertRes))
                break;
            else {
                cellsToIndex.put(convertRes, i);
                indexToCell.put(i, convertRes);
            }
        }

        if(cellsToIndex.size() == N+1)   return cells;
        int last = cellsToIndex.size();
        int first = cellsToIndex.get(convert(cells));
        int n1 = N - first;
        int n2 = n1 % (last - first);

        return convertBack(indexToCell.get(first + n2));
    }

    private int[] convertBack(int n) {
        int[] res = new int[8];
        for(int i = 0; i < n; i++) {
            int temp = n % 2;
            res[7-i] = temp;
            n = n / 10;
        }
        return res;
    }

    private int convert(int[] cells) {
        int res = 0;
        for(int i = 0; i < cells.length; i ++) {
            res = res * 10 + cells[i];
        }
        return res;
    }
}
