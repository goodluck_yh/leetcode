package henry.leetcode;

import java.util.LinkedList;

/*
Input: ["@.a.#","###.#","b.A.B"]
Output: 8
 */
public class Leetcode864 {
    int[] dx = {-1, 0, 1, 0};
    int[] dy = {0, 1, 0, -1};
    class State {
        int key, x, y;
        State(int key, int x, int y) {
            this.key = key;
            this.x = x;
            this.y = y;
        }
    }

    public int shortestPathAllKeys(String[] grid) {
        int cnt = 0, si = 0, sj = 0;
        for(int i = 0; i < grid.length; i++) {
            for(int j = 0; j < grid[0].length(); j++) {
                char ch = grid[i].charAt(j);
                if(ch != '.' && ch != '#' && ch != '@') {
                    cnt++;
                }
                else if(ch == '@') {
                    si = i;
                    sj = j;
                }
            }
        }
        cnt = cnt / 2;
        boolean[][][] visited = new boolean[grid.length][grid[0].length()][64];
        visited[si][sj][0] = true;
        LinkedList<State> list = new LinkedList<>();
        list.add(new State(0, si, sj));
        int step = 0;
        while(!list.isEmpty()) {
            int size = list.size();
            while(size-- > 0) {
                State state = list.pop();
                int key = state.key, i = state.x, j = state.y;
                if(key == (1 << cnt) - 1) {
                    return step;
                }
                for(int idx = 0; idx < 4; idx++) {
                    int x = i + dx[idx], y = j + dy[idx];
                    if(x < 0 || x >= grid.length || y < 0 || y >= grid[0].length()) continue;
                    char ch = grid[x].charAt(y);
                    if(ch == '#')   continue;
                    if(ch >= 'a' && ch <= 'f')
                        key = key | 1 << (ch - 'a');
                    if(ch >= 'A' && ch <= 'F' && ((key >> (ch - 'A')) & 1) == 0) continue;
                    if(!visited[x][y][key]){
                        visited[x][y][key] = true;
                        list.add(new State(key, x, y));
                    }

                }
            }
            step++;
        }
        return -1;
    }
}
