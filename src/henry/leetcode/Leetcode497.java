package henry.leetcode;

import java.util.Random;

public class Leetcode497 {
    class Solution {
        private int[][] rects;
        private int[] accu;
        private
        Random rand;
        public Solution(int[][] rects) {
            this.rects = rects;
            accu = new int[rects.length];
            for(int i = 0; i < accu.length; i++) {
                int size = getSize(rects[i]);
                if(i == 0) {
                    accu[i] = size;
                } else {
                    accu[i] = accu[i-1] + size;
                }
            }
            rand = new Random();
        }

        private int getSize(int[] rect) {
            int x1 = rect[0], y1 = rect[1], x2 = rect[2], y2 = rect[3];
            return (x2-x1+1) * (y2-y1+1);
        }

        public int[] pick() {
            int randInt = rand.nextInt(accu[accu.length-1]);
            int index = binarySearch(randInt);
            if(index > 0)
                randInt -= accu[index-1];
            return getPoint(randInt, rects[index]);

        }

        private int[] getPoint(int num, int[] rect) {
            int x1 = rect[0], y1 = rect[1], x2 = rect[2], y2 = rect[3];
            int x = x2 - x1 + 1;
            int y = y2 - y1 + 1;
            return new int[] {x1 + num % x, y1 + num / x};
        }

        private int binarySearch(int num) {
            int begin = 0, end = accu.length-1, mid = (begin+end)/2;
            while(begin + 1 < end) {
                mid = (begin+end) / 2;
                if(accu[mid] == num)  return mid;
                else if(accu[mid] > num)  end = mid;
                else begin = mid + 1;
            }
            if(begin == end) {
                return begin;
            } else {
                return accu[begin] > num ? begin : end;
            }
        }

    }
}



/*
思路：
    计算所有矩阵点的和，按照矩阵点的和为上边界，随机一个数，再按照这个数字计算出那个点
    初始化：o(n) pick: o(lgn)
我想到了这个思路，但是这个题目没有AC，不知道为什么
随机算法其实是一类题目，可以一起做掉！
其中一个经典算法是水塘抽样算法：https://blog.csdn.net/My_Jobs/article/details/48372399
 */
