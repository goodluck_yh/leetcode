package henry.leetcode;

import java.util.*;

public class Leetcode767 {
    class Info {
        char c;
        int count;
        public Info(char c, int count) {
            this.c = c;
            this.count = count;
        }
    }

    public String reorganizeString(String S) {
        char[] chs = S.toCharArray();
        int[] counts = new int[26];
        int max = 0;
        for(char c: chs) {
            counts[c - 'a'] += 1;
            max = Math.max(max, counts[c - 'a']);
        }
        if(max > (chs.length + 1) / 2)
            return "";
        PriorityQueue<Info> pq = new PriorityQueue<>((a, b) -> b.count - a.count);
        for(int i = 0; i < 26; i++) {
            Info info = new Info((char)('a' + i), counts[i]);
            pq.add(info);
        }

        char[] chs2 = new char[chs.length];
        Arrays.fill(chs2, (char)1);
        Info info = pq.poll();
        for(int i = 0; i < chs2.length; i += 2) {
            chs2[i] = info.c;
            info.count--;
            if(info.count == 0) {
                info = pq.poll();
            }
        }

        for(int i = 1; i < chs2.length; i += 2) {
            chs2[i] = info.c;
            info.count--;
            if(info.count == 0) {
                info = pq.poll();
            }
        }
        return new String(chs2);
    }
}

/*
利用堆，思路好想，也好写
复杂度O(nlgn)

下面这个题目是非排序的算法，其实，根本不需要pq，因为如果最多的元素不会造成问题，后面任何一个元素都不会造成问题
复杂度o(n) beat 98.83%
 */

class Leetcode767_2 {

    public String reorganizeString(String S) {
        char[] chs = S.toCharArray();
        int[] counts = new int[26];
        int max = 0;
        char maxCh = '#';
        for(char c: chs) {
            counts[c - 'a'] += 1;
            if(counts[c - 'a'] > max) {
                max = counts[c - 'a'];
                maxCh = c;
            }
        }
        if(max > (chs.length + 1) / 2)
            return "";

        char[] chs2 = new char[chs.length];
        Arrays.fill(chs2, (char)1);
        int index = 0;
        for(int i = 0; i < chs2.length; i += 2) {
            if(max != 0) {
                max--;
                chs2[i] = maxCh;
            } else {
                while(index == maxCh - 'a' || counts[index] == 0)  index++;
                chs2[i] = (char)(index + 'a');
                counts[index]--;
            }
        }

        for(int i = 1; i < chs2.length; i += 2) {
            while(index == maxCh - 'a' || counts[index] == 0)  index++;
            chs2[i] = (char)(index + 'a');
            counts[index]--;
        }
        return new String(chs2);
    }
}