package henry.leetcode;

/*
题目有图，详见leetcode
 */
public class Leetcode858 {
    class Solution {
        public int mirrorReflection(int p, int q) {
            boolean up = true;
            boolean right = false;
            int cur = 0;
            while(cur != p) {
                right = !right;
                if(up){
                    cur = cur + q;
                    if(cur == p) {
                        return right ? 1 : 2;
                    }
                    if(cur > p) {
                        up = false;
                        cur = p - (cur - p);
                    }
                }else {
                    cur = cur - q;
                    if(cur == 0)    return 0;
                    if(cur < 0) {
                        up = true;
                        cur = -cur;
                    }
                }
            }
            return -1;
        }
    }
}

/*
完整的写了出来，有一个小bug
能想出来的关键是我画了正方形长度为5， 第一次高度为2的情况。

还有一个更牛逼的解法：https://buptwc.com/2018/06/26/Leetcode-858-Mirror-Reflection/
 */