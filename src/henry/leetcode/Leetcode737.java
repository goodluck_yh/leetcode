package henry.leetcode;

import java.util.HashMap;

public class Leetcode737 {
    class UnionFind {
        HashMap<String, String> map = new HashMap<>();

        public UnionFind(String[][] pairs) {
            for(String[] p : pairs) {
                union(p);
            }
        }

        public void union(String[] pairs) {
            String source = pairs[0], target = pairs[1];
            if(!map.containsKey(source) && !map.containsKey(target)) {
                map.put(target, target);
                map.put(source, target);
            } else if (!map.containsKey(source)) {
                map.put(source, target);
            } else if (!map.containsKey(target)) {
                map.put(target, source);
            } else {
                String root1 = find(source);
                String root2 = find(target);
                if(!root1.equals(root2)) {
                    map.put(root1, root2);
                }
            }
        }

        public String find(String src) {
            if(!map.containsKey(src))
                return null;
            while(!map.get(src).equals(src)) {
                src = map.get(src);
            }
            return src;
        }
    }

    public boolean areSentencesSimilarTwo(String[] words1, String[] words2, String[][] pairs) {
        if(words1.length != words2.length)
            return false;
        UnionFind uf = new UnionFind(pairs);
        for(int i = 0; i < words1.length; i++) {
            String src = words1[i];
            String target = words2[i];
            if(src.equals(target))
                continue;
            String root1 = uf.find(src);
            String root2 = uf.find(target);
            if(root1 == null || root2 == null || !root1.equals(root2))
                return false;
        }
        return true;
    }
}

/*
这个题目是union find解法，不难想
 */