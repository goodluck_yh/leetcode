package henry.leetcode;

public class Leetcode985 {
    public int[] sumEvenAfterQueries(int[] A, int[][] queries) {
        int[] res = new int[queries.length];
        int sum = 0;
        for(int i = 0; i < A.length; i++) {
            if(A[i] % 2 == 0)
                sum = sum + A[i];
        }

        for(int i = 0; i < res.length; i++) {
            int val = queries[i][0], idx = queries[i][1];
            int old = A[idx], next = old + val;
            A[idx] = next;
            if(Math.abs(old % 2) == 1 && Math.abs(next % 2) == 1) {
                res[i] = sum;
            } else if(Math.abs(old % 2) == 1) {
                sum = sum + next;
                res[i] = sum;
            } else if(Math.abs(next % 2) == 1) {
                sum = sum - old;
                res[i] = sum;
            } else {
                sum = sum - old + next;
                res[i] = sum;
            }
        }
        return res;
    }

    public static void main(String[] args) {
        Leetcode985 leetocde = new Leetcode985();
        // [1,2,3,4]
        //[[1,0],[-3,1],[-4,0],[2,3]]
        int[] res = leetocde.sumEvenAfterQueries(new int[] {1,2,3,4}, new int[][] {{1,0}, {-3, 1}, {-4, 0}, {2, 3}});
        for(int i : res) {
            System.out.print(i + " ");
        }
    }
}
