package henry.leetcode;

/*
Input: N = 6, edges = [[0,1],[0,2],[2,3],[2,4],[2,5]]
Output: [8,12,6,10,10,10]
Explanation:
Here is a diagram of the given tree:
  0
 / \
1   2
   /|\
  3 4 5
We can see that dist(0,1) + dist(0,2) + dist(0,3) + dist(0,4) + dist(0,5)
equals 1 + 1 + 2 + 2 + 2 = 8.  Hence, answer[0] = 8, and so on.
 */

/*
1 initial two array: the sum distance of all children nodes, the number of children
2 use: left = left_only + right_only + right_num to calculate the result
 */

import java.util.*;

public class Leetcode834 {
    int[] cnt;
    int[] dist;
    int N;
    HashMap<Integer, Set<Integer>> edges = new HashMap<>();

    public int[] sumOfDistancesInTree(int N, int[][] edges) {
        cnt = new int[N];
        dist = new int[N];
        this.N = N;
        for(int i = 0; i < N; i++) {
            this.edges.put(i, new HashSet<>());
        }
        for(int[] edge: edges) {
            this.edges.get(edge[0]).add(edge[1]);
            this.edges.get(edge[1]).add(edge[0]);
        }
        prepare(0,-1);
        calc(0, -1);
        return dist;
    }

    private void calc(int node, int parent) {
        Set<Integer> neis = edges.get(node);
        for(int child: neis) {
            if(child != parent) {
                // left = left_only + right_only + right_num
                dist[child] = dist[node] - cnt[child] + N - cnt[child];
                calc(child, node);
            }
        }
    }

    private void prepare(int node, int parent) {
        Set<Integer> neis = edges.get(node);
        for(int child: neis) {
            if(child != parent) {
                prepare(child, node);
                cnt[node] += cnt[child];
                dist[node] += dist[child] + cnt[child];
            }
        }
        cnt[node]++;
    }
}

/*
不好想！
 */