package henry.leetcode;

/*
Let f(x) be the number of zeroes at the end of x!. (Recall that x! = 1 * 2 * 3 * ... * x, and by convention, 0! = 1.)
For example, f(3) = 0 because 3! = 6 has no zeroes at the end, while f(11) = 2 because 11! = 39916800 has 2 zeroes at the end. Given K, find how many non-negative integers x have the property that f(x) = K.

Example 1:
Input: K = 0
Output: 5
Explanation: 0!, 1!, 2!, 3!, and 4! end with K = 0 zeroes.
 */
public class Leetcode793 {
    public int preimageSizeFZF(int K) {
        int lo = 0, hi = K;
        while(lo < hi) {
            int mid = (lo + hi) / 2;
            int actual = count(mid);
            if(actual == K)
                return 5;
            if(actual < K) {
                lo = mid + 1;
            } else {
                hi = mid - 1;
            }
        }
        int actual = count(lo);
        return actual == K ? 5 : 0;
    }

    private int count(int k) {
        int res = k;
        while(k > 0) {
            k = k / 5;
            res += k;
        }
        return res;
    }
}

/*
这个题目一开始没有想出来，看了解答之后，给了我启示：
如果要计划一个数字，不能直接计算，而且我们又知道上下边界的时候，可以考虑二分查找。
 */