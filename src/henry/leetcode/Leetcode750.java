package henry.leetcode;

public class Leetcode750 {
    public int countCornerRectangles(int[][] grid) {
        int res = 0;
        for(int i = 0; i < grid.length; i++) {
            for(int j = i+1; j < grid.length; j++) {
                int same = 0;
                for(int k = 0; k < grid[0].length; k++) {
                    if(grid[i][k] == 1 && grid[j][k] == 1)
                        same++;
                }

                if(same >= 2) {
                    res = res + same * (same-1) / 2;
                }
            }
        }

        return res;
    }
}

/*
这个解法比较慢，是我自己想出来的，复杂度O(r*r*c)
网上有dp算法，在时间上更快，但是复杂度还是一样的

下面是非stack解法，是这个题目的最优解！
 */

class Leetcode750_2 {
    public int[] dailyTemperatures(int[] T) {
        int[] res = new int[T.length];
        res[res.length - 1] = 0;
        for(int i = res.length-2; i >= 0; i--) {
            int j = i + 1;
            while(j < res.length && res[j] != 0 && T[i] >= T[j]) {
                j += res[j];
            }
            if(T[i] < T[j]) {
                res[i] = j - i;
            } else {
                res[i] = 0;
            }
        }
        return res;
    }
}