package henry.leetcode;

import java.util.*;

/*
In an election, the i-th vote was cast for persons[i] at time times[i].
Now, we would like to implement the following query function: TopVotedCandidate.q(int t) will return the number of the person that was leading the election at time t.
Votes cast at time t will count towards our query.  In the case of a tie, the most recent vote (among tied candidates) wins.

Example 1:
Input: ["TopVotedCandidate","q","q","q","q","q","q"], [[[0,1,1,0,0,1,0],[0,5,10,15,20,25,30]],[3],[12],[25],[15],[24],[8]]
Output: [null,0,1,1,0,0,1]
Explanation:
At time 3, the votes are [0], and 0 is leading.
At time 12, the votes are [0,1,1], and 1 is leading.
At time 25, the votes are [0,1,1,0,0,1], and 1 is leading (as ties go to the most recent vote.)
This continues for 3 more queries at time 15, 24, and 8.


Note:

1 <= persons.length = times.length <= 5000
0 <= persons[i] <= persons.length
times is a strictly increasing array with all elements in [0, 10^9].
TopVotedCandidate.q is called at most 10000 times per test case.
TopVotedCandidate.q(int t) is always called with t >= times[0].
 */

public class Leetcode911 {
    private int[] times;
    // time -> person
    private HashMap<Integer, Integer> winner = new HashMap<>();
    private HashMap<Integer, Integer> count = new HashMap<>();

    public Leetcode911(int[] persons, int[] times) {
        this.times = times;
        int max = 0, p = -1;;
        for(int i = 0; i < times.length; i++) {
            int cur = count.getOrDefault(persons[i], 0) + 1;
            count.put(persons[i] ,cur);
            if(cur >= max) {
                p = persons[i];
                max = cur;
            }
            winner.put(times[i], p);
        }
    }

    public int q(int t) {
        return winner.get(find(t));
    }

    private int find(int t) {
        int begin = 0, end = times.length-1, mid = (begin + end) / 2;
        while(begin + 1 < end) {
            mid = (begin + end) / 2;
            if(times[mid] == t)    return times[mid];
            if(times[mid] > t) {
                end = mid - 1;
            } else {
                begin = mid;
            }
        }

        return times[end]> t ? times[begin] : times[end];
    }
}


/*
思路：
1. 首先想到了segment tree, 后来发现不对，线段树是某一区间的和，而此题是某一区间的最大值
2. 其次想到了TreeMap, 发现题目中如果投票数相等，返回最近投票的人这个条件很难满足
3. 看了答案想到了最简单的方法，就是一个列表记录所有的投票数，并记录下最大值，每次有一个新的投票，就跟最大值比较！
 */