package henry.leetcode;

import java.util.*;

public class Leetcode894 {
    class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }

    public List<TreeNode> allPossibleFBT(int N) {
        if(N % 2 == 0)
            return new ArrayList<TreeNode>();
        return helper(N);
    }

    private List<TreeNode> helper(int n) {
        List<TreeNode> res = new LinkedList<>();
        if(n == 1) {
            TreeNode node = new TreeNode(0);
            res.add(node);
            return res;
        }
        for(int i = 1; i < n - 1; i = i + 2) {
            int left = i, right = n - 1 - i;
            List<TreeNode> l = helper(left), r = helper(right);
            for(TreeNode ln: l) {
                for(TreeNode rn: r) {
                    TreeNode node = new TreeNode(0);
                    node.left = ln;
                    node.right = rn;
                    res.add(node);
                }
            }
        }
        return res;
    }
}

/*
簡單
beat 81.47%
複雜度：O(2^N)
 */