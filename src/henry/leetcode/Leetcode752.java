package henry.leetcode;

import java.util.*;

/*
You are given a list of deadends dead ends, meaning if the lock displays any of these codes, the wheels of the lock will stop turning and you will be unable to open it.
Given a target representing the value of the wheels that will unlock the lock, return the minimum total number of turns required to open the lock, or -1 if it is impossible.

Example 1:
Input: deadends = ["0201","0101","0102","1212","2002"], target = "0202"
Output: 6
Explanation:
A sequence of valid moves would be "0000" -> "1000" -> "1100" -> "1200" -> "1201" -> "1202" -> "0202".
Note that a sequence like "0000" -> "0001" -> "0002" -> "0102" -> "0202" would be invalid,
because the wheels of the lock become stuck after the display becomes the dead end "0102".

Note:
The length of deadends will be in the range [1, 500].
target will not be in the list deadends.
Every string in deadends and the string target will be a string of 4 digits from the 10,000 possibilities '0000' to '9999'.
 */

public class Leetcode752 {
    public int openLock(String[] deadends, String target) {
        Set<String> visited = new HashSet<>(Arrays.asList(deadends));
        return bfs(visited, target);
    }

    private int bfs(Set<String> visited, String target) {
        List<String> elements = new LinkedList<>();
        int res = 0;
        if(visited.contains("0000"))    return -1;
        elements.add("0000");
        visited.add("0000");
        while(elements.size() != 0){
            List<String> newElements = new LinkedList<>();
            for(String temp: elements) {
                if(temp.equals(target)) return res;
                List<String> nei = getNei(visited, new StringBuffer(temp));
                for(String s: nei) {
                    newElements.add(s);
                    visited.add(s);
                }
            }
            elements = newElements;
            res++;
        }
        return -1;
    }

    private List<String> getNei(Set<String> visited, StringBuffer str) {
        List<String> res = new LinkedList<>();
        for(int i = 0; i < 4; i++) {
            char ch;
            char origin = str.charAt(i);
            ch = (char)(origin - 1);
            if(ch == (char)('0' - 1))   ch = '9';
            str.setCharAt(i, ch);
            String s = str.toString();
            if(!visited.contains(s))
                res.add(s);
            ch = (char)(origin + 1);
            if(ch == (char)('9' + 1))    ch = '0';
            str.setCharAt(i, ch);
            s = str.toString();
            if(!visited.contains(s))
                res.add(s);
            str.setCharAt(i, origin);
        }
        return res;
    }

    public static void main(String[] args) {
        Leetcode752 leetcode = new Leetcode752();
        System.out.println(leetcode.openLock(new String[] {"0201","0101","0102","1212","2002"}, "0009"));
    }
}

/*
看了求最小值，可以想到DP，或者BFS，这个题目一看就是BFS
 */