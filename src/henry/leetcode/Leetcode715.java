package henry.leetcode;

import java.util.*;
/*
A Range Module is a module that tracks ranges of numbers. Your task is to design and implement the following interfaces in an efficient manner.
addRange(int left, int right) Adds the half-open interval [left, right), tracking every real number in that interval.
Adding an interval that partially overlaps with currently tracked numbers should add any numbers in the interval [left, right) that are not already tracked.
queryRange(int left, int right) Returns true if and only if every real number in the interval [left, right) is currently being tracked.
removeRange(int left, int right) Stops tracking every real number currently being tracked in the interval [left, right).
Example 1:
addRange(10, 20): null
removeRange(14, 16): null
queryRange(10, 14): true (Every number in [10, 14) is being tracked)
queryRange(13, 15): false (Numbers like 14, 14.03, 14.17 in [13, 15) are not being tracked)
queryRange(16, 17): true (The number 16 in [16, 17) is still being tracked, despite the remove operation)
 */
public class Leetcode715 {
}

class RangeModule {

    TreeMap<Integer, Integer> treeMap = new TreeMap<>();

    public RangeModule() {

    }

    public void addRange(int left, int right) {
        Integer start = treeMap.floorKey(left), end = treeMap.floorKey(right);
        if(start == null && end == null) {
            treeMap.put(left, right);
        } else if(start == null) {
            treeMap.put(left, Math.max(right, treeMap.get(end)));
        } else{
            if(left <= treeMap.get(start)){
                treeMap.put(start, Math.max(right, treeMap.get(end)));
            }
            else
                treeMap.put(left, Math.max(right, treeMap.get(end)));
        }

        // cleanup
        treeMap.subMap(left, false, right, true).clear();
    }

    public boolean queryRange(int left, int right) {
        Integer start = treeMap.floorKey(left);
        if(start == null)   return false;
        if(treeMap.get(start) >= right)
            return true;
        return false;
    }

    public void removeRange(int left, int right) {
        Integer start = treeMap.floorKey(left), end = treeMap.floorKey(right);
        if(start == null && end == null)    return;
        if(start == null) {
            int end_right = treeMap.get(end);
            treeMap.subMap(left, false, right, true).clear();
            if(right < end_right)    treeMap.put(right, end_right);
        } else {
            int end_right = treeMap.get(end), end_left = treeMap.get(start);
            treeMap.subMap(left, true, right, true).clear();
            if(right < end_right)    treeMap.put(right, end_right);
            if(start != left)   treeMap.put(start, Math.min(left, end_left));
        }
    }

    public static void main(String[] args) {
        RangeModule leetcode = new RangeModule();
        leetcode.addRange(1, 4);
        leetcode.removeRange(1, 40);

        System.out.println(leetcode.queryRange(2, 9));
        System.out.println(leetcode.queryRange(180, 300));
    }
}

/*
beat 78.70%
这个题目不好写，很多bug
通过这个题目有两个需要学习的地方：
1 treemap如果删除部分，如何找到上下界限
2 interval的treemap解法
 */