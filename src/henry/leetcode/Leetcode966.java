package henry.leetcode;

import java.util.*;

public class Leetcode966 {
    public String[] spellchecker(String[] wordlist, String[] queries) {
        Set<String> words = new HashSet<>();
        HashMap<String, Integer> ignoreCase = new HashMap<>();
        HashMap<String, HashMap<String, Integer>> vowel = new HashMap<>();
        for(int i = 0; i < wordlist.length; i++){
            String w = wordlist[i];
            words.add(w);
            String w_l = w.toLowerCase();
            if(!ignoreCase.containsKey(w_l)) {
                ignoreCase.put(w_l, i);
            }
            char[] chs = w_l.toCharArray();
            StringBuffer sb = new StringBuffer();
            List<Integer> idxs = new LinkedList<>();
            for(int j = 0; j < chs.length; j++) {
                if(isVowel(chs[j])) {
                    idxs.add(j);
                } else {
                    sb.append(chs[j]);
                }
            }
            String extra = sb.toString();
            if(!vowel.containsKey(extra)) {
                vowel.put(extra, new HashMap<>());
            }
            String key2 = chs.length + "";
            for(int idx: idxs) {
                key2 = key2 + "#" + idx;
            }
            if(!vowel.get(extra).containsKey(key2)) {
                vowel.get(extra).put(key2, i);
            }
        }
        //System.out.println(vowel.get(""));

        String[] res = new String[queries.length];
        for(int i = 0; i < queries.length; i++) {
            String q = queries[i];

            if(words.contains(q)) {
                res[i] = q;
            } else if(ignoreCase.containsKey(q.toLowerCase())) {
                res[i] = wordlist[ignoreCase.get(q.toLowerCase())];
            } else {
                StringBuffer sb = new StringBuffer();
                List<Integer> idxs = new LinkedList<>();
                char[] chs = q.toLowerCase().toCharArray();
                for(int j = 0; j < chs.length; j++) {
                    if(isVowel(chs[j])) {
                        idxs.add(j);
                    } else {
                        sb.append(chs[j]);
                    }
                }
                String extra = sb.toString();
                if(!vowel.containsKey(extra)) {
                    vowel.put(extra, new HashMap<>());
                }
                String key2 = chs.length + "";
                for(int idx: idxs) {
                    key2 = key2 + "#" + idx;
                }
                //System.out.println(extra + "  " + key2);
                if(vowel.containsKey(extra) && vowel.get(extra).containsKey(key2)) {
                    res[i] = wordlist[vowel.get(extra).get(key2)];
                } else{
                    res[i] = "";
                }
            }
        }
        return res;
    }

    private boolean isVowel(char ch) {
        return ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u';
    }
}
