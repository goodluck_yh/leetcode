package henry.leetcode;

import java.util.HashMap;

public class Leetcode659 {
    public boolean isPossible(int[] nums) {
        HashMap<Integer, Integer> map1 = new HashMap<>();
        HashMap<Integer, Integer> map2 = new HashMap<>();
        for(int i: nums) {
            map1.put(i, map1.getOrDefault(i, 0) + 1);
        }

        for(int i: nums) {
            if(map1.get(i) == 0)
                continue;
            if(map2.getOrDefault(i, 0) > 0) {
                map2.put(i, map2.get(i) - 1);
                map2.put(i+1, map2.getOrDefault(i+1, 0) + 1);
            } else if(map1.getOrDefault(i+1, 0) > 0 && map1.getOrDefault(i+2, 0) > 0) {
                map1.put(i+1, map1.get(i+1) - 1);
                map1.put(i+2, map1.get(i+2) - 1);
                map2.put(i+3, map2.getOrDefault(i+3, 0) + 1);
            } else
                return false;
            map1.put(i, map1.get(i) - 1);
        }
        return true;
    }
}

/*
这个题目大意是说：有一个排序数组，返回能不能将它分成若干个子数组（可以为1），要求，每个子数组长度至少为三，且元素连续
例如[1,2,3,3,4,5] => [1,2,3], [3,4,5] => true

这个题目是一般解法，思路是
- 创建两个map，一个map统计某一个元素出现的频率，另一个map统计可以放i元素的个数
- 遍历数组，创建map1
- 遍历数组，获得元素i：
    - 如果i对应的频率已经为0， 则continue （会预支）
    - 否则，查找i+1, i+2 频率，如果有一个为0，则返回false
    - 否则，减少i+1, i+2 的频率（-1）， 并且，将需要i+3的个数加一
    - 最后，无论哪种情况，i的频率减少1
 */

class Leetcode659_1 {
    public boolean isPossible(int[] nums) {
        int pre = Integer.MIN_VALUE, p1 = 0, p2 = 0, p3 = 0;
        int cur = 0, cnt = 0, c1 = 0, c2 = 0, c3 = 0;
        for(int i = 0; i < nums.length; pre = cur, p1 = c1, p2 = c2, p3 = c3) {
            for(cur = nums[i], cnt = 0; i < nums.length && cur == nums[i]; cnt++, i ++)
                ;
            if(cur != pre + 1) {
                if(p1 != 0 || p2 != 0)
                    return false;
                c1 = cnt;
                c2 = 0;
                c3 = 0;
            } else {
                if(cnt < p1 + p2)   return false;;
                c1 = Math.max(0, cnt - (p1 + p2 + p3));
                c2 = p1;
                c3 = p2 + Math.min(p3, cnt - p1 - p2);
                //c1 = cnt - p1 - p2;
            }

        }
        return p1 == 0 && p2 == 0;
    }
}

/*
上面的做法是discussion里面的做法，复杂度一样，速度快很多
主要的tips：
- 大循环负责一个一个循环，小循环负责找个数，这样逻辑清晰很多
- 如何计算出c1
 */