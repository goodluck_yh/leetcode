package henry.leetcode;

import java.util.*;

public class Leetcode721 {
    private class UnionFind {
        int[] array;
        int[] weight;

        public UnionFind(int len) {
            array = new int[len];
            weight = new int[len];
            for(int i = 0; i < array.length; i++) {
                array[i] = i;
                weight[i] = 1;
            }
        }

        public void union(int a, int b) {
            int ia = find(a);
            int ib = find(b);
            if(ia != ib) {
                if(weight[ia] > weight[ib]) {
                    weight[ia] += weight[ib];
                    array[ib] = array[ia];
                } else {
                    weight[ib] += weight[ia];
                    array[ia] = array[ib];
                }
            }
        }

        public int find(int a) {
            while(array[a] != a) {
                array[a] = array[array[a]];
                a = array[a];
            }
            return a;
        }
    }

    public List<List<String>> accountsMerge(List<List<String>> accounts) {
        HashMap<String, Integer> emailToId = new HashMap<>();
        UnionFind uf = new UnionFind(accounts.size());
        for(int i = 0; i < accounts.size(); i++) {
            List<String> account = accounts.get(i);
            for(int j = 1; j < account.size(); j++) {
                if(emailToId.containsKey(account.get(j))) {
                    uf.union(i, emailToId.get(account.get(j)));
                } else{
                    emailToId.put(account.get(j), i);
                }
            }
        }

        HashMap<Integer, Set<String>> map = new HashMap<>();
        for(int i = 0; i < accounts.size(); i++) {
            int root = uf.find(i);
            if(!map.containsKey(root)){
                map.put(root, new TreeSet<>());
                map.get(root).add(accounts.get(root).get(0));
            }
            for(int j = 1; j < accounts.get(i).size(); j++){
                map.get(root).add(accounts.get(i).get(j));
            }

        }

        List<List<String>> res = new ArrayList<>();
        for(int i : map.keySet()) {
            List<String> temp = new ArrayList<>();
            for(String each: map.get(i)) {
                temp.add(each);
            }
            res.add(temp);
        }
        return res;
    }
}


/*
这个题目union-find一下子就想到了，但是还是一直超时，主要是判断两个点是否相连的方法不对
 */