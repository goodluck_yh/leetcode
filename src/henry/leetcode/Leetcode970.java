package henry.leetcode;

import java.util.*;

public class Leetcode970 {
    public List<Integer> powerfulIntegers(int x, int y, int bound) {
        Set<Long> set = new HashSet<>();
        for(int i = 0; Math.pow(x, i) <= bound; i++) {
            for(int j = 0; Math.pow(y, j) <= bound; j++) {
                long val = (long)Math.pow(x, i) + (long)Math.pow(y, j);
                if(val <= bound)
                    set.add(val);
                else
                    break;
                if(y == 1)
                    break;
            }
            if(x == 1)
                break;
        }
        List<Integer> res = new LinkedList<>();
        for(long l: set) {
            res.add((int)l);
        }
        return res;
    }
}

/*
特例是1的情况
 */