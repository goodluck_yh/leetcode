package henry.leetcode;

import java.util.*;

/*
Given an integer array, return the k-th smallest distance among all the pairs. The distance of a pair (A, B) is defined as the absolute difference between A and B.

Example 1:
Input:
nums = [1,3,1]
k = 1
Output: 0
Explanation:
Here are all the pairs:
(1,3) -> 2
(1,1) -> 0
(3,1) -> 2
Then the 1st smallest distance pair is (1,1), and its distance is 0.
 */

public class Leetcode719 {
    class Solution {
        public int smallestDistancePair(int[] nums, int k) {
            Arrays.sort(nums);
            int lo = 0, hi = nums[nums.length-1] - nums[0];
            while(lo < hi) {
                int mid = (lo + hi) / 2, cnt = 0, pos = -1;
                for(int i = 0; i < nums.length; i++) {
                    int c = count(nums, k, i, mid);
                    cnt += c;
                    pos = Math.max(nums[c+i] - nums[i], pos);
                }
                if(cnt == k)
                    return pos;
                if(cnt > k) {
                    hi = mid;
                } else {
                    lo = mid + 1;
                }
            }
            return lo;
        }

        private int count(int[] nums, int k, int start, int target) {
            int lo = start+1, hi = nums.length-1;
            while(lo + 1 < hi) {
                int mid = (lo + hi) / 2;
                if(nums[mid] - nums[start] > target) {
                    hi = mid - 1;
                } else {
                    lo = mid;
                }
            }
            if(nums[hi] - nums[start] <= target)
                return hi - start;
            if(nums[lo] - nums[start] <= target)
                return lo - start;
            return 0;
        }
    }
}

/*
经典binary search 题目
 */