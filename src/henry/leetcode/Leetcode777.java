package henry.leetcode;

/*
In a string composed of 'L', 'R', and 'X' characters, like "RXXLRXRXL", a move consists of either replacing one occurrence of "XL" with "LX", or replacing one occurrence of "RX" with "XR". Given the starting string start and the ending string end, return True if and only if there exists a sequence of moves to transform one string to the other.
Example:
Input: start = "RXXLRXRXL", end = "XRLXXRRLX"
Output: True
Explanation:
We can transform start to end following these steps:
RXXLRXRXL ->
XRXLRXRXL ->
XRLXRXRXL ->
XRLXXRRXL ->
XRLXXRRLX

Note:
1 <= len(start) = len(end) <= 10000.
Both start and end will only consist of characters in {'L', 'R', 'X'}.
 */

public class Leetcode777 {
    public boolean canTransform(String start, String end) {
        char[] chs1 = start.toCharArray();
        char[] chs2 = end.toCharArray();
        int i = 0, j = 0;
        while(i < chs1.length && j < chs2.length) {
            // find first not X in start
            while(i < chs1.length && chs1[i] == 'X')    i++;
            //find fisrt not X in end
            while(j < chs2.length && chs2[j] == 'X')    j++;
            if(i == chs1.length && j == chs2.length)    return true;
            if(i == chs1.length || j == chs2.length)    return false;
            if(chs1[i] != chs2[j])  return false;
            if(i < j && chs1[i] == 'L') return false;
            if(i > j && chs1[i] == 'R') return false;
            i++;
            j++;
        }
        return true;
    }
}

/*
这个题目是智力测试题，没想到怎么做
解题思路：
（1） LX在两个数组中，相同
（2） 对应L在两个数组的下标中，start必须小于等于
（3） 对应R在两个数组的下标中，start必须大于等于
 */