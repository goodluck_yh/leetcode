package henry.leetcode;

import java.util.*;

public class Leetcode731 {
    TreeMap<Integer, Integer> map;
    public Leetcode731() {
        map = new TreeMap<>();
    }

    public boolean book(int start, int end) {
        map.put(start, map.getOrDefault(start, 0) + 1);
        map.put(end, map.getOrDefault(end, 0) - 1);
        int booked = 0;
        for(Map.Entry<Integer, Integer> entry: map.entrySet()) {
            booked = booked + entry.getValue();
            if(booked >= 3) {
                map.put(start, map.get(start) - 1);
                map.put(end, map.get(end) + 1);
                return false;
            }
        }
        return true;
    }
}

/*
上面的解法是imos解法，是这类问题的通用解法
复杂度: O(n)
 */