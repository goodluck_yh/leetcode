package henry.leetcode;

import java.util.HashSet;
import java.util.Set;

public class Leetcode817 {
    public class ListNode {
        int val;
        ListNode next;

        ListNode(int x) {
            val = x;
        }
    }

    public int numComponents(ListNode head, int[] G) {
        Set<Integer> set = new HashSet<>();
        for (int i : G)
            set.add(i);
        int res = 0;
        while (head != null && !set.contains(head.val)) head = head.next;
        boolean missing = true;
        while (head != null) {
            if (set.contains(head.val)) {
                if (missing) {
                    res++;
                }
                missing = false;
            } else {
                missing = true;
            }
            head = head.next;
        }
        return res;
    }
}

/*
思路不难，注意语法：
set.addAll 不能加array
new HashSet<>() 参数不能使array
 */