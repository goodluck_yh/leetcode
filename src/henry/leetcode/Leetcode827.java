package henry.leetcode;

import java.util.*;

public class Leetcode827 {
    class UnionFind{
        int[] parent;
        int[] size;
        UnionFind(int n) {
            parent = new int[n];
            size = new int[n];
            for(int i = 0; i < n; i++) {
                parent[i] = i;
                size[i] = 1;
            }
        }

        void union(int a, int b) {
            int pa = find(a), pb = find(b);
            if(pa != pb) {
                parent[pa] = pb;
                size[pb] += size[pa];
            }
        }

        int find(int a) {
            while(parent[a] != a)
                a = parent[a];
            return a;
        }
    }

    int[] dx = {0, 1, 0, -1};
    int[] dy = {1, 0, -1, 0};
    public int largestIsland(int[][] grid) {
        int M = grid.length, N = grid[0].length;
        UnionFind uf = new UnionFind(M * N);
        for(int i = 0; i < M; i++) {
            for(int j = 0; j < N; j++) {
                if(grid[i][j] == 1) {
                    for(int k = 0; k < 4; k++) {
                        int i1 = i + dx[k], j1 = j + dy[k];
                        if(i1 >= 0 && i1 < M && j1 >= 0 && j1 < N && grid[i1][j1] == 1) {
                            uf.union(i*M+j, i1*M+j1);
                        }
                    }
                }
            }
        }
        int res = uf.size[uf.find(0)];
        for(int i = 0; i < M; i++) {
            for(int j = 0; j < N; j++) {
                if(grid[i][j] == 0) {
                    Set<Integer> set = new HashSet<>();
                    int temp = 1;
                    for(int k = 0; k < 4; k++) {
                        int i1 = i + dx[k], j1 = j + dy[k];
                        if(i1 >= 0 && i1 < M && j1 >= 0 && j1 < N && grid[i1][j1] == 1 && !set.contains(uf.find(i1*M+j1))) {
                            set.add(uf.find(i1*M+j1));
                            temp += uf.size[uf.find(i1*M+j1)];
                        }
                    }
                    res = Math.max(res, temp);
                }
            }
        }
        return res;
    }
    public static void main(String[] args) {
        Leetcode827 leetcode = new Leetcode827();
        System.out.println(leetcode.largestIsland(new int[][] {{1,1},{1,0}}));
    }
}
