package henry.leetcode;

/*
Let's say a positive integer is a superpalindrome if it is a palindrome, and it is also the square of a palindrome.
Now, given two positive integers L and R (represented as strings), return the number of superpalindromes in the inclusive range [L, R].

Example 1:
Input: L = "4", R = "1000"
Output: 4
Explanation: 4, 9, 121, and 484 are superpalindromes.
Note that 676 is not a superpalindrome: 26 * 26 = 676, but 26 is not a palindrome.

Note:
1 <= len(L) <= 18
1 <= len(R) <= 18
L and R are strings representing integers in the range [1, 10^18).
int(L) <= int(R)
 */

import java.util.*;

public class Leetcode906 {
    public int superpalindromesInRange(String L, String R) {
        // from begin and end
        long l = Long.parseLong(L), r = Long.parseLong(R);
        int begin = (int)Math.ceil(Math.sqrt(l));
        int end = (int)Math.sqrt(r);
        int res  = 0;
        // construct the candidate
        int min = (begin + "").length(), max = (end + "").length();
        for(int len = min; len <= max; len++) {
            List<Long> all = new LinkedList<>();
            getAllCandidate(len, all, new StringBuilder());
            for(long candidate: all) {
                if(candidate >= begin && candidate <= end) {
                    long temp = candidate * candidate;
                    if(isPalin(temp))
                        res++;
                }
            }
        }
        return res;
    }

    private boolean isPalin(long num) {
        String temp = num + "";
        for(int i = 0, j = temp.length() - 1; i < j; ) {
            if(temp.charAt(i) != temp.charAt(j))
                return false;
            i++;
            j--;
        }
        return true;
    }

    private void getAllCandidate(int len, List<Long> all, StringBuilder sb) {
        if(sb.length() == len) {
            all.add(Long.parseLong(sb.toString()));
            return;
        }
        if(sb.length() == 0) {
            if(len % 2 == 1) {
                for(int i = 0; i <= 9; i++) {
                    sb.append(i);
                    getAllCandidate(len, all, sb);
                    sb.deleteCharAt(0);
                }
            } else {
                for(int i = 0; i <= 9; i++) {
                    sb.append(i);
                    sb.append(i);
                    getAllCandidate(len, all, sb);
                    sb.deleteCharAt(0);
                    sb.deleteCharAt(0);
                }
            }
        } else {
            int min = 0;
            if(sb.length() + 2 == len) {
                min = 1;
            }
            for(int i = min; i <= 9; i++) {
                sb.insert(0, i);
                sb.append(i);
                getAllCandidate(len, all, sb);
                sb.deleteCharAt(0);
                sb.deleteCharAt(sb.length() - 1);
            }
        }
    }
}

/*
beat 10.78%
这个题目面的时候应该过了
 */