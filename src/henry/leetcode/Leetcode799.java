package henry.leetcode;

public class Leetcode799 {
    public double champagneTower(int poured, int query_row, int query_glass) {
        if (poured == 0) return 0.0;
        double[] cur = null;
        int i = 0;
        for (i = 0; i <= query_row; i++) {
            int total = i + 1;
            if (i == 0) {
                cur = new double[]{poured};
                if (poured == 1) {
                    i++;
                    break;
                }
                continue;
            }
            double[] pre = cur;
            boolean hasNext = false;
            cur = new double[total];
            for (int j = 0; j < total; j++) {
                if (j == 0) {
                    if (pre[j] - 1 > 0)
                        cur[j] = (pre[j] - 1) / 2;
                } else if (j == total - 1) {
                    if (pre[j - 1] - 1 > 0)
                        cur[j] = (pre[j - 1] - 1) / 2;
                } else {
                    double sum = 0;
                    if (pre[j - 1] - 1 > 0)
                        sum = sum + pre[j - 1] - 1;
                    if (pre[j] - 1 > 0)
                        sum = sum + pre[j] - 1;
                    cur[j] = sum / 2;
                }
                if (cur[j] > 0) hasNext = true;
            }
            if (!hasNext)
                break;
        }

        if (i > query_row) {
            return Math.min(1, cur[query_glass]);
        }
        return 0.0;
    }
}

/*
这个废了半天力气，最后最初来的，但是解法肯定不是最优解
答案这个解法，意思一样，但是简洁了很多
 */

class Leetcode799_2 {

    public double champagneTower(int poured, int query_row, int query_glass) {
        double[][] A = new double[102][102];
        A[0][0] = (double) poured;
        for (int r = 0; r <= query_row; ++r) {
            for (int c = 0; c <= r; ++c) {
                double q = (A[r][c] - 1.0) / 2.0;
                if (q > 0) {
                    A[r+1][c] += q;
                    A[r+1][c+1] += q;
                }
            }
        }

        return Math.min(1, A[query_row][query_glass]);
    }

}