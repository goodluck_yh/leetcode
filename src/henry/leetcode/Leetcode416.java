package henry.leetcode;

import java.util.*;
/*
Given a non-empty array containing only positive integers, find if the array can be partitioned into two subsets such that the sum of elements in both subsets is equal.

Note:
Each of the array element will not exceed 100.
The array size will not exceed 200.
Example 1:
Input: [1, 5, 11, 5]
Output: true
Explanation: The array can be partitioned as [1, 5, 5] and [11].
 */

public class Leetcode416 {
    HashMap<Integer, HashMap<Integer, Boolean>> map = new HashMap<>();
    public boolean canPartition(int[] nums) {
        if(nums.length == 0)    return true;
        if(nums.length == 1)    return false;
        int total = 0;
        for(int i : nums)   total += i;
        if(total % 2 == 1)  return false;
        int target = total / 2;
        Arrays.sort(nums);
        return helper(nums, 0, target, 0);
    }

    private boolean helper(int[] nums, int cur, int target, int start) {
        if(cur > target)    return false;
        if(target == cur)   return true;
        if(start >= nums.length)    return false;
        if(cur + nums[start] > target) return false;
        if(map.containsKey(cur) && map.get(cur).containsKey(start))
            return map.get(cur).get(start);
        int i = start;
        for(; i < nums.length; i++) {
            if(nums[i] != nums[start])  break;
        }
        boolean flag = helper(nums, cur, target, i);
        if(flag) {
            setMap(cur, start, true);
            return true;
        }

        for(int j = 0; j < i - start; j++) {
            cur += nums[start];
            flag = helper(nums, cur, target, i);
            if(flag){
                setMap(cur, start, true);
                return true;
            }
        }
        setMap(cur, start, false);
        return false;
    }

    private void setMap(int cur, int start, boolean value) {
        HashMap<Integer, Boolean> map1 = map.getOrDefault(cur, new HashMap<>());
        map1.put(start, value);
        map.put(cur, map1);
    }
}

/*
仅仅beat 1%， 需要研究一下为什么？
 */

class Leetcode416_2 {
    public boolean canPartition(int[] nums) {
        if(nums == null || nums.length == 0)    return true;
        if(nums.length == 1)    return false;
        int total = 0;
        for(int i : nums)   total += i;
        if(total % 2 == 1)  return false;
        int average = total / 2;
        boolean[] dp = new boolean[average+1];
        dp[0] = true;
        // for(int i = 1; i < dp.length; i++) {
        //     for(int j = 0; j < nums.length; j++) {
        //         int pre = i - nums[j];
        //         if(pre >= 0) {
        //             dp[i] = dp[i] || dp[pre];
        //         }
        //     }
        // }
        for (int i = 1; i <= nums.length; i++) {
            for (int j = average; j >= nums[i-1]; j--) {
                dp[j] = dp[j] || dp[j - nums[i-1]];
            }
        }
        for(boolean b: dp) {
            System.out.print(b + " ");
        }
        return dp[average];
    }
}

/*
上面这个解法好好体会！为什么先要遍历num，而不是dp，需要仔细体会体会
 */