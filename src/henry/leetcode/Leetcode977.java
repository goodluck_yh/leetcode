package henry.leetcode;

/*
Given an array of integers A sorted in non-decreasing order, return an array of the squares of each number, also in sorted non-decreasing order.

Example 1:
Input: [-4,-1,0,3,10]
Output: [0,1,9,16,100]
 */

public class Leetcode977 {
    public int[] sortedSquares(int[] A) {
        // find positive
        int pos = 0;
        while(pos < A.length && A[pos] < 0)   pos++;
        // two points
        int[] res = new int[A.length];
        int neg = pos - 1, idx = 0;
        while(neg >= 0 && pos < A.length) {
            if(A[pos] <= -A[neg]) {
                res[idx++] = A[pos] * A[pos];
                pos++;
            } else {
                res[idx++] = A[neg] * A[neg];
                neg--;
            }
        }

        // remaining
        while(neg >= 0) {
            res[idx++] = A[neg] * A[neg];
            neg--;
        }
        while(pos < A.length) {
            res[idx++] = A[pos] * A[pos];
            pos++;
        }
        return res;
    }
}


