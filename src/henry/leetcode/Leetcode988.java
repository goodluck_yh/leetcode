package henry.leetcode;

/*
Given the root of a binary tree, each node has a value from 0 to 25 representing the letters 'a' to 'z': a value of 0 represents 'a', a value of 1 represents 'b', and so on.
Find the lexicographically smallest string that starts at a leaf of this tree and ends at the root.
(As a reminder, any shorter prefix of a string is lexicographically smaller: for example, "ab" is lexicographically smaller than "aba".  A leaf of a node is a node that has no children.)

https://leetcode.com/problems/smallest-string-starting-from-leaf/
 */
public class Leetcode988 {
    class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }

    String res = null;
    public String smallestFromLeaf(TreeNode root) {
        helper(new StringBuffer(), root);
        return res;
    }

    private void helper(StringBuffer sb, TreeNode node) {
        if(node.left == null && node.right == null) {
            String s = (char)(node.val + 'a') + sb.toString();
            res = compare(res, s);
            return;
        }

        sb.insert(0, (char)(node.val + 'a'));
        if(node.left == null) {
            helper(sb, node.right);
        } else if(node.right == null) {
            helper(sb, node.left);
        } else {
            helper(sb, node.left);
            helper(sb, node.right);
        }
        sb.deleteCharAt(0);

    }

    private String compare(String a, String b) {
        if(a == null)
            return b;
        for(int i = 0; i < a.length() && i < b.length(); i++) {
            if(a.charAt(i) < b.charAt(i)) {
                return a;
            } else if(a.charAt(i) > b.charAt(i)) {
                return b;
            }
        }
        return a.length() <= b.length() ? a : b;
    }
}
