package henry.leetcode;

public class Leetcode640 {
    public String solveEquation(String equation) {
        String[] parts = equation.split("=");

        char[] left = parts[0].toCharArray();
        char[] right = parts[1].toCharArray();
        int[] l = getCoefficient(left);
        int[] r = getCoefficient(right);
        int coefficient = l[0] - r[0];
        int total = r[1] - l[1];
        if(coefficient == 0) {
            if(total == 0)
                return "Infinite solutions";
            else
                return "No solution";
        } else {
            int res = total / coefficient;
            return "x=" + res;
        }
    }

    private int[] getCoefficient (char[] part) {
        int coefficient = 0, total = 0;
        for(int i = 0; i < part.length;) {
            StringBuffer sb = new StringBuffer();
            boolean isPos = true;
            if(part[i] == '-') {
                isPos = false;
                i++;
            } else if(part[i] == '+') {
                i++;
            }
            while(i < part.length && part[i] != 'x' && part[i] != '+' && part[i] != '-'){
                sb.append(part[i]);
                i++;
            }
            int temp = sb.length() != 0 ? Integer.parseInt(sb.toString()) : 1;
            if(i < part.length && part[i] == 'x') {
                i++;
                if(isPos) {
                    coefficient += temp;
                } else {
                    coefficient -= temp;
                }
            } else {
                if(isPos) {
                    total += temp;
                } else {
                    total -= temp;
                }
            }
        }
        return new int[] {coefficient, total};
    }
}

/*
Solve the Equation
题目有没有说清楚： 解必是整数

题目不难，但是有几个越界bug
（1）养成检查的习惯
（2）for while if 操作数组一定要小心
（3） Integer.parseInt 一定要判断空
 */