package henry.leetcode;

/*
A subarray A[i], A[i+1], ..., A[j] of A is said to be turbulent if and only if:
For i <= k < j, A[k] > A[k+1] when k is odd, and A[k] < A[k+1] when k is even;
OR, for i <= k < j, A[k] > A[k+1] when k is even, and A[k] < A[k+1] when k is odd.
That is, the subarray is turbulent if the comparison sign flips between each adjacent pair of elements in the subarray.
Return the length of a maximum size turbulent subarray of A.

Example 1:
Input: [9,4,2,10,7,8,8,1,9]
Output: 5
Explanation: (A[1] > A[2] < A[3] > A[4] < A[5])
 */
public class Leetcode978 {
    public int maxTurbulenceSize(int[] A) {
        int[] min = new int[A.length];
        int[] max = new int[A.length];
        int res = 1;
        max[0] = 1;
        min[0] = 1;

        for(int i = 1; i < A.length; i++) {
            if(A[i] == A[i-1]) {
                max[i] = 1;
                min[i] = 1;
            } else if(A[i] > A[i-1]) {
                max[i] = 1 + min[i-1];
                min[i] = 1;
                res = Math.max(res, max[i]);
            } else {
                min[i] = max[i-1] + 1;
                max[i] = 1;
                res = Math.max(res, min[i]);
            }
        }
        return res;
    }
}
