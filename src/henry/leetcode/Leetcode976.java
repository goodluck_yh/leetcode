package henry.leetcode;

import java.util.*;

public class Leetcode976 {
    public int largestPerimeter(int[] A) {
        if(A.length < 3)    return 0;
        Arrays.sort(A);
        for(int i = A.length-1; i-2 >= 0; i--) {
            if(A[i-1] + A[i-2] > A[i]) {
                return A[i-1] + A[i-2] + A[i];
            }
        }
        return 0;
    }
}
