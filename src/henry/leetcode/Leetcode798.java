package henry.leetcode;

/*
Given an array A, we may rotate it by a non-negative integer K so that the array becomes A[K], A[K+1], A{K+2], ... A[A.length - 1], A[0], A[1], ..., A[K-1].  Afterward, any entries that are less than or equal to their index are worth 1 point.
For example, if we have [2, 4, 1, 3, 0], and we rotate by K = 2, it becomes [1, 3, 0, 2, 4].  This is worth 3 points because 1 > 0 [no points], 3 > 1 [no points], 0 <= 2 [one point], 2 <= 3 [one point], 4 <= 4 [one point].
Over all possible rotations, return the rotation index K that corresponds to the highest score we could receive.  If there are multiple answers, return the smallest such index K.

Example 1:
Input: [2, 3, 1, 4, 0]
Output: 3
Explanation:
Scores for each K are listed below:
K = 0,  A = [2,3,1,4,0],    score 2
K = 1,  A = [3,1,4,0,2],    score 3
K = 2,  A = [1,4,0,2,3],    score 3
K = 3,  A = [4,0,2,3,1],    score 4
K = 4,  A = [0,2,3,1,4],    score 3
So we should choose K = 3, which has the highest score.
 */

public class Leetcode798 {
    public int bestRotation(int[] A) {
        int k = 0, max = 0;
        int[] cnt = new int[A.length * 2];

        // initial cnt
        for(int i= 0; i < A.length; i++) {
            if(A[i] <= i) {
                cnt[i - A[i]] ++;
                max++;
            }
        }
        int cur = max;
        // for loop A, move first to last one by one
        for(int i = 0; i < A.length; i++) {
            // update first in cnt, if first is valid, then first is zero
            int first = A[i];
            // second, third ... move to left
            cur = cur - cnt[i];
            // last is always valid
            cur++;
            // update first
            cnt[i + A.length - first] ++;

            if(cur > max) {
                max = cur;
                k = i+1;
            }
        }
        return k;
    }
}

/*
这个题目不好做，他的精髓就是申请了2*length的数组，我当时的第一反应是去更新数组，后来发现没必须要，直接移动移动下标就好！
据说这个题目还能用扫描线做！
 */