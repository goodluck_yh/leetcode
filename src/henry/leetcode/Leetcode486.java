package henry.leetcode;

/*
Given an array of scores that are non-negative integers. Player 1 picks one of the numbers from either end of the array followed by the player 2 and then player 1 and so on. Each time a player picks a number, that number will not be available for the next player. This continues until all the scores have been chosen. The player with the maximum score wins.

Given an array of scores, predict whether player 1 is the winner. You can assume each player plays to maximize his score.

Example 1:
Input: [1, 5, 2]
Output: False
Explanation: Initially, player 1 can choose between 1 and 2.
If he chooses 2 (or 1), then player 2 can choose from 1 (or 2) and 5. If player 2 chooses 5, then player 1 will be left with 1 (or 2).
So, final score of player 1 is 1 + 2 = 3, and player 2 is 5.
Hence, player 1 will never be the winner and you need to return False.
 */

public class Leetcode486 {
    public boolean PredictTheWinner(int[] nums) {
        int[][][] dp = new int[nums.length][nums.length][2];
        for(int i = 0; i < nums.length; i++) {
            dp[i][0][0] = nums[i];
        }
        for(int i = 1; i < nums.length; i++) {
            for(int j = 0; j + i < nums.length; j++) {
                // choose j
                int temp1 = nums[j] + dp[j+1][i-1][1];
                // choose j+i
                int temp2 = nums[j+i] + dp[j][i-1][1];
                if(temp1 >= temp2) {
                    dp[j][i][0] = temp1;
                    dp[j][i][1] = dp[j+1][i-1][0];
                } else {
                    dp[j][i][0] = temp2;
                    dp[j][i][1] = dp[j][i-1][0];
                }
            }
        }
        return dp[0][nums.length-1][0] >= dp[0][nums.length-1][1];
    }
}

/*
典型dp，想了一会就做出来了
唯一想了想的地方是第二位是len
 */