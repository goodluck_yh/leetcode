package henry.leetcode;
/*
A string of '0's and '1's is monotone increasing if it consists of some number of '0's (possibly 0), followed by some number of '1's (also possibly 0.)
We are given a string S of '0's and '1's, and we may flip any '0' to a '1' or a '1' to a '0'.
Return the minimum number of flips to make S monotone increasing.

Example 1:
Input: "00110"
Output: 1
Explanation: We flip the last digit to get 00111.

Example 2:
Input: "010110"
Output: 2
Explanation: We flip to get 011111, or alternatively 000111.

Example 3:
Input: "00011000"
Output: 2
Explanation: We flip to get 00000000.

Note:
1 <= S.length <= 20000
S only consists of '0' and '1' characters.
 */

public class Leetcode926 {
    public int minFlipsMonoIncr(String S) {
        char[] chs = S.toCharArray();
        int[][] dp = new int[chs.length][2];
        if(chs[0] == '0') {
            dp[0][0] = 0;
            dp[0][1] = 1;
        } else {
            dp[0][0] = 1;
            dp[0][1] = 0;
        }
        for(int i = 1; i < chs.length; i++) {
            if(chs[i] == '1') {
                dp[i][1] = Math.min(dp[i-1][0], dp[i-1][1]);
                dp[i][0] = dp[i-1][0] + 1;
            } else {
                dp[i][0] = dp[i-1][0];
                dp[i][1] = Math.min(dp[i-1][0], dp[i-1][1]) + 1;
            }
        }
        return Math.min(dp[chs.length-1][0], dp[chs.length-1][1]);
    }
}

/*
这个题目有两种解法，一种解法是官方解法，但是这个解法比较特殊。另一个解法是DP。
为什么是DP: 求最优解的问题，一般都是DP。（最大，最小）
当确定是DP之后，我们就要分类讨论，0和1的情况。很容易就可以发现，我们需要知道之前是0还是1，于是根据Leetcode935，我们需要使用两个DP数组。
复杂度：0（n)
 */
