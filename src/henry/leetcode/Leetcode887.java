package henry.leetcode;
/*
You are given K eggs, and you have access to a building with N floors from 1 to N.
Each egg is identical in function, and if an egg breaks, you cannot drop it again.
You know that there exists a floor F with 0 <= F <= N such that any egg dropped at a floor higher than F will break, and any egg dropped at or below floor F will not break.
Each move, you may take an egg (if you have an unbroken one) and drop it from any floor X (with 1 <= X <= N).
Your goal is to know with certainty what the value of F is.
What is the minimum number of moves that you need to know with certainty what F is, regardless of the initial value of F?

Example 1:
Input: K = 1, N = 2
Output: 2
Explanation:
Drop the egg from floor 1.  If it breaks, we know with certainty that F = 0.
Otherwise, drop the egg from floor 2.  If it breaks, we know with certainty that F = 1.
If it didn't break, then we know with certainty F = 2.
Hence, we needed 2 moves in the worst case to know what F is with certainty.
 */
public class Leetcode887 {
    public int superEggDrop(int K, int N) {
        // init dp[i][j] -> i moves and j eggs
        int[][] dp = new int[N+1][K+1];
        int m = 0;
        while(dp[m][K] < N) {
            m++;
            for(int k = 1; k <= K; k++) {
                dp[m][k] = dp[m-1][k-1] + dp[m-1][k] + 1;
            }
        }

        return m;
    }

    public int superEggDrop2(int K, int N) {
        // init dp[i][j] -> i floors and j eggs
        int[][] dp = new int[N+1][K+1];
        // if egg is one, move is equal to floor
        for(int i = 1; i <= N; i++) {
            dp[i][1] = i;
        }

        for(int i = 1; i < dp.length; i++) {
            for(int j = 2; j < dp[0].length; j++) {
                int lo = 1, hi = i;
                while(lo < hi) {
                    int mid = (lo + hi) / 2;
                    int below = dp[mid-1][j-1], upper = dp[i-mid][j];
                    if(below == upper)
                        break;
                    if(below > upper) {
                        hi = mid;
                    } else {
                        lo = mid + 1;
                    }
                }
                int mid = (lo + hi) / 2;
                dp[i][j] = 1 + Math.max(dp[i-mid][j], dp[mid-1][j-1]);
            }
        }
        return dp[N][K];
    }
}

/*
此题目很难，不好理解

 */
