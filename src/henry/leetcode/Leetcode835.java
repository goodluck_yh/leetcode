package henry.leetcode;

public class Leetcode835 {
    public int largestOverlap(int[][] A, int[][] B) {
        int len = A.length;
        int res = 0;
        for(int i = 0; i < len; i++) {
            for(int j = 0; j < len; j++) {
                res = Math.max(res, count(A, B, i, j));
                res = Math.max(res, count(B, A, i, j)); // bug
            }
        }
        return res;
    }

    private int count(int[][] A, int[][] B, int x, int y) {
        int len = A.length;
        int res = 0;
        for(int i = 0; i + x < len; i++) {
            for(int j = 0; j + y < len; j++) {
                if(A[i][j] == 1 && B[i+x][j+y] == 1) {
                    res++;
                }
            }
        }
        return res;
    }

    public static void main(String[] args) {
        int[][] A = {{0,0,0,0,0}, {0,1,0,0,0}, {0,0,1,0,0}, {0,0,0,0,1}, {0,1,0,0,1}};
        int[][] B = {{1,0,1,1,1}, {1,1,1,1,1}, {1,1,1,1,1}, {0,1,1,1,1}, {1,0,1,1,1}};
        Leetcode835 leetcode = new Leetcode835();
        System.out.println(leetcode.largestOverlap(A, B));
    }
}

/*
以为有什么特别好的解，其实没有
复杂度O(n^4)
有一个bug，在第10行
 */