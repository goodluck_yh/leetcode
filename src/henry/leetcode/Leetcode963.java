package henry.leetcode;

import java.util.*;
/*
给定一组坐标，找出四个顶点使其能构成长方形，求最小的长方形的面积。注意，边有可能不和x,y轴平行。
 */
public class Leetcode963 {
    public double minAreaFreeRect(int[][] points) {
        double res = Double.MAX_VALUE;
        HashMap<String, List<int[]>> map = new HashMap<>();
        for(int i = 0; i < points.length; i++) {
            int[] p1 = points[i];
            for(int j = i + 1; j < points.length; j++) {
                int[] p2 = points[j];
                long dis = (long)(p1[0] - p2[0]) * (p1[0] - p2[0]) + (long)(p1[1] - p2[1]) * (p1[1] - p2[1]);
                int x = p1[0] + p2[0];
                int y = p1[1] + p2[1];
                String key = dis + "+" + x + "+" + y;
                if(!map.containsKey(key))
                    map.put(key, new ArrayList<>());
                // 存下标
                map.get(key).add(new int[] {i, j});
            }
        }

        for(String key: map.keySet()) {
            List<int[]> list = map.get(key);
            if(list.size() > 1) {
                for(int i = 0; i < list.size(); i++) {
                    for(int j = i+1; j < list.size(); j++) {
                        // 任取三点
                        int p1 = list.get(i)[0];
                        int p2 = list.get(j)[0];
                        int p3 = list.get(j)[1];
                        double len1 = Math.sqrt((points[p1][0] - points[p2][0]) * (points[p1][0] - points[p2][0]) + (points[p1][1] - points[p2][1]) * (points[p1][1] - points[p2][1]));
                        double len2 = Math.sqrt((points[p1][0] - points[p3][0]) * (points[p1][0] - points[p3][0]) + (points[p1][1] - points[p3][1]) * (points[p1][1] - points[p3][1]));
                        res = Math.min(res, len1 * len2);
                    }
                }
            }
        }
        return res == Double.MAX_VALUE ? 0 : res;
    }
}

/*
长方形的充分必要条件：对角线长度相等切相互平分
算法思路：
（1）遍历points，放入到一个map中，map key是长度+中心点x+中心点y，value是一个int[] list
(2) 遍历这个map，对于同一个list中存在多个元素，则计算面积，求最小值

矩形问题的程序技巧：
（1）使用long
（2）计算长度，内部无需开根号
（3）斜率不要除，要加法 deltax1 * deltax2 + deltay1 * deltay2 = 0
（4）java.awt.Point有对应的工具
 */