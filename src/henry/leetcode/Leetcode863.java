package henry.leetcode;

import java.util.*;

public class Leetcode863 {
    public class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }

    public List<Integer> distanceK(TreeNode root, TreeNode target, int K) {
        List<TreeNode> path = new LinkedList<>();
        findPath(path, root, target);
        Set<TreeNode> pathSet = new HashSet<>();
        pathSet.addAll(path);
        Set<Integer> nei = new HashSet<>();
        for (int i = path.size() - 1; i >= 0; i--) {
            findNei(nei, K - (path.size() - 1 - i), path.get(i), pathSet);
        }
        List<Integer> res = new LinkedList<>();
        for (int i : nei) {
            res.add(i);
        }
        return res;
    }

    private void findNei(Set<Integer> nei, int k, TreeNode root, Set<TreeNode> pathSet) {
        if (root == null || pathSet.contains(root)) return;
        if (k == 0) {
            nei.add(root.val);
            return;
        }
        findNei(nei, k - 1, root.left, pathSet);
        findNei(nei, k - 1, root.right, pathSet);
    }

    private boolean findPath(List<TreeNode> path, TreeNode root, TreeNode target) {
        if (root == null) return false;
        path.add(root);
        if (root == target) return true;

        boolean res = findPath(path, root.left, target);
        if (res) return true;
        res = findPath(path, root.right, target);
        if (res) return true;
        path.remove(path.size() - 1);
        return false;
    }
}

/*
看了一下答案，答案算法跟我的不一样，我的算法是：
计算从root 到 target的路径
根据path计算出所有的节点
复杂度o(n)

第二种解法： 转换为graph，然后做bfs
 */
