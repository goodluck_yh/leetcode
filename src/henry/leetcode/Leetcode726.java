package henry.leetcode;

import java.util.*;

public class Leetcode726 {
    TreeMap<String, Integer> set = new TreeMap<>();

    public String countOfAtoms(String formula) {
        helper(formula, 1);
        StringBuilder sb = new StringBuilder();
        for(String s: set.keySet()) {
            sb.append(s);
            if(set.get(s) > 1)
                sb.append(set.get(s));
        }
        return sb.toString();
    }

    private void helper(String formula, int cnt) {
        int idx = 0;
        while(idx < formula.length()) {
            int i = idx;
            if(formula.charAt(i) >= 'A' && formula.charAt(i) <= 'Z') {
                i++;
                while(i < formula.length() && formula.charAt(i) != '(' && (formula.charAt(i) > 'Z' || formula.charAt(i) < 'A')) i++;
                String sub = formula.substring(idx, i);
                cnt(sub, cnt);
                idx = i;
            } else if(formula.charAt(i) == '(') {
                i++;
                int left = 1;
                while(left != 0) {
                    if(formula.charAt(i) == '(')
                        left++;
                    else if(formula.charAt(i) == ')')
                        left--;
                    i++;
                }
                String sub = formula.substring(idx+1, i-1);
                idx = i;
                while(i < formula.length() && formula.charAt(i) >= '0' && formula.charAt(i) <= '9') i++;
                String numStr = formula.substring(idx, i);
                int num = numStr.length() == 0 ? 1 : Integer.parseInt(numStr);
                helper(sub, cnt * num);
                idx = i;
            }
        }
    }

    private void cnt(String s, int cnt) {
        int i = 0;
        while(i < s.length() && (s.charAt(i) > '9' || s.charAt(i) < '0'))   i++;
        String ele = s.substring(0, i), numStr = s.substring(i, s.length());
        int num = numStr.length() == 0 ? cnt : Integer.parseInt(numStr) * cnt;
        set.put(ele, set.getOrDefault(ele, 0) + num);
    }
}
/*
简单
 */