package henry.leetcode;

public class Leetcode647 {
    class Solution {
        public int countSubstrings(String s) {
            int res = s.length(), len = s.length();
            boolean[][] dp = new boolean[len][len];
            for(int i = 0; i < len; i++) {
                dp[i][0] = true;
            }

            for(int i = 1; i < len; i++) {
                for(int j = 0; j + i < len; j++) {
                    if(s.charAt(j) == s.charAt(j+i)) {
                        if(i == 1)
                            dp[j][i] = true;
                        else
                            dp[j][i] = dp[j+1][i-2];
                    } else {
                        dp[j][i] = false;
                    }
                    if(dp[j][i])
                        res++;
                }
            }
            return res;
        }
    }
}

/*
解法1： dp 复杂度O(n^2)
解法2： 马拉车算法(Manacher) 复杂度O(n) 不会！
 */