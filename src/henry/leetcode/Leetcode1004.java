package henry.leetcode;

/*
Input: A = [1,1,1,0,0,0,1,1,1,1,0], K = 2
Output: 6
Explanation:
[1,1,1,0,0,1,1,1,1,1,1]
Bolded numbers were flipped from 0 to 1.  The longest subarray is underlined.
 */
public class Leetcode1004 {
    public int longestOnes(int[] A, int K) {
        int s = 0, e = 0, cnt = 0, res = 0;
        while(e < A.length) {
            if(A[e++] == 0)
                cnt++;
            if(cnt == K) {
                res = Math.max(res, e - s);

            }
            else if(cnt > K) {
                while(A[s] != 0)
                    s++;
                cnt--;
            }
        }
        return res;
    }
}
