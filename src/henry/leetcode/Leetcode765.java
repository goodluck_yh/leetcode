package henry.leetcode;

/*
N couples sit in 2N seats arranged in a row and want to hold hands. We want to know the minimum number of swaps so that every couple is sitting side by side. A swap consists of choosing any two people, then they stand up and switch seats.
The people and seats are represented by an integer from 0 to 2N-1, the couples are numbered in order, the first couple being (0, 1), the second couple being (2, 3), and so on with the last couple being (2N-2, 2N-1).
The couples' initial seating is given by row[i] being the value of the person who is initially sitting in the i-th seat.

Example 1:
Input: row = [0, 2, 1, 3]
Output: 1
Explanation: We only need to swap the second (row[1]) and third (row[2]) person.
 */
public class Leetcode765 {
    public int minSwapsCouples(int[] row) {
        int res = 0;
        for(int i = 0; i < row.length;) {
            int first = row[i] / 2, second = row[i+1] / 2;
            if(first != second) {
                res++;
                for(int j = i + 2; j < row.length; j++) {
                    if(row[j] / 2 == first) {
                        int temp = row[i+1];
                        row[i+1] = row[j];
                        row[j] = temp;
                        break;
                    } else if(row[j] / 2 == second) {
                        int temp = row[i];
                        row[i] = row[j];
                        row[j] = temp;
                        break;
                    }
                }
            }

            i = i + 2;
        }
        return res;
    }
}

/*
复杂度o(n^2) beat 76.92%
 */

// follow up: how can we reduce it to o(n)
class Leetcode765_2 {
    public int minSwapsCouples(int[] row) {
        int res = 0;
        int[] pos = new int[row.length];
        for(int i = 0; i < row.length; i++) {
            pos[row[i]] = i;
        }
        for(int i = 0; i < row.length;) {
            int first = row[i] / 2, second = row[i+1] / 2;
            if(first != second) {
                res++;
                if(row[i] % 2 == 0) {
                    int idx = pos[row[i] + 1];
                    int temp = row[i+1];
                    row[i+1] = row[idx];
                    row[idx] = temp;
                    pos[row[i+1]] = i+1;
                    pos[row[idx]] = idx;
                } else {
                    int idx = pos[row[i] - 1];
                    int temp = row[i+1];
                    row[i+1] = row[idx];
                    row[idx] = temp;
                    pos[row[i+1]] = i+1;
                    pos[row[idx]] = idx;
                }
            }
            i = i + 2;
        }
        return res;
    }

    public static void main(String[] args) {
        Leetcode765_2 leetcode = new Leetcode765_2();
        System.out.println(leetcode.minSwapsCouples(new int[]{0,2,4,6,7,1,3,5}));
    }
}

/*
beat 76.92%
tips:
难点在于，要持续更新pos数组
 */