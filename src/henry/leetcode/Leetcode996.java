package henry.leetcode;

import java.util.*;

public class Leetcode996 {
    Set<String> res = new HashSet<>();
    public int numSquarefulPerms(int[] A) {
        helper(A, new LinkedList<>(), 0);
        return res.size();
    }

    private void helper(int[] A, List<Integer> path, int idx) {
        if(idx == A.length) {
            StringBuilder sb = new StringBuilder();
            for(int i: path) {
                sb.append(i);
                sb.append('#');
            }
            res.add(sb.toString());
            return;
        }
        Set<Integer> visited = new HashSet<>();
        for(int i = idx; i < A.length; i++) {
            int next = A[i];
            if(!visited.contains(next) && (path.size() == 0 || isSquare(path.get(path.size() - 1), next))) {
                visited.add(next);
                A[i] = A[idx];
                A[idx] = next;
                path.add(A[idx]);
                helper(A, path, idx+1);
                A[idx] = A[i];
                A[i] = next;
                path.remove(path.size() - 1);
            }
        }
    }

    private boolean isSquare(int a, int b) {
        int sqrt = (int)Math.sqrt(a + b);
        return sqrt * sqrt == (a + b);
    }
}
