package henry.leetcode;

import java.util.*;
/*
In a given integer array A, we must move every element of A to either list B or list C. (B and C initially start empty.)

Return true if and only if after such a move, it is possible that the average value of B is equal to the average value of C, and B and C are both non-empty.

Example :
Input:
[1,2,3,4,5,6,7,8]
Output: true
Explanation: We can split the array into [1,4,5,8] and [2,3,6,7], and both of them have the average of 4.5.
 */

public class Leetcode805 {
    HashMap<Integer, Boolean> map = new HashMap<>();
    public boolean splitArraySameAverage(int[] A) {
        int total = 0;
        for(int i : A)  total += i;
        double target = (double)total / A.length;
        List<Integer> list = new LinkedList<>();
        for(int i = 1; i < A.length; i++) {
            if(total * i % A.length == 0)
                list.add(i);
        }
        if(list.size() == 0)    return false;
        for(int k: list) {
            map = new HashMap<>();
            boolean flag = helper(A, target, 0, 0, 0, k);
            if(flag)
                return true;
        }
        return false;
    }

    private boolean helper(int[] a, double target, int cur, int visited, int cnt, int k) {
        if(cnt == k) {
            if(Math.abs(target * cnt - cur) <= 0.000001) {
                map.put(visited, true);
                return true;
            } else {
                map.put(visited, false);
                return false;
            }
        }
        if(map.containsKey(visited))    return map.get(visited);
        for(int i = 0; i < a.length; i++) {
            if((visited & (1 << i)) == (1 << i)) continue;
            int new_visited = visited;
            new_visited = new_visited | (1 << i);
            boolean flag = helper(a, target, cur + a[i], new_visited, cnt+1, k);
            if(flag) {
                map.put(visited, true);
                return true;
            }
        }
        map.put(visited, false);
        return false;
    }

    public static void main(String[] args) {
        Leetcode805 leetcode = new Leetcode805();
        System.out.println(leetcode.splitArraySameAverage(new int[] {2,0,5,6,16,12,15,12,4}));
    }
}

/*
tag: hard
思路应该没问题，但是剪枝有问题
 */

class Leetcode805_2 {
    public boolean check(int[] A, int leftSum, int leftNum, int startIndex) {
        if (leftNum == 0) return leftSum == 0;
        if ((A[startIndex]) > leftSum / leftNum) return false;
        for (int i = startIndex; i < A.length - leftNum + 1; i ++) {
            if (i > startIndex && A[i] == A[i - 1]) continue;
            if (check(A, leftSum - A[i], leftNum - 1, i + 1)) return true;
        }
        return false;
    }

    public boolean splitArraySameAverage(int[] A) {
        if (A.length == 1) return false;
        int sumA = 0;
        for (int a: A) sumA += a;
        Arrays.sort(A);
        for (int lenOfB = 1; lenOfB <= A.length / 2; lenOfB ++) {
            if ((sumA * lenOfB) % A.length == 0) {
                if (check(A, (sumA * lenOfB) / A.length, lenOfB, 0)) return true;
            }
        }
        return false;
    }
}

/*
题目重点是sort，思路不好想,可以回来再做做！
 */