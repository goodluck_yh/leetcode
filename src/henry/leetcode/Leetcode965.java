package henry.leetcode;

public class Leetcode965 {
    class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }

    int same = -1;
    public boolean isUnivalTree(TreeNode root) {
        same = root.val;
        return helper(root);
    }

    private boolean helper(TreeNode node) {
        if(node == null)
            return true;
        if(node.val != same)
            return false;
        return helper(node.left) && helper(node.right);
    }
}
