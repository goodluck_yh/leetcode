# Given a non-empty array of unique positive integers A, consider the following graph:
#
# There are A.length nodes, labelled A[0] to A[A.length - 1];
# There is an edge between A[i] and A[j] if and only if A[i] and A[j] share a common factor greater than 1.
# Return the size of the largest connected component in the graph.
#
#
#
# Example 1:
#
# Input: [4,6,15,35]
# Output: 4
import math


class Solution:
    class UnionFind:
        def __init__(self, n):
            self.parents = [i for i in range(n)]
            self.size = [1] * n

        def union(self, a, b):
            idx_a = self.find(a)
            idx_b = self.find(b)
            if idx_a < idx_b:
                self.parents[idx_b] = idx_a
                self.size[idx_a] += self.size[idx_b]
            elif idx_a > idx_b:
                self.parents[idx_a] = idx_b
                self.size[idx_b] += self.size[idx_a]

        def find(self, idx):
            while self.parents[idx] != idx:
                idx = self.parents[idx]
            return idx

    def largestComponentSize(self, A: 'List[int]') -> 'int':
        uf = self.UnionFind(len(A))
        primeToIndex = {}
        for i, a in enumerate(A):
            primes = self.get_prime_factors(a)
            for p in primes:
                if p in primeToIndex:
                    uf.union(i, primeToIndex[p])
                primeToIndex[p] = i

        return max(uf.size)

    @staticmethod
    def get_prime_factors(n):
        res = set()
        while n % 2 == 0:
            res.add(2)
            n = n / 2
        for num in range(3, int(math.sqrt(n)) + 1, 2):
            while n % num == 0:
                res.add(num)
                n = n / num
        if n > 1:
            res.add(n)
        return res


if __name__ == "__main__":
    solution = Solution()
    print(solution.largestComponentSize([2, 3, 6, 7, 4, 12, 21, 39]))
